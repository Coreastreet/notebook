class NoteWorker
  include Sidekiq::Worker

  # generate pdf sample and, if a former docx/doc file, update the orientation value of the note.
  def perform(note_id, upload_type_docx, file_title)
      if (Note.exists?(note_id)) # if note exists, then a pdf must be attached since it runs in create.
          note = Note.find(note_id)

          if (upload_type_docx)
            hw_ratio = get_hw_ratio("/tmp/#{file_title}.pdf") # only true for vertical
            pages = CombinePDF.load("/tmp/#{file_title}.pdf").pages
          else
            note.pdf.blob.open do |temp_file|
                hw_ratio = get_hw_ratio(temp_file.path) # only true for vertical
                pages = CombinePDF.load(temp_file.path).pages
            end
          end # no need for upload_type_pdf since pages length and orientation already saved.

          (pages[0].orientation == :landscape) ?
                    note.update!(number_of_pages: pages.length, number_of_sample_pages: get_pdf_sample_count(pages.length),
                                 paper_size_ratio: (1/hw_ratio), is_horizontal?: true) :
                    note.update!(number_of_pages: pages.length, number_of_sample_pages: get_pdf_sample_count(pages.length), 
                                 paper_size_ratio: hw_ratio)

          pdf_sample = generate_pdf_sample(pages, file_title)
          note.pdf_sample.attach(io: File.open("/tmp/#{file_title}_sample.pdf"),
                                 filename: "#{file_title}_sample.pdf", content_type: 'application/pdf')
      else # then user must have deleted the note for some reason.
          logger.debug "noteworker cannot perform since note not found."
      end
  end

  def get_pdf_sample_count(page_count)
      if (page_count <= 85)
          result = (1.4*(Math.sqrt(page_count).round(2))) - 2
      else
          result = (1.4*(Math.sqrt(page_count).round(2))) - 0.5
      end
      return result.round(0)
  end

  def get_middle_index(num) # start of the second portion after division into halves
      if num.even?
          middle_index = (num/2)
      else
          middle_index = ((num/2.0).round(0))
      end
      return middle_index
  end

  def get_pdf_sample_indicies(page_count)
      number_of_sample_pages = get_pdf_sample_count(page_count)
      unshifted_second_start = get_middle_index(number_of_sample_pages)
      first_end = unshifted_second_start - 1

      # adjust the second half by adding half the page count.
      second_start = get_middle_index(page_count)
      second_end = second_start + (number_of_sample_pages - 1 - unshifted_second_start)

      indicies = {"first": first_end, "second": [second_start, second_end]}
      return indicies
  end

  def generate_pdf_sample(pages, title)
      pdf_sample = CombinePDF.new

      indicies = get_pdf_sample_indicies(pages.length)
      0.upto(indicies[:first]) do |i|
          pdf_sample << pages[i]
      end

      indicies[:second][0].upto(indicies[:second][1]) do |i|
          pdf_sample << pages[i]
      end

      pdf_sample.save "/tmp/#{title}_sample.pdf"
      return pdf_sample
  end

  # assumed to be for height over width for vertical orientation.
  def get_hw_ratio(pdf_reader_file_path)
      reader = PDF::Reader.new(pdf_reader_file_path)
      page1 = reader.page(1)
      dimensions = page1.attributes[:MediaBox]

      width = dimensions[2] - dimensions[0]
      height = dimensions[3]- dimensions[1]
      hw_ratio  = height/(width.to_f)

      return hw_ratio
  end
end
