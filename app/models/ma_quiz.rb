class MaQuiz < ApplicationRecord
    has_one :lesson, as: :learnable
    # belongs_to :module_part

    serialize :hash_array_answers, Array     
end
