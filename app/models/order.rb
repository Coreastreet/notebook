class Order < ApplicationRecord
  belongs_to :note
  belongs_to :buyer, class_name: "Student"
  belongs_to :seller, class_name: "Student"
end
