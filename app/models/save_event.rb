class SaveEvent < ApplicationRecord
  belongs_to :saveable, polymorphic: true
  belongs_to :student
end
