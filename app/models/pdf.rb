class Pdf < ApplicationRecord
  # belongs_to :high_school_subject, optional: true
  # belongs_to :uni_subject, optional: true
  # belongs_to :notable, polymorphic: true
  # belongs_to :student

  has_one_attached :pdf_doc, dependent: :destroy
  # has_one_attached :pdf_sample_doc, dependent: :destroy
  # remove pdf_thumbnail since pdf preview is better
  #has_many :categories
  # has_many :orders
  # has_many :shopping_carts

  validates_length_of :title, :within => 6..150,
                              :too_short => 'title cannot be less than 6 characters',
                              :too_long => 'title cannot be more than 150 characters'

  before_save :capitalize_title
  before_save :round_to_two_places

  has_secure_token :id_token


  #has_many :like_events, dependent: :destroy
  #has_many :view_events, dependent: :destroy
  #has_many :save_events, dependent: :destroy

  #has_many :comments, dependent: :destroy

  private

      def capitalize_title
          self.title.capitalize!
      end

      def round_to_two_places
          self.price = self.price.round(2)
      end
end
