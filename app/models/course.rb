class Course < ApplicationRecord
    serialize :objectives, Array

    belongs_to :student
    has_secure_token :id_token

    has_one_attached :image, dependent: :destroy
    has_one_attached :course_outline, dependent: :destroy

    has_many :orders
    #has_many :shopping_carts
    validates :texts_count, numericality: { greater_than_or_equal_to: 0 }
    validates :videos_count, numericality: { greater_than_or_equal_to: 0 }
    validates :quizzes_count, numericality: { greater_than_or_equal_to: 0 }
    validates :lessons_count, numericality: { greater_than_or_equal_to: 0 }
    #has_many :like_events, dependent: :destroy
    #has_many :view_events, dependent: :destroy
    #has_many :enroll_events, dependent: :destroy
    has_many :progress_events, dependent: :destroy
    has_many :save_events, as: :saveable, dependent: :destroy

    # for now, hide the ability to comment for convenience
    has_many :comments, dependent: :destroy

    has_many :module_parts, dependent: :destroy
    has_many :lessons, through: :module_parts

    enum difficulty_level: { Beginner: 0, Intermediate: 1, Advanced: 2 }
    enum subject: { Learning_project: 1, Volunteer_project: 2, Corporate_project: 3 }

    validates_length_of :title, :within => 6..150,
                                :too_short => 'title cannot be less than 6 characters',
                                :too_long => 'title cannot be more than 150 characters'

    after_create :create_intro_and_summary

    def increment_counter(lesson_type)
        case lesson_type
        when "LessonText"
            self.increment!(:texts_count)
        when "LessonVideo"
            self.increment!(:videos_count)
        when "McQuiz"
            self.increment!(:quizzes_count)
        else
        end
    end
  
    def decrement_counter(lesson_type)
        case lesson_type
        when "LessonText"
            self.decrement!(:texts_count)
        when "LessonVideo"
            self.decrement!(:videos_count)
        when "McQuiz"
            self.decrement!(:quizzes_count)
        else
        end
    end

    def get_module_index(module_id)
        return self.module_order.index(module_id)
    end

    def create_intro_and_summary
        intro = ModulePart.create!(title: "Introduction", course_id: self.id)
        intro_lesson = Lesson.create!(learnable: "LessonText".constantize.new(), module_part_id: intro.id)

        summary = ModulePart.create!(title: "Summary", course_id: self.id)
        summary_lesson = Lesson.create!(learnable: "LessonText".constantize.new(), module_part_id: summary.id)

        # update the lesson_order and count manually to save operations.
        self.update!(lesson_order: [intro_lesson.id, summary_lesson.id], lessons_count: 2)
        intro.update!(lesson_order: [intro_lesson.id])
        summary.update!(lesson_order: [summary_lesson.id])
    end
end
