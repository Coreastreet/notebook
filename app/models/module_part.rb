class ModulePart < ApplicationRecord
  belongs_to :course

  has_many :lessons, dependent: :destroy

  has_one_attached :image, dependent: :destroy

  validates :title, presence: true
  validates :image, content_type: ['image/png', 'image/jpg', 'image/jpeg', 'image/webp'],
            size: { less_than: 2.megabytes , message: 'must be less than 2MB in size' }
  
  validates :texts_count, numericality: { greater_than_or_equal_to: 0 }
  validates :videos_count, numericality: { greater_than_or_equal_to: 0 }
  validates :quizzes_count, numericality: { greater_than_or_equal_to: 0 }
  # before_destroy :delete_all_lessons, prepend: true
  # so to ensure it runs before dependent: :destroy e.g. on parent course deletion.
  after_create :increment_course_counter, unless: :destroyed_by_association
  before_destroy :decrement_course_counter, unless: :destroyed_by_association
  before_destroy :remove_from_progress, unless: :destroyed_by_association

  enum lesson_type: { lesson_text: 1, lesson_video: 2, ma_quiz: 3, 
        mc_quiz: 4, fa_quiz: 5, sa_quiz: 6, assignment_text: 7, assignment_video: 8 }

  def module_order_index
    return self.course.module_order.index(self.id)
  end

  def get_lesson_index(lesson_id)
    return self.lesson_order.index(lesson_id) 
  end

  # return the next module or if current module is the last one, return nil.
  def next_module
      module_order = self.course.module_order
      next_module_index = self.module_order_index + 1

      if (next_module_index == module_order.size) 
          # this means the current module is the last
          return nil
      else 
          # current module is not the last
          # return the next module_id
          next_module = ModulePart.find(module_order[next_module_index])

          if (next_module.lessons_count > 0)
              return next_module
          else   
              # since effectively not useful because next module has no lessons.
              return nil
          end
      end
  end

  # return the prev module or if current module is the first one, return nil.
  def prev_module
      module_order = self.course.module_order

      current_module_index = module_order.index(self.id)
      prev_module_index = current_module_index - 1
      if (prev_module_index < 0) 
          # this means the current module is the first
          return nil
      else 
          # current module is not the last
          # return the next module_id
          prev_module = ModulePart.find(module_order[prev_module_index])

          if (prev_module.lessons_count > 0)
              return prev_module
          else   
              # since effectively not useful because prev module has no lessons.
              return nil
          end
      end
  end


    def increment_counter(lesson_type)
        case lesson_type
            when "LessonText"
                self.increment!(:texts_count)
            when "LessonVideo"
                self.increment!(:videos_count)
            when "McQuiz"
                self.increment!(:quizzes_count)
            else
        end
    end

    def decrement_counter(lesson_type)
        case lesson_type
            when "LessonText"
                self.decrement!(:texts_count)
            when "LessonVideo"
                self.decrement!(:videos_count)
            when "McQuiz"
                self.decrement!(:quizzes_count)
            else
        end
    end

    def decrement_course_counter
        # remove all the deleted module lessons from parent course lesson array.
        course = self.course
        new_module_order = course.module_order - [self.id] 

        if (self.lesson_order.length > 0)
            # if the module deleted had lessons, update all the different counters.
            new_course_lesson_order = course.lesson_order - self.lesson_order
            new_quiz_count = course.quizzes_count - self.quizzes_count
            new_text_count = course.texts_count - self.texts_count
            new_video_count = course.videos_count - self.videos_count

            course.update!(lesson_order: new_course_lesson_order,
                        module_order: new_module_order,  
                        lessons_count: new_course_lesson_order.length,
                        quizzes_count: new_quiz_count,
                        texts_count: new_text_count,
                        videos_count: new_video_count)
        else 
            # else just remove the module id from the parent array, no counters change.
            course.update!(module_order: new_module_order)
        end
    end

    def increment_course_counter
        # add the new module id to parent course module array after new module is created
        course = self.course
        module_order = course.module_order
        # the course is newly created.
        new_module_order = (module_order.length < 2) ? module_order.push(self.id)
                                                : module_order.insert(-2, self.id)                                 
        course.update!(module_order: new_module_order)
    end

    def remove_from_progress 
        progress = self.course.progress_events
        progress.each do |progress_event|
            overlapping_lessons = progress_event.lessons_completed & self.lesson_order   
            if (overlapping_lessons.any?) # that is modules' lessons have been completed as progress
                progress_event.update!(lessons_completed: lesson_progress - overlapping_lessons)
            end
        end
    end

end


