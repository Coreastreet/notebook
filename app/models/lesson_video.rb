class LessonVideo < ApplicationRecord
    #belongs_to :module_part
    has_one :lesson, as: :learnable

    has_rich_text :description
    has_one_attached :video, dependent: :destroy
end
