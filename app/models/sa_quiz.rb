class SaQuiz < ApplicationRecord
    has_one :lesson, as: :learnable

    has_one_attached :image
end
