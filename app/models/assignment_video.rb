class AssignmentVideo < ApplicationRecord
    #belongs_to :module_part
    has_one :lesson, as: :learnable

    has_rich_text :requirements
    has_many :video_submissions, dependent: :destroy

    validates :prize_money, :numericality => { :greater_than_or_equal_to => 0 }
    
    validate :valid_deadline, :on => :update

    def valid_deadline
        if (self.deadline_date < Date.current)
            errors.add(:deadline_date, "must not be earlier than the current date")
        end
    end
end
