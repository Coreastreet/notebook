class Comment < ApplicationRecord
  belongs_to :student

  belongs_to :commentable, polymorphic: true
  has_many :comments, as: :commentable, dependent: :destroy

  has_many :notifications, as: :notifiable
  has_many :upvotes, as: :upvoteable, dependent: :destroy

  validates :upvotes_count, :numericality => { :greater_than_or_equal_to => 0 }
end
