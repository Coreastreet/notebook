class Competition < ApplicationRecord
  belongs_to :student

  has_rich_text :requirements
  has_rich_text :description

  has_one_attached :video, dependent: :destroy
  has_many :video_submissions, dependent: :destroy
  has_many :competition_events, dependent: :destroy
  has_many :contestants, through: :competition_events, source: :student

  has_many :notifications, as: :notifiable

  has_many :comments, as: :commentable

  has_many :save_events, as: :saveable, dependent: :destroy

  validates :prize_money, :numericality => { :greater_than_or_equal_to => 0 }
  
  # validate :valid_deadline, :on => :update

  def valid_deadline
      if (self.deadline_date < Date.current)
          errors.add(:deadline_date, "must not be earlier than the current date")
      end
  end
end
