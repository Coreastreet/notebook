class CompetitionEvent < ApplicationRecord
  belongs_to :student
  belongs_to :competition

  validates :student_id, uniqueness: { scope: :competition_id, message: "Student already joined this competition." }
end
