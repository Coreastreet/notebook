class AssignmentText < ApplicationRecord
    #belongs_to :module_part
    has_one :lesson, as: :learnable

    has_one_attached :doc
end
