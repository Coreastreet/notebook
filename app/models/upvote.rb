class Upvote < ApplicationRecord
  belongs_to :student
  belongs_to :upvoteable, polymorphic: true
  #scope :accepted, -> { where(like_status: true) }
  #scope :rejected, -> { where(dislike_status: true) }
end
