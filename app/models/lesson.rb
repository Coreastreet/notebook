class Lesson < ApplicationRecord
    belongs_to :module_part, counter_cache: :lessons_count
    # belongs_to :learnable, polymorphic: true

    delegated_type :learnable, 
        types: %w[ lesson_text lesson_video ma_quiz 
                   mc_quiz fa_quiz sa_quiz assignment_text assignment_video ], dependent: :destroy
    # keep in mind that incrementing counter does not update the lesson order for module or course.
    after_create :increment_course_counter
    after_create :increment_module_counter
    before_destroy :decrement_course_counter, unless: :destroyed_by_association
    before_destroy :decrement_module_counter, unless: :destroyed_by_association
    before_destroy :remove_from_progress, unless: :destroyed_by_association

    def lesson_type
        return self.learnable_type.underscore.to_sym
    end

    def lesson_order_index
        return self.module_part.lesson_order.index(self.id)
    end

    # expand later on
    def get_icon
        case self.learnable_type
        when "LessonText"
            "book_text.png"
        when "LessonVideo"
            "video_icon.png"
        when "McQuiz"
            "quiz.png"
        else 
            nil
        end
    end

    # return the id of the next lesson or nil
    def next_lesson
        module_part = self.module_part
        lesson_order = module_part.lesson_order

        # make sure module has lessons under it.
        # current_lesson_index = lesson_order.index(current_lesson_id)
        next_lesson_index = self.lesson_order_index + 1
        if (next_lesson_index == lesson_order.size) 
            # this means the current lesson is the last
            next_module = module_part.next_module
            if (!next_module.nil?)
                return next_module.lesson_order[0]
            else 
                # either next module has no lessons or the current module is the last. 
                return nil 
            end
        else 
            # current lesson is not the last
            # return the next lesson_id
            return lesson_order[next_lesson_index]
        end
    end

    # return the id of the next lesson or nil
    def prev_lesson
        module_part = self.module_part
        lesson_order = module_part.lesson_order

        # current_lesson_index = lesson_order.index(current_lesson_id)
        prev_lesson_index = self.lesson_order_index - 1
        if (prev_lesson_index < 0) 
            # this means the current lesson is the first
            prev_module = module_part.prev_module
            if (!prev_module.nil?)
                # current module is not the first and prev module has lessons
                return prev_module.lesson_order[-1]
            else 
                # either prev module has no lessons or the current module is the first so no prev module exists.
                return nil 
            end
        else 
            # current lesson is not the first of the current module
            # return the prev lesson_id
            return lesson_order[prev_lesson_index]
        end
    end

    def increment_course_counter
        course = self.module_part.course
        course.increment_counter(self.learnable_type)
        # leave updating the lesson order to controller
    end

    def increment_module_counter
        module_part = self.module_part
        module_part.increment_counter(self.learnable_type)
        # leave updating the lesson order to controller
    end

    def decrement_course_counter
        course = self.module_part.course
        course.decrement_counter(self.learnable_type)
        # remove from parent array
        new_lesson_order = course.lesson_order - [self.id]
        course.update!(lesson_order: new_lesson_order, lessons_count: new_lesson_order.length)
    end

    def decrement_module_counter
        module_part = self.module_part
        module_part.decrement_counter(self.learnable_type)
        # remove from parent array
        new_lesson_order = module_part.lesson_order - [self.id]
        module_part.update!(lesson_order: new_lesson_order) 
    end

    def remove_from_progress
        progress = self.module_part.course.progress_events
        progress.each do |progress_event|
            lesson_progress = progress_event.lessons_completed
            if (lesson_progress.include?(self.id))
                progress_event.update!(lessons_completed: lesson_progress - [self.id])
            end
        end
    end

    def set_has_content
        case self.learnable_type
        when "McQuiz"
            bool = !(self.learnable.question_order.empty?)
        when "LessonText"
            bool = !(self.learnable.content.empty?)
        when "LessonVideo"
            bool = self.learnable.video.attached?
        else 
        end
        self.update!(has_content: bool)
    end
end
