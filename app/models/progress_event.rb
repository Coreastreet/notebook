class ProgressEvent < ApplicationRecord
  belongs_to :student
  belongs_to :course

  scope :completed, -> { where(is_completed: true) }
  scope :in_progress, -> { where(is_completed: false, is_enrolled: true) }

  validates :student_id, uniqueness: { scope: :course_id }

  def reset_all_progress  # basically unenroll the student concerned from a course.
      self.update(modules_completed: [], lessons_completed: [], 
                  is_enrolled: false, is_completed: false, progress_percent: 0,
                  hash_of_quiz_answers: {}, most_recent_quiz_question_index: {},
                  hash_of_quiz_results: {})
  end
end
