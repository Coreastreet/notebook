class McQuizQuestion < ApplicationRecord
  belongs_to :mc_quiz, counter_cache: :mc_quiz_questions_count

  has_rich_text :question
  has_rich_text :explanation

  def question_order_index
      index = self.mc_quiz.question_order.index(self.id)
      return index
  end

  def next_question_id
      question_order = self.mc_quiz.question_order
      question_order_index = self.question_order_index
      if ((question_order_index+1) < question_order.length)
          # this means current index is not the last 
          next_question_id = self.mc_quiz.question_order.at(self.question_order_index + 1)
      else 
          # if true then it means the last index or beyond is reached.
          next_question_id = nil
      end
      
      return next_question_id
  end

  def lesson
      self.mc_quiz.lesson  
  end
end
