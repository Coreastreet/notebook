class Notification < ApplicationRecord
  belongs_to :notifiable, polymorphic: true
  belongs_to :student, counter_cache: :notifications_count

  after_create :notify_student
  after_destroy :decrement_count

  def notify_student
      student = self.student
      student.increment_unread_notifications_count
      student.increment_notifications_count
  end

  def decrement_count
      student = self.student
      student.decrement_notifications_count
  end
end
