class Practical < ApplicationRecord
    has_one :lesson, as: :learnable
end
