class McQuiz < ApplicationRecord
    #belongs_to :module_part
    has_one :lesson, as: :learnable
    has_many :mc_quiz_questions, dependent: :destroy
end
