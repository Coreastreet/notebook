class LessonText < ApplicationRecord
    #belongs_to :module_part
    has_one :lesson, as: :learnable

    has_rich_text :content

    has_many_attached :images, dependent: :destroy
    has_many_attached :docs, dependent: :destroy
end
