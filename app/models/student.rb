class Student < ApplicationRecord
  has_secure_password

  validates :username, presence: true, uniqueness: true,
                       length: { minimum: 6, maximum: 30 }, format: { with: Notebank::USERNAME_REGEX, multiline: true }
  validates :email, presence: true, uniqueness: true,
                    confirmation: true, format: { with: URI::MailTo::EMAIL_REGEXP }
  # validates :email_confirmation, presence: true
  # email can be set with a confirmation field. esp on the first time.
  validates :display_name, length: { maximum: 30 }

  enum account_status: { active: 0, inactive: 1, deleted: 2 }

  has_one_attached :image

  has_one :password_reset_digest, dependent: :destroy
  has_one :report_account_digest, dependent: :destroy
  has_one :billing_address, required: false, dependent: :destroy
  has_one :bank_account, required: false, dependent: :destroy

  has_many :notifications, dependent: :destroy
  has_many :competitions
  has_many :competition_events, dependent: :destroy
  has_many :subscribed_competitions, through: :competition_events, source: :competitions

  validates :unread_notifications_count, numericality: { greater_than_or_equal_to: 0 }
  # has_many :subjects
  # has_many :notes, dependent: :nullify

  has_many :sales, class_name: 'Order', foreign_key: 'seller_id'
  has_many :purchases, class_name: 'Order', foreign_key: 'buyer_id'
  
  # for comments and likes/dislikes
  #has_many :likes, -> { LikeEvent.accepted }, class_name: "LikeEvent", dependent: :destroy
  #has_many :dislikes, -> { LikeEvent.rejected }, class_name: "LikeEvent", dependent: :destroy
  # has_many :view_events, dependent: :destroy

  has_many :created_courses, class_name: "Course"
  #has_many :course_subscriptions, dependent: :destroy
  #has_many :subscribed_courses, through: :course_subscriptions, source: :course

  #has_many :shopping_carts
  #has_many :cart_courses, through: :shopping_carts, source: :course 
  #has_many :liked_courses, through: :likes, source: :course
  #has_many :disliked_courses, through: :dislikes, source: :course  
  #has_many :enroll_events, dependent: :destroy
  #has_many :enrolled_courses, through: :enroll_events, source: :course

  has_many :progress_events, dependent: :destroy

  has_many :completed_progress_events, -> { ProgressEvent.completed }, class_name: "ProgressEvent", dependent: :destroy
  has_many :completed_courses, through: :completed_progress_events, source: :course

  has_many :in_progress_events, -> { ProgressEvent.in_progress }, class_name: "ProgressEvent", dependent: :destroy
  has_many :in_progress_courses, through: :in_progress_events, source: :course

  has_many :save_events, dependent: :destroy
  has_many :saved_courses, through: :save_events, source: :course  

  # disable comments for now
  has_many :comments, dependent: :destroy
  has_many :upvotes, dependent: :destroy

  has_many :video_submissions, dependent: :destroy

  # for reference in the nav bar when logged in.
  has_secure_token :id_token

  #has_many :high_school_notes, through: :HighSchoolSubjects, source: :notes
  # has_many :notes, through: :subjects, source: :notes

  def username=(str)
    self[:username] = str.gsub(/\s+/, '') unless str.nil?
  end

  def is_enrolled?(course_id)
      self.progress_events.exists?(course_id: course_id, is_enrolled: true)
  end

  def has_saved?(course_id)
      self.save_events.pluck(:course_id).include?(course_id)
  end

  def increment_unread_notifications_count
      self.increment!(:unread_notifications_count)
  end

  def increment_notifications_count
      self.increment!(:notifications_count)
  end

  def decrement_notifications_count
      if (self.notifications_count > 0)
        self.decrement!(:notifications_count)
      end
  end

  private

  def downcase_letters(str)
     arr = str.split();
     arr.each_with_index do |char, index|
       if (char.match?(/[A-Z]/))
          arr[index] = char.downcase
       end
     end
     return arr.join
  end
end
