class ApplicationRecord < ActiveRecord::Base
  # support image as base64 string uploads
  include ActiveStorageSupport::SupportForBase64
  self.abstract_class = true
end
