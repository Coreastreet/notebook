class VideoSubmission < ApplicationRecord
  belongs_to :assignment_video, counter_cache: :video_submissions_count, optional: true

  belongs_to :student
  belongs_to :competition

  has_one_attached :video 

  validates :student_id, uniqueness: { scope: :competition_id, message: "Student already made submission to this competition." }
  has_many :comments, as: :commentable

  has_many :notifications, as: :notifiable
  has_many :upvotes, as: :upvoteable

  validates :upvotes_count, :numericality => { :greater_than_or_equal_to => 0 }
  validates :comments_count, :numericality => { :greater_than_or_equal_to => 0 }
end
