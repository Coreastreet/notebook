class StudentMailer < ApplicationMailer
  def welcome_email
    require "base64"
    @student = params[:student]
    domain_url = params[:origin_url]
    report_account_token = params[:report_account_token]

    paramsCode = Base64.strict_encode64(@student.one_time_pin.to_s)

    @verification_url = "#{domain_url}/api/v1/accounts/#{@student.id}/verify_email?pin=#{paramsCode}"

    base64_email = Base64.strict_encode64(@student.email)
    @report_url = "#{domain_url}/api/v1/accounts/not_me?email=#{base64_email}&token=#{report_account_token}"
    attachments.inline['notebank_logo.png'] = File.read('app/assets/images/notebank_logo.png')
    mail(to: @student.email,
         subject: 'Welcome to NoteBank!')
  end

  def change_email
    require "base64"
    @email = params[:email]
    @student = params[:student]
    domain_url = params[:origin_url]

    pinParam = Base64.strict_encode64(@student.one_time_pin.to_s)
    emailParam = Base64.strict_encode64(@email)

    @verification_url = "http://#{domain_url}/api/v1/accounts/#{@student.id}/change_email?pin=#{pinParam}&email=#{emailParam}"
    attachments.inline['notebank_logo.png'] = File.read('app/assets/images/notebank_logo.png')
    mail(to: @email,
         subject: 'Request to change your Notebook email.')
  end

  def recover_username
    @username = params[:username]
    email = params[:email]
    attachments.inline['notebank_logo.png'] = File.read('app/assets/images/notebank_logo.png')
    mail(to: email,
         subject: "Your Notebook username.")
  end

  def reset_password
    domain_url = params[:domain_url]
    password_reset_token = params[:password_reset_token]
    email = params[:email]
    email_code = Base64.strict_encode64(email)

    @verification_url = "#{domain_url}/api/v1/accounts/reset_password_confirmation?email_token=#{email_code}&token=#{password_reset_token}"
    @username = params[:username]

    attachments.inline['notebank_logo.png'] = File.read('app/assets/images/notebank_logo.png')
    mail(to: email,
         subject: "Your Notebook password.")
  end
end
