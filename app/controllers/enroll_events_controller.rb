class EnrollEventsController < ApplicationController
  def destroy
    @course = Course.find_by(id_token: params[:course_token])
    if (EnrollEvent.exists?(course_id: @course.id, student_id: current_student.id))
        EnrollEvent.find_by(course_id: @course.id, student_id: current_student.id).destroy!
    end
    
    respond_to do |format|
      format.js
    end
  end

  private 
end
