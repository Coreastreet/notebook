class SaveEventsController < ApplicationController
    def create
      #student = Student.find_by(id_token: student_id_params[:id_token])
      competition = Competition.find(params[:competition_id])

      if (!SaveEvent.exists?(saveable_type: "Competition", saveable_id: competition.id, student_id: current_student&.id))
          SaveEvent.create(saveable_type: "Competition", saveable_id: competition.id, student_id: current_student&.id)
          #course.increment!(:number_of_savers)
          #course.save
          @msg = "Competition saved for later"
      else
          @msg = "Competition has already been saved"
          # like already exists.
      end
      
      respond_to do |format|
        format.js
      end
    end

    def destroy
      @competition = Competition.find(params[:competition_id])

      if (SaveEvent.exists?(saveable_type: "Competition", saveable_id: @competition.id, student_id: current_student.id))
          SaveEvent.destroy_by(saveable_type: "Competition", saveable_id: @competition.id, student_id: current_student.id)
          #course.decrement!(:number_of_savers)
          #course.save
          @msg = "Competition has been removed from the save list"
      else
          @msg = "Competition not found on the save list"
          # like already exists.
      end
      
      respond_to do |format|
        format.js
      end
    end

    def student_id_params
      params.require(:student).permit(:id_token)
    end
end
