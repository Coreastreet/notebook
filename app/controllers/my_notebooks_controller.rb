class MyNotebooksController < ApplicationController

  before_action :authenticate

  def show_course_results
      @course = Course.find_by(id_token: params[:course_id_token])
      progress = ProgressEvent.find(course_id: course.id, student_id: current_student.id)

      @mcquiz_results = progress.hash_of_quiz_results

      @module_indexes = []

      @module_quizzes = []
      subarray_quizzes = []
      mcquiz_results.each do |module_id, quiz_results|
        subarray_quizzes = []
        module_part = ModulePart.find(course.module_order[module_id.to_i])
        quiz_results.keys.each do |quiz_id|
            mc_quiz = McQuiz.find(quiz_id)
            subarray_quizzes << mc_quiz
        end
        @module_quizzes << [module_part, subarray_quizzes]
        @module_indexes << module_id.to_i
      end

      respond_to do |format|
        format.js
      end
  end

  def competitions_current
    @action = "Competitions_current"
    @my_notebook_tab = "Competition"

    tomorrow = Date.current + 1.day

    student_competition_ids = current_student.competition_events.pluck(:competition_id)
    @current_competitions = Competition.where(id: student_competition_ids, deadline_date: tomorrow...)

    @videos_submitted_array = []
    @current_competitions.each do |competition|
       @videos_submitted_array << VideoSubmission.find_by(competition_id: competition.id, student_id: current_student.id)
    end

    respond_to do |format|
      format.html
    end
  end

  def competitions_past
    @action = "Competitions_past"
    @my_notebook_tab = "Competition"

    @competition_events = current_student.competition_events
    student_competition_ids = @competition_events.pluck(:competition_id)
    # getting all comps past their deadline
    competitions = Competition.find(student_competition_ids)
    @past_competitions = []
    competitions.each{ |competition| 
          deadline_date_array = competition.deadline_date.strftime("%Y-%m-%d").split("-") 
          deadline_time_array = competition.deadline_time.strftime("%H:%M").split(":")

          local_deadline = Time.utc(deadline_date_array[0], deadline_date_array[1], deadline_date_array[2], 
                                    deadline_time_array[0], deadline_time_array[1])
          local_deadline.localtime(current_student.time_zone)

          local_deadline_date = Date.parse(local_deadline.strftime("%Y-%m-%d"))

          if (competition.deadline_date < local_deadline_date) ||
              ((competition.deadline_date == local_deadline_date) &&
              (competition.deadline_time.strftime("%H:%M") < local_deadline.strftime("%H:%M")))
              @past_competitions << competition
          end
    }

    @videos_submitted_array = []
    @past_competitions.each do |competition|
       @videos_submitted_array << VideoSubmission.find_by(competition_id: competition.id, student_id: current_student.id)
    end

    respond_to do |format|
      format.html
    end
  end

  def competitions_saved
    @action = "Competitions_saved"
    @my_notebook_tab = "Competition"

    save_events = current_student.save_events.where(saveable_type: "Competition")
    @saved_competitions = Competition.find(save_events.pluck(:saveable_id))

    @videos_submitted_array = []
    @saved_competitions.each do |competition|
       @videos_submitted_array << VideoSubmission.find_by(competition_id: competition.id, student_id: current_student.id)
    end
    
    respond_to do |format|
      format.html
    end
  end

  def courses_in_progress
    @action = "Courses_in_progress"
    @my_notebook_tab = "Course"

    @courses_in_progress = current_student.in_progress_courses

    @course_outlines = @courses_in_progress.map { |course|
        course.course_outline
    }
    
    @progress_events = @courses_in_progress.pluck(:id).map { |id| 
        ProgressEvent.find_by(student_id: current_student.id, course_id: id) 
    }

    @most_recent_lessons = @progress_events.map { |progress_event|
        most_recent_lesson = Lesson.find(progress_event.lessons_completed.max)
        resuming_lesson_id = most_recent_lesson.next_lesson
        if (resuming_lesson_id == nil) # course is completed, redirect to final page
            most_recent_lesson # the last lesson of the course.
        else 
            Lesson.find(resuming_lesson_id)
        end
    }

    @most_recent_modules = @most_recent_lessons.map { |lesson|
        lesson.module_part
    }

    respond_to do |format|
      format.html
    end
  end

  def courses_completed
    # aka a course complete button is submitted
    if (params[:complete] == "true" && params[:course_token].present?) 
        complete_course(params[:course_token])
    end
    
    @action = "Courses_completed"
    @my_notebook_tab = "Course"

    @courses_completed = current_student.completed_courses
    @first_lessons = Lesson.find(@courses_completed.map{ |c| c.lesson_order[0] })

    @course_completed_ids = @courses_completed.pluck(:id)
    @progress_events = ProgressEvent.where(student_id: current_student.id, 
                                           course_id: @course_completed_ids)

    respond_to do |format|
      format.html
    end
  end

  def saved_for_later
    @action = "Saved_for_later"
    @saved_courses = current_student.saved_courses
    
    respond_to do |format|
      format.html
    end
  end

  def basic_info
    @my_notebook_tab = "Account"

    @student = current_student

    respond_to do |format|
      format.html
    end
  end

  def password_and_email
    @my_notebook_tab = "Account"

    respond_to do |format|
      format.html
    end
  end

  def payment_details 
    @my_notebook_tab = "Payment_details"
    @billing_address = (current_student.billing_address.present?) ? current_student.billing_address : BillingAddress.new
    @bank_account = (current_student.bank_account.present?) ? current_student.bank_account : BankAccount.new

    respond_to do |format|
      format.html
    end    
  end

  def bio_and_qualifications
    @my_notebook_tab = "Account"

    respond_to do |format|
      format.html
    end
  end 

  def messages
    @my_notebook_tab = "Messages"

    respond_to do |format|
      format.html
    end
  end

  private 

  def complete_course(course_token)
      if (Course.exists?(id_token: course_token))
          course = Course.find_by(id_token: course_token)
          progress_event = ProgressEvent.find_by(course_id: course.id, student_id: current_student.id)
          if (progress_event.lessons_completed.size == course.lessons.size) # that is all lesson have been completed.
              progress_event.update!(is_complete: true)
          end

          msg_beginning = (logged_in_as_student? ? "Congratulations #{current_student.display_name}," :
          "Congratulations,")
          @flash_success = "#{msg_beginning} you completed the course --- #{course.title}" 
      end
  end
end
