class NotificationsController < ApplicationController
    def read
        notification = Notification.find(params[:id])
        if !(notification.read)
            notification.update!(read: true)
        end
        
        if (current_student.unread_notifications_count > 0)
            current_student.decrement!(:unread_notifications_count)
        end

        respond_to do |format|
            if (notification.notifiable_type == "Competition")
                if (notification.win_alert)
                    format.html { redirect_to winning_faq_url(subdomain: false) }
                else 
                    format.html { redirect_to competition_url(subdomain: false, id: notification.notifiable_id) }
                end
            else # == video_submission
                format.html { redirect_to video_submission_url(subdomain: false, id: notification.notifiable_id) }
            end
        end
    end
end 