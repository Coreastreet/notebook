class BillingAddressesController < ApplicationController
    def create
        current_student.create_billing_address(billing_address_params)

        @msg = "Billing addresses have been uploaded"
        respond_to do |format|
            format.js
        end
    end

    def update
        billing_address = current_student.billing_address
        billing_address.update!(billing_address_params)

        @msg = "Billing addresses have been updated"
        respond_to do |format|
            format.js
        end
    end

    def billing_address_params
        params.fetch(:billing_address).permit(:address_line_one, 
        :address_line_two, :suburb, :state, :postcode, :country)
    end
end