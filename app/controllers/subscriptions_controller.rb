class SubscriptionsController < ApplicationController

  # show is for displaying details of the subscription once student upgraded.
  def show
  end

  def payment_success
  end

  # create is for clicking the submit/pay button on the checkout form.
  def create
      # aka referring to myself via my square account.
      client = Square::Client.new(
          access_token: "EAAAEBDCEmrU0PdNO12GAcmOyFqNcMy43wAIMfwMCPA9HYjyefj08D5Yey281oE_",
          environment: "sandbox"
      )
      # create a subscription by sending the square card nonce to square subscriptions api.
      # 1. Create a customer - providing username, first and last name.
      # name_array = credit_card_params[:name].split(" "). Not needed since first and last name saved when account first made.

      first_result = client.customers.create_customer(
          body: {
            idempotency_key: SecureRandom.uuid,
            given_name: credit_card_params[:first_name],
            family_name: credit_card_params[:last_name],
            email_address: current_student.email
          }
      )

      if first_result.success?
          puts first_result.data
          customer_id = first_result.data.customer[:id]   

          second_result = client.cards.create_card(
              body: {
                idempotency_key: SecureRandom.uuid,
                source_id: credit_card_params[:card_nonce], # "cnon:card-nonce-ok"
                card: {
                    cardholder_name: credit_card_params[:cardholder_name],
                    #billing_address: {
                    #  address_line_1: "500 Electric Ave",
                    #  address_line_2: "Suite 600",
                    #  locality: "New York",
                    #  administrative_district_level_1: "NY",
                    #  postal_code: "94103",
                    #  country: "AU"
                    #},
                    customer_id: customer_id,
                    # i.e. use the id_token of student and the current time and
                    # dates as in db migrations to create alternate reference.
                    reference_id: "#{current_student.id_token}:#{DateTime.now.strftime('%Y%m%d%H%M')}"
                }
              }
          )

          if second_result.success?
              puts second_result.data
              card_id = second_result.data.card[:id] 

              third_result = client.catalog.upsert_catalog_object( 
                    body: {
                      idempotency_key: SecureRandom.uuid,
                      object: {
                        type: "SUBSCRIPTION_PLAN",
                        id: "#premium_plan",
                        subscription_plan_data: {
                          name: "Notebank premium membership",
                          phases: [
                            {
                              cadence: "MONTHLY",
                              recurring_price_money: {
                                amount: 999,
                                currency: "AUD"
                              }
                            }
                          ]
                        }
                      }
                    }
              )

              if third_result.success?
                  puts third_result.data
                  subscription_plan_id = third_result.data.catalog_object[:id]

                  fourth_result = client.subscriptions.create_subscription(
                      body: {
                          idempotency_key: SecureRandom.uuid,
                          location_id: "LMX6TRKH0S295", # aka the location id
                          plan_id: subscription_plan_id,
                          customer_id: customer_id,
                          card_id: card_id
                      }
                  )

                  if fourth_result.success?
                    puts fourth_result.data
                    # subscription has now been created, so update the student model.
                    current_student.update!(is_subscribed: true)

                    success = true

                  elsif fourth_result.error?
                    warn fourth_result.errors
                    @error = fourth_result.errors
                  end
                                    
              elsif third_result.error?
                warn third_result.errors
                @error = third_result.errors
              end

          elsif second_result.error?
              warn second_result.errors
              @error = second_result.errors
          end   

      elsif first_result.error?
          warn first_result.errors
          @error = first_result.errors
      end 
      
      respond_to do |format|
          if success
            format.html { render "payment_success" }
          else
            # must be an error. 
            format.js { render "payment_error" }
          end
      end
  end

  def offer # premium page
    if !logged_in_as_student?
        # if the user is a guest wants to upgrade to premium, redirect to same page after login.
        if (premium_params[:nbp] == "guest")
            cookies[:referer] = { value: request.url, domain: ".#{Notebank::DOMAIN}" }
            redirect_to login_url(subdomain: false)
        end
    else
        # if logged_in as a student, but redirected to page to access a premium course.
        # display the premium course and save course_url so to redirect back to after upgrading to premium.
        if (premium_params[:course_id_token].present? && Course.exists?(id_token: premium_params[:course_id_token]))
            @premium_course = Course.find_by(id_token: premium_params[:course_id_token])
            @course_designer = @premium_course.student
            # store for later redirect after payment successful.
            cookies[:premium_referer] = { value: course_url(course_token: premium_params[:course_id_token]), domain: ".#{Notebank::DOMAIN}" }
        end
        # so that those logged in already and those logged out with 
        # no special query can access page directly.
    end

    respond_to do |format|
        format.html
    end
  end

  # checkout is for displaying the checkout form.
  def checkout
    respond_to do |format|
      format.html
    end
  end

  def destroy
  end

  def update_card
  end

  private 

  def premium_params
      params.permit(:nbp, :course_id_token)
  end

  def credit_card_params
      params.fetch(:subscription).permit(:first_name, :last_name, :cardholder_name, :card_nonce)
  end
end
