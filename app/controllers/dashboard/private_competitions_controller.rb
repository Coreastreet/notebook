class Dashboard::PrivateCompetitionsController < Dashboard::StudentController
  layout "dashboard"

  def new
  end

  def create
    competition = Competition.new(competition_params)
    competition.student_id = current_student.id

    video_blob = ActiveStorage::Blob.find(video_key_params[:video_key])

    competition.assign_attributes(start_date: convert_to_utc_time(competition_deadline_params[:start_date]),
                                  deadline_date: convert_to_utc_time(competition_deadline_params[:deadline_date]),
                                  results_date: convert_to_utc_time(competition_deadline_params[:results_date]))

    competition.save!
    competition.video.attach(video_blob)    
    redirect_to dashboard_student_competitions_url(subdomain: "dashboard", 
                                            student_username: current_student.username)
  end

  def edit
    @competition_tab = "Details"
    @competition = Competition.find(params[:id])
    @competition_video = @competition.video
  end

  def video_submissions
    @competition_tab = "Video_submissions"
    
    @competition = Competition.find(params[:id])
    @competition_video = @competition.video 

    @total_submissions = @competition.video_submissions

    @pagy, @competition_submissions = pagy(@total_submissions, 
                    page: pagy_page_param, items: 3, link_extra: 'data-remote="true"')
  end

  def update
    @competition = Competition.find(params[:id])

    @competition.assign_attributes(competition_params)
    @competition.assign_attributes(deadline_date: convert_to_utc_time(competition_deadline_params[:deadline_date]),
                                   start_date: convert_to_utc_time(competition_deadline_params[:start_date]),
                                   results_date: convert_to_utc_time(competition_deadline_params[:results_date]))

    if (@competition.save == false) 
        @message = "Competition could not be updated!"
    else 
        @message = "Competition updated successfully!"
    end

    respond_to do |format|
      format.js
    end
  end

  def announce_winners
      competition = Competition.find(params[:id])
      results_date_array = competition.results_date.strftime("%Y-%m-%d").split("-")
      utc_results_deadline = Time.utc(results_date_array[0], results_date_array[1], results_date_array[2], 03, 25)

      submissions = competition.video_submissions.pluck(:ranking).compact
      if (([1,2,3] - submissions).empty?)
          # all 1st, 2nd and 3rd places are assigned.
          @msg = "The announcement of winners has been scheduled for #{utc_results_deadline.strftime('%d %b, %Y (%H:%M)')}"
          CompetitionAwardJob.set(wait_until: utc_results_deadline).perform_later(competition) 
      else 
          difference = [1,2,3] - submissions
          @msg = "#{difference[0].ordinalize} place has not yet been assigned."
      end

      respond_to do |format|
          format.js 
      end
  end

  def upload_video
    return true
  end

  def destroy
    @competition = Competition.find(params[:id])
    if (@competition.destroy == false)
        @message = "Competition could not be deleted"
    else 
        @message = "Competition deleted"
    end

    respond_to do |format|
      format.js
    end
  end

  def index
      @dashboard = true
      if request.url.ends_with?("/new")
        @show_new_modal = true
      end

      @competitions = current_student.competitions.order(deadline_date: :asc)

      @pagy, @created_competitions = pagy(@competitions, 
                                          page: pagy_page_param, items: 3, link_extra: 'data-remote="true"')
      #@created_competitions.map do |cc|
      #    [cc.progress_event_ids.length, cc.save_event_ids.length]
      #end
      respond_to do |format|
        format.html # html for the first load
        format.js # js for the subsequent page loads when pagy triggered.
      end
  end

  private

  def convert_to_utc_time(date_value)
      deadline_date_array = date_value.split("-")

      local_deadline = Time.new(deadline_date_array[0], deadline_date_array[1], deadline_date_array[2], 
                          23, 59, current_student.time_zone)
      utc_deadline = local_deadline.utc

      return utc_deadline.strftime("%Y-%m-%d")
  end

  def pagy_page_param
    if params["page"].nil?
      return 1
    else
      params["page"].to_i
    end
  end

  def competition_params
      params.fetch(:competition).permit(:title, :subtitle, :description, :requirements, 
                                        :prize_money)
  end

  def competition_deadline_params
      params.fetch(:competition).permit(:deadline_date, :deadline_time, :start_date, :results_date)
  end

  def video_key_params
      params.fetch(:competition).permit(:video_key)
  end
end
