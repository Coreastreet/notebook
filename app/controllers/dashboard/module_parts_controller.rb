class Dashboard::ModulePartsController < Dashboard::StudentController

  def create
    # must upload a picture to ensure a module is created.
    course = Course.find_by(id_token: params[:course_id_token])

    @new_module = ModulePart.new(title: module_params[:title],
                                     subtitle: module_params[:subtitle], 
                                     course_id: course.id)
    @new_module.image.attach(module_params[:image])
    @new_module.save

    @module_count = (course.module_order.length + 1) - 2  
    # add one to account for new module
    # remove two to account for the intro and summary.

    respond_to do |format|
      format.js
    end
  end

  def show
  end

  def update # mainly just update the name
    @module_part = ModulePart.find(params[:module_id])
    module_part_image = @module_part.image
    if module_params[:image]
       module_part_image.attach(module_params[:image])
    end
    @module_part.update(title: module_params[:title], subtitle: module_params[:subtitle])

    @module_image = module_part_image

    respond_to do |format|
      format.js
    end
  end

  def destroy
    module_part = ModulePart.find(params[:module_id])    
    course = module_part.course
    module_part.destroy!
    
    # use as variable to get the updated counters
    @course = Course.find(course.id)

    respond_to do |format|
      format.js
    end
  end

  def module_params
      params.fetch(:module_part).permit(:title, :subtitle, :image)
  end
end
