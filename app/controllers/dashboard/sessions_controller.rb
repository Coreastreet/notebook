class Dashboard::SessionsController < Dashboard::StudentController
    
    def destroy
        # destroy session from the dashboard subdomain    
        cookies.delete(:auth_token, domain: ".#{Notebank::DOMAIN}")

        # destroy so that next time login, you go to dashboard home if so desired.
        cookies.delete(:referer, domain: ".#{Notebank::DOMAIN}" }
        
        current_student = nil
        redirect_to root_url(subdomain: false)
    end
end