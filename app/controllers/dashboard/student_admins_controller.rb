class Dashboard::StudentAdminsController < Dashboard::StudentController
  layout "dashboard"

  def profile
    @student = Student.find_by(username: student_username_params[:student_username])
    respond_to do |format|
      format.html
    end
  end

  def edit
  end

  # all update actions for student

  def update_photo
    @student = Student.find_by(username: student_username_params[:student_username])
    @student.image.attach(student_image_params[:image])

    @message = "Profile photo updated successfully"
    @image = @student.image
    respond_to do |format|
      format.js
    end
  end

  def update_display_name
    current_student.update!(display_name: display_name_params[:display_name])
    @message = "Display name updated successfully"
    respond_to do |format|
      format.js
    end
  end

  def destroy
  end

  def show_payment_history
  end

  private

      def display_name_params
        params.fetch(:student).permit(:display_name)
      end

      def student_image_params
        params.fetch(:student).permit(:image)
      end

      # Find the user that owns the access token

      def student_username_params
        params.permit(:student_username)
      end

end
