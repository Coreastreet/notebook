class Dashboard::PrivateCoursesController < Dashboard::StudentController
  layout "dashboard"

  def courses # index for the student's own uploads
    @dashboard = true
    if request.url.ends_with?("/new")
      @show_new_modal = true
    end
    @pagy, @created_courses = pagy(current_student.created_courses, page: pagy_page_param, items: 3, link_extra: 'data-remote="true"')
    @created_courses.map do |cc|
        [cc.progress_event_ids.length, cc.save_event_ids.length]
    end
    respond_to do |format|
      format.html # html for the first load
      format.js # js for the subsequent page loads when pagy triggered.
    end
  end

  def comments

  end

  def load_slider_notes
       #convert string of array token ids into a 2d array with sub-array length of 3 max.
    # use zero-base index
    array_of_page_tokens = load_slider_notes_params[:slider_notes_array].split(",")
                                .each_slice(3).to_a
    @current_slider_index = load_slider_notes_params[:slider_page_id].to_i

    @note_type = load_slider_notes_params[:note_type]

    # make sure index is greater than or equal to 0.
    if (@current_slider_index >= 0)
      slider_notes = Note.where(id_token: array_of_page_tokens[@current_slider_index])

      # get the index for the all the other notes to be displayed.
      # values depend on which action (next or previous is taken).
      slider_notes_indicies = [*(@current_slider_index * 3)..(@current_slider_index * 3 + 2)]
                                  .slice(0, slider_notes.length)
      # combine the indicies and the note objects for easier iteration.
      @hash_of_next_slider_notes = Hash[slider_notes_indicies.zip(slider_notes)]
    end

    respond_to do |format|
      format.js
    end
  end

  def show
  end

  def edit
    @course_tab = "Details"
    @course = Course.find_by(id_token: params[:course_id_token])
  end

  def design
    @course_tab = "Designer"
    @course = Course.find_by(id_token: params[:course_id_token])

    @new_module = ModulePart.new

    @course_modules = ModulePart.find(@course.module_order[1...-1])

    @array_of_module_lessons = []

    @course_modules.each do |course_module|
        @array_of_module_lessons << Lesson.find(course_module.lesson_order)
    end
  end

  def intro_summary
    @course_tab = "Intro_and_summary"
    @course = Course.find_by(id_token: params[:course_id_token])

    @intro_lesson = Lesson.find(@course.lesson_order[0])
    @intro_learnable = @intro_lesson.learnable

    @summary_lesson = Lesson.find(@course.lesson_order[-1])
    @summary_learnable = @summary_lesson.learnable
  end

  def update
    course = Course.find_by(id_token: params[:course_id_token])
    if course.update!(course_params)
      @message = "Your course details have successfully been updated"
    else
      @message = "Your course details could not be updated"
    end
    respond_to do |format|
      format.js
    end
  end

  def update_lesson_order
    module_part = ModulePart.find_by(module_part: params[:course_id_token])
    module_part.update!(lesson_order: lesson_order_params[:module_order], 
                        lesson_types: lesson_type_params)
    
    # update the course with a hash with its lesson ids as key and lesson type as the value.
    @message = "Your lesson order has been updated."

    respond_to do |format|
      format.js
    end
  end

  def update_module_order
    course = Course.find_by(id_token: params[:course_id_token])

    module_order = course.module_order
    new_module_order = [module_order[0]] + course_order_params[:module_order] + [module_order[-1]]
    
    course_lesson_order = ModulePart.find(new_module_order).map do |module_part|
        module_part.lesson_order
    end
    course_lesson_order.flatten!
      
    course.update!(module_order: new_module_order, lesson_order: course_lesson_order)
    # update the course with a hash with its lesson ids as key and lesson type as the value.
    @message = "Your module order has been updated."

    respond_to do |format|
      format.js
    end
  end

  def delete
    @upload = Note.find_by(id_token: params[:upload_id_token])
    respond_to do |format|
      format.js # {render layout: false} not needed.
    end
  end

  def delete_multiple
    respond_to do |format|
      format.js # {render layout: false} not needed.
    end
  end

  def destroy
    @upload_id_token = params[:upload_id_token]
    upload = Note.find_by(id_token: @upload_id_token)
    if (current_student.id == upload.student_id)
      # meaning that the note file belongs to the current student logged in.
      # preventing another logged in user from trying to delete another users notes.
      if upload.destroy! #
          if request.url.ends_with?("/uploads")
              @message = "#{upload.title} has been successfully deleted."
          else
              flash[:snackbar] = "#{upload.title} has been successfully deleted."
              redirect_to "http://dashboard.#{Notebank::DOMAIN}:3000/students/#{current_student.username}/uploads"
          end
      else
        @message = "An error occurred with the deletion."
      end

    end
    respond_to do |format|
      format.js
    end
  end

  def new # launch upload modal for new notes
    @hsc_subjects = hash_of_courses
    @show_new_modal = true
    respond_to do |format|
      #format.html
      format.js {render "new", locals: { hsc_courses: hash_of_courses }}
    end
  end

  def create
    basic_course_params[:subject].gsub!(" ", "_")

    @new_course = Course.new(basic_course_params)
    @new_course.student_id = current_student.id

    @active_storage_blob = ActiveStorage::Blob.where(filename: file_params[:file_name],
                                                    byte_size: file_params[:file_size].to_i,
                            created_at: (Time.current - 1.hour)..(Time.current)).first
    #duplicate_count = ActiveStorage::Blob.where(checksum: @active_storage_blob&.checksum).count - 1
    # ignore whether the image is a duplicate or not, since a determined people will slightly alter the picture any way.
    # thus, be able to evade the detection and post copied pictures anyway.

    respond_to do |format|
        # require that the uploaded picture is unique and is of image type.
        if (["image/jpeg", "image/jpg", "image/png", "image/webp"]
          .include?(@active_storage_blob.content_type))

            @new_course.image.attach(@active_storage_blob)
            @new_course.save!

            format.js {render "dashboard/private_courses/new_notes_success", locals: {course: @new_course}}

        else
            #if (duplicate_count > 0) # i.e. if the user has uploaded the same file once or more in the past 24 hours.
            # @error_message = "This image has already been uploaded to this website. 
            #                    Please choose another one."
            if ( ["image/jpeg", "image/jpg", "image/png", "image/webp"]
                     .exclude?(@active_storage_blob.content_type) )
              @error_message = "The content type of the file must be an image."
            else
              @error_message = "Unknown error. Failed to upload."
            end
            # at least one of the conditions has failed.
            @active_storage_blob.purge # since the uploaded image is not unique..

            format.js {render "dashboard/private_courses/new_notes_error"}
        end
    end
  end
=begin 
  this is old create action used for processing the pdf upload. May be useful later for pdf model.
  def create # submit details and save to student
    note_params[:course].gsub!(" ", "_")
    @new_note = Note.new(note_params)
    @active_storage_blob = ActiveStorage::Blob.where(filename: file_params[:file_name],
                                                    byte_size: file_params[:file_size].to_i,
                            created_at: (Time.current - 1.hour)..(Time.current)).first
    duplicate_count = ActiveStorage::Blob.where(checksum: @active_storage_blob&.checksum).count - 1
    # one blob should exist and so duplicate count is 0 in normal case. 
    # If previously uploaded, more than one blob is found and so duplicate count is 1 or greater
    # deal with duplicates later presumably by checking checksum and byte_size
    # ActiveStorage::Blob.unattached.each(&:purge)
    respond_to do |format|
      # make sure the file sent is less than 5MB
      if ((@active_storage_blob.byte_size < 5000000) && (duplicate_count == 0) &&
          (["application/pdf","application/vnd.openxmlformats-officedocument.wordprocessingml.document"]
              .include?(@active_storage_blob.content_type)))

            matchdata = @active_storage_blob.filename.to_s.match(/^(.+)(\.pdf|\.docx?)$/)

        if (@active_storage_blob.content_type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")# &&
            
            @active_storage_blob.open do |temp_file|
              Libreconv.convert(temp_file.path, "/tmp/#{matchdata[1]}.pdf", "/usr/lib/libreoffice/program/soffice")
            end
            # attach manually not directly because it allows conversion to pdf.
            @new_note.pdf.attach(io: File.open("/tmp/#{matchdata[1]}.pdf"),
                                 filename: "#{matchdata[1]}.pdf", content_type: 'application/pdf')

            # insert pdf_sample code after successful testing in the pdf type section.
            upload_type_docx = true
        else # must be ((@active_storage_blob.content_type == "application/pdf") by deductive logic
            @new_note.pdf.attach(@active_storage_blob)
            upload_type_docx = false
        end
        @new_note.save!

        logger.debug "start generating pdf_sample in background"
        NoteWorker.perform_async(@new_note.id, upload_type_docx, matchdata[1])

        format.js {render "dashboard/private_notes/new_notes_success"} #locals: {note: @new_note}}
      else
        if (duplicate_count > 0) # i.e. if the user has uploaded the same file once or more in the past 24 hours.
          @error_message = "This file has already been uploaded to this website"
        elsif (@active_storage_blob.byte_size > 5000000)
          @error_message = "The file size exceeds the limit of 5MB."
        elsif (["application/pdf","application/vnd.openxmlformats-officedocument.wordprocessingml.document"]
                  .exclude?(@active_storage_blob.content_type))
          @error_message = "The content type of the file must be either .pdf or .docx"
        else
          @error_message = "Unknown error. Failed to upload."
        end
        # at least one of the conditions has failed.
        @new_note.destroy!
        @active_storage_blob.purge # since the original uploaded file is not valid.

        format.js {render "dashboard/private_notes/new_notes_error"}
      end
    end
  end
=end

  def my_drive_slider(notes_full)
     
     notes_id_token_array = notes_full.pluck(:id_token)
     notes = notes_full.first(3)
     notes_length = (notes_full.count/3.0).ceil * 3
     notes_remaining_indicies = [*3..(notes_length - 1)]

     return notes_id_token_array, notes, notes_length, notes_remaining_indicies

  end

  private

  def pagy_page_param
    if params["page"].nil?
      return 1
    else
      params["page"].to_i
    end
  end

  def basic_course_params
    params.fetch(:course)
        .permit(:title, :subject, :difficulty_level, :description)
  end

  def course_order_params
    params.fetch(:course).permit(module_order: [])
  end

  def file_params
    params.fetch(:course).permit(:file_name, :file_size)
  end

  def course_params
    params.fetch(:course)
        .permit(:subject, :title, :subtitle, { objectives: [] },
                :description, :difficulty_level, :course_outline, :is_premium) 
                #:has_class_limit, :max_class_size, 
                #:has_assignment_marking, :assignment_marking_charge
  end

  def load_slider_notes_params
    params.fetch(:note).permit(:slider_page_id, :slider_notes_array, :note_type)
  end
end
