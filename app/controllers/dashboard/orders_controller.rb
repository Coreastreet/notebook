class Dashboard::OrdersController < Dashboard::StudentController
  def new
  end

  def create # make a purchase. Only users with an account can purchase.

  end

  def destroy # give the user a refund if possible.
  end

  def index
  end

  private 

  def order_params
      params.fetch(:order).permit(:card_nonce, :billing_address)
  end


end
