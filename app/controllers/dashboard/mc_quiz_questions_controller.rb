class Dashboard::McQuizQuestionsController < Dashboard::StudentController
    layout "design_mc_quiz", only: [:edit]

    def create
      @mc_quiz_question = McQuizQuestion.create!(title: quiz_question_params[:title],
                        mc_quiz_id: quiz_question_params[:mc_quiz_id], 
                        number_of_choices: quiz_question_params[:number_of_choices],
                        correct_answer_index: quiz_question_params[:correct_answer_index], 
                        question: quiz_question_params[:question], 
                        explanation: quiz_question_params[:explanation],
                        options: quiz_question_params[:options])

      mc_quiz = @mc_quiz_question.mc_quiz
      
      @mc_quiz_question_order = mc_quiz.question_order
      new_question_order = @mc_quiz_question_order.push(@mc_quiz_question.id)

      mc_quiz.update(question_order: new_question_order) 
                        
      respond_to do |format|
        format.js
      end
    end

    def edit
      @mc_quiz_question = McQuizQuestion.find(params[:id])
      # for the breadcrumb
      mc_quiz = @mc_quiz_question.mc_quiz
      mc_quiz_question_order = mc_quiz.question_order
      @questions = McQuizQuestion.find(mc_quiz_question_order)

      @lesson = mc_quiz.lesson
      @module = @lesson.module_part

      @question_position = mc_quiz_question_order.index(@mc_quiz_question.id) + 1
      @lesson_position = @module.lesson_order.index(@lesson.id)
      @module_position = @module.module_order_index

      respond_to do |format|
        format.html
      end
    end

    def update
      @mc_quiz_question = McQuizQuestion.find(params[:id])

      @mc_quiz_question.update!(title: quiz_question_params[:title], 
                                number_of_choices: quiz_question_params[:number_of_choices],
                                correct_answer_index: quiz_question_params[:correct_answer_index], 
                                question: quiz_question_params[:question], 
                                explanation: quiz_question_params[:explanation],
                                options: quiz_question_params[:options])
      respond_to do |format|
        format.js
      end
    end

    def destroy
      mc_question = McQuizQuestion.find(params[:id])
      mc_quiz = mc_question.mc_quiz
      
      mc_quiz.update!(question_order: (mc_quiz.question_order - [mc_question.id]))
      mc_question.destroy!

      respond_to do |format|
        format.js
      end
    end

    def quiz_question_params
      params.fetch(:mc_quiz_question).permit(:title, :mc_quiz_id, :number_of_choices, 
                   :correct_answer_index, :question, :explanation, options: [])
    end
end
