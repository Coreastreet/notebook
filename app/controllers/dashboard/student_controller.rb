class Dashboard::StudentController < ApplicationController
    #no other methods imported since necessary methods already present in the main application controller.        
    # check that student is logged before any webpage on the dashboard subdomain is accessed.
    before_action :authenticate
end