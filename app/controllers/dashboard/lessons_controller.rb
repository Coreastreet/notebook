class Dashboard::LessonsController < Dashboard::StudentController
    layout "design_lesson"
     # create is responsible for saving new lessons and saving the rearrangement of lesson order.
    def create
        @module_id = params[:module_id]
        module_part = ModulePart.find(params[:module_id])
        course = module_part.course     

        current_lesson_order = module_part.lesson_order
        new_lesson_order = new_lesson_params[:lesson_type_order]
        # combine the values of lesson_ids and types into a single string and check for a letter.
        # true if a new lesson was added and false if not.  
        new_lessons_added = !(/[a-zA-Z]/.match(new_lesson_order.reduce(:+)).nil?)
        @new_lesson_counter = 0

        if (new_lessons_added)
            @new_edit_lesson_urls = []
            @lesson_ids = []
                
            new_lesson_order.map! do |string|
                    # iterate over the array of lesson ids to find the new lesson
                    if (/[1-9]/.match(string).nil?) # match is nil means it must be the new lesson inserted.
                        # if a new lesson was inserted, then create a new lesson at that index.
                        lesson_type = string.split("-").map(&:capitalize).join
                        lesson = Lesson.create!(learnable: lesson_type.constantize.new(), module_part_id: params[:module_id])
                        # course lesson type counter increased by 1 after the save.

                        @new_edit_lesson_urls << edit_dashboard_lesson_url(lesson_id: lesson.id, subdomain: "dashboard")
                        @lesson_ids << lesson.id

                        @new_lesson_counter += 1
                        lesson.id
                    else 
                        # convert string lesson id to integer
                        string.to_i
                    end
            end
            module_part.update!(lesson_order: new_lesson_order)
            # once module order has been saved
            # update the course to reflect the change in that new module order
            course_lesson_order = ModulePart.find(course.module_order).map do |module_part|
                module_part.lesson_order
            end
            course_lesson_order.flatten!

            #update both the lesson order array and the lesson length
            course.update(lesson_order: course_lesson_order, 
                        lessons_count: course_lesson_order.length)
            # update_lessons_count
        else # no new lessons added. Thus lessons were only rearranged.
            # convert elements to int
            new_lesson_order.map!(&:to_i)
            if (new_lesson_order == current_lesson_order)
                # do nothing since no lesson has been rearranged.
                # same number of lessons and same lesson order
            else     
                # new lesson rearrangement has been updated to the module.
                # or some lessons have been deleted.
                module_part.update!(lesson_order: new_lesson_order)         
                # @course.update_lessons_count, since the lesson count may be lower now.   
            end
        end

        @lessons_count = new_lesson_order.length
        @course = Course.find(course.id)

        respond_to do |format|
            format.js
        end
    end

    def edit
      @lesson = Lesson.find(params[:lesson_id])    
      @learnable = @lesson.learnable
      @lesson_type = @learnable.class.to_s.underscore.to_sym

      if (@lesson_type == :lesson_video)
          if (@learnable.video.attached?)
              @video_attached = true
              @video = @learnable.video
          end
      elsif (@lesson_type == :mc_quiz)
          @mc_quiz_questions = McQuizQuestion.find(@learnable.question_order)
      end

      @module = @lesson.module_part
      module_lesson_order = @module.lesson_order
      @module_lessons = Lesson.find(module_lesson_order)

      @course = @module.course
      @module_position = @course.module_order.index(@module.id)
      @lesson_position = module_lesson_order.index(@lesson.id)

      respond_to do |format|
        format.html { render "/dashboard/lessons/lesson_type_edits/#{@lesson_type.to_s}_edit" }
      end
    end

    def update
        lesson = Lesson.find(params[:lesson_id])
        learnable = lesson.learnable

        lesson_type = learnable.class.to_s
        @msg = "#{lesson_type} successfully updated!"

        case lesson_type
        when "LessonText"
            learnable.update!(content: lesson_text_params[:content])
            has_content = (lesson_text_params[:content].present?) ? true : false
        when "LessonVideo"
            video = ActiveStorage::Blob.find_by(id: lesson_video_params[:video_key])
            if video.present?
                learnable.video.purge
                learnable.video.attach(video) 
                @msg = "Video successfully uploaded!"
            end
        when "McQuiz"
            if (params.fetch(:lesson).has_key?(:mc_quiz))
                learnable.update!(question_order: lesson_mc_quiz_params[:question_order])
                has_content = true
            else 
                has_content = false 
            end
        when "AssignmentVideo"
            deadline_time = assignment_video_params[:deadline_date] + " " + assignment_video_params[:deadline_time] 
            if (Time.parse(deadline_time).utc < Time.current)
                @msg = "Deadline must not be earlier than the current time"
            else
                learnable.update!(assignment_video_params)            
            end
        end
        if (lesson_params[:title].present? && (lesson_params[:title] != lesson.title))
            lesson.update!(title: lesson_params[:title], has_content: has_content)
        end 
        respond_to do |format|
          format.js
        end
    end

    def update_intro_summary # update either summary or the intro, depending on the lesson id.
        lesson = Lesson.find(params[:lesson_id])
        
        lesson.update!(title: lesson_params[:title])
        lesson.learnable.update!(content: lesson_text_params[:content])

        @message = (lesson_params[:type] == "intro") ? "Introduction" : "Summary" 

        respond_to do |format|
            format.js
        end
    end


    def destroy
        @lesson_id = params[:lesson_id]
        lesson = Lesson.find(params[:lesson_id])

        lesson.destroy!  
        # use as variable to get the updated counters
        @module_part = lesson.module_part
        @course = @module_part.course

        respond_to do |format|
          format.js
        end
    end

    def new_lesson_params
        params.fetch(:module_part).permit(lesson_type_order: [])
    end

    def lesson_params
        params.fetch(:lesson).permit(:title, :type)
    end

    def lesson_text_params
        params.fetch(:lesson).fetch(:lesson_text).permit(:content)
    end

    def lesson_mc_quiz_params
        params.fetch(:lesson).fetch(:mc_quiz).permit(question_order: [])
    end

    def lesson_video_params
        params.fetch(:lesson).permit(:video_key)
    end

    def assignment_video_params
        params.fetch(:lesson).fetch(:assignment_video).permit(:requirements, :prize_money, 
                                               :deadline_date, :deadline_time)
    end
end
