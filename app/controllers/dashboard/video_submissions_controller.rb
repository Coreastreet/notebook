class Dashboard::VideoSubmissionsController < Dashboard::StudentController

    def rate
        submission = VideoSubmission.find(params[:id])

        if (video_submission_params[:ranking].empty?)
            submission.assign_attributes(ranking: nil, rating: video_submission_params[:rating])
        else 
            submission.assign_attributes(video_submission_params)
        end

        if (submission.save)
            @msg = "Submission has been rated #{submission.rating}"
        else
            @msg = "Submission could not be rated for some reason"        
        end

        respond_to do |format|
            format.js 
        end
    end

    private

    def video_submission_params
        params.fetch(:video_submission).permit(:rating, :ranking)
    end

end