class Learning::LessonsController < Learning::StudentsController
    
    before_action except: [:update_mc_quiz_answer] do 
        load_layout unless (request.xhr?) 
    end
        
    def show
        # depending on learnable type, call learnable different type 
        # and return different content.
        render_page = params[:learnable_type].gsub("-", "_")

        case params[:learnable_type]
        when "lesson-text"
            @lesson ||= Lesson.find_by(learnable_type: "LessonText", 
                                       learnable_id: params[:learnable_id])
            @lesson_text = (@learnable || @lesson.learnable)
        when "lesson-video"
            @lesson ||= Lesson.find_by(learnable_type: "LessonVideo", 
                                       learnable_id: params[:learnable_id])
            @lesson_video = (@learnable || @lesson.learnable)
            @video = @lesson_video.video
        when "mc-quiz" # no question_id is expected
            @lesson ||= Lesson.find_by(learnable_type: "McQuiz", 
                                       learnable_id: params[:learnable_id])

            @mc_quiz = (@learnable || @lesson.learnable)
        when "assignment-video"
            @lesson ||= Lesson.find_by(learnable_type: "AssignmentVideo", 
                                       learnable_id: params[:learnable_id])
            @assignment_video = (@learnable || @lesson.learnable)

            @video_submissions = @assignment_video.video_submissions
        else
        end

        @continued = (params[:continue] == "true") ? true : false
        @learnable_id = params[:learnable_id]
        @learnable_type = @lesson.learnable_type
        # update the progress_event
        if (request.xhr?) # ajax run, so no progress event present in controller
            course ||= Course.find_by(id_token: params[:course_id_token])
            @progress_event ||= ProgressEvent.find_by(student_id: current_student.id,
                    course_id: course.id)

            if (@continued) 
                # assign 4 values: most recent lesson_id, most recent module_id, lessons_completed and progress_percent
                # ignore the values of hash quiz answers and hash recent quiz questions, modules_completed.
                new_lessons_completed_array = @progress_event.lessons_completed | [@progress_event.most_recent_lesson_id]
                new_progress_percent = (new_lessons_completed_array.length * 100.0) / course.lessons_count   

                @progress_event.assign_attributes(most_recent_lesson_id: @lesson.id, 
                                        lessons_completed: new_lessons_completed_array,
                                        progress_percent: new_progress_percent)
            else 
                @progress_event.assign_attributes(most_recent_lesson_id: @lesson.id)
            end
        end
        @progress_event.save!

        respond_to do |format|
            if (request.xhr?) # ajax request
                format.js { render "learning/lessons/ajax_load/#{render_page}" }
            else 
                format.html { render "#{render_page}" }
            end
        end
    end

    def show_quiz_question # i.e a question is given in the url.
        @course ||= Course.find_by(id_token: params[:course_id_token])
        @progress_event ||= ProgressEvent.find_by(course_id: @course.id, 
                                               student_id: current_student.id)
        @question_index = params[:question_id].to_i

        @learnable_id = params[:learnable_id]
        @learnable_type = "McQuiz"
        @continued = (params[:continue] == "true") ? true : false

        @mc_quiz = (@learnable || McQuiz.find(@learnable_id))

        if ((@question_index > 0) && (@question_index <= @mc_quiz.mc_quiz_questions_count))

            @quiz_question = McQuizQuestion.find(@mc_quiz.question_order[@question_index - 1])
            # get the question by the index provided
            quiz_question_id = @quiz_question.id.to_s # key is string

            @question_answered = @progress_event.hash_of_quiz_answers.has_key?(quiz_question_id)
            recent_quiz_questions_hash = @progress_event.most_recent_quiz_question_index
            recent_quiz_questions_hash[@mc_quiz.id] = @question_index
            # quiz_answer = McQuizAnswer.find_by(mc_quiz_id: @mc_quiz.id, student_id: @course.student_id)
            @lesson ||= @mc_quiz.lesson
            if request.xhr? # progress_event must be absent
                # progress event not updated
                # progress_event defined though. Skip definition
                lessons_completed_array = @progress_event.lessons_completed | [@progress_event.most_recent_lesson_id]
                new_progress_percent = (lessons_completed_array.length * 100.0) / @course.lessons_count      

                if (@continued)
                    # ajax with continue, same as below.
                    # however, only on continue = true do we update 
                    # the lessons completed hash and the progress_percent.
                    @progress_event.assign_attributes(most_recent_quiz_question_index: recent_quiz_questions_hash,
                                           most_recent_lesson_id: @lesson.id,
                                           lessons_completed: lessons_completed_array, 
                                           progress_percent: new_progress_percent)
                
                else
                    # ajax without continue, update only recent lesson and recent quiz fields
                    @progress_event.assign_attributes(most_recent_lesson_id: @lesson.id,
                                           most_recent_quiz_question_index: recent_quiz_questions_hash)
                end
                # omit module_count
                # hash_of_quiz_answers
            
            else # request is not remote; progress_event is present and is already updated
                # however still need to update progress with the most recent question index.
                @progress_event.assign_attributes(most_recent_quiz_question_index: recent_quiz_questions_hash)
            end
            @progress_event.save!
            
            if (@question_answered) # if answers present aka. question has been answered previously.
                # get the answer option that was previously chosen.
                @user_answer_index = @progress_event.hash_of_quiz_answers[quiz_question_id]
            else 
                @user_answer_index = nil
            end
            @question_order = @mc_quiz.question_order
            @questions_completed = @progress_event.hash_of_quiz_answers.keys.map(&:to_i) & 
                                       @question_order

            render_page = "mc_quiz_question"
        else
            # for clicks on left column, this will never run ajax.
            render_page = "mc_quiz"
        end


        respond_to do |format|
            if (request.xhr?) # ajax request
                format.js { render "learning/lessons/ajax_load/#{render_page}" }
            else
                # get a record of all previously completed questions.
                format.html { render "#{render_page}" }
            end
        end
    end
    
    def update_mc_quiz_answer  
        @course = Course.find_by(id_token: params[:course_id_token])
        @module_index = @course.module_order.index(mc_quiz_answer_params[:module_id].to_i)

        @progress_event = ProgressEvent.find_by(student_id: current_student.id, course_id: @course.id)
        @quiz_question = McQuizQuestion.find(params[:learnable_id])

        @user_answer_index = mc_quiz_answer_params[:hash_array_answer].to_i    
    
        hash = @progress_event.hash_of_quiz_answers
        hash2 = @progress_event.hash_of_quiz_results
        quiz_result_present = hash2[@module_index.to_s]&.has_key?(@quiz_question.mc_quiz_id.to_s)

        # store the users response to the question
        hash.store(@quiz_question.id, @user_answer_index)        

        if (@quiz_question.correct_answer_index == @user_answer_index) # answer is correct
            number_of_correct_answers = quiz_result_present ? 
                                        hash2[@module_index.to_s][@quiz_question.mc_quiz_id.to_s] + 1 : 1
            hash2.store(@module_index, {@quiz_question.mc_quiz_id => number_of_correct_answers})
        else
            hash2.store(@module_index, {@quiz_question.mc_quiz_id => 0}) if !quiz_result_present
        end
        @progress_event.update!(hash_of_quiz_answers: hash, hash_of_quiz_results: hash2)

        respond_to do |format|
            format.js
        end
    end

    private

    def mc_quiz_answer_params
        params.fetch(:mc_quiz_answers).permit(:hash_array_answer, :module_id)
    end

    def get_main_variables # aka learnable, lesson, module, course
        lesson_type = params[:learnable_type].split("-").map(&:capitalize).join().constantize()
        @learnable = lesson_type.find(params[:learnable_id])

        @lesson = @learnable.lesson 
        @module = @lesson.module_part
        @course = Course.find_by(id_token: params[:course_id_token])

        return @learnable, @lesson, @module, @course
    end

    def load_layout 
        @learnable, @lesson, @module, @course = get_main_variables
        load_lesson_layout(@course, @module, @lesson)
        @progress_event, @module_progress = record_progress(@course, @module, @lesson)
    end

    def load_lesson_layout(course, module_part, lesson) 
        # 1. Get lesson data for right column index
        # 2. Get the prev & next module and their respective lessons for reference.
        # 3. Get the prev & next lesson - aka lesson 'triad'
        # 4. Get the first lesson of next and prev module, if they exist.
        @course_designer = @course.student

        @index_list_of_course = []
        ModulePart.find(course.module_order).each do |module_part|
            @index_list_of_course << [module_part.id, module_part.title, Lesson.find(module_part.lesson_order)]
        end
        # first, get the module triad, for the learning navbar.
        # index of current module
        @course_module_index = course.get_module_index(module_part.id) # 1-based
        if (@course_module_index > 0) 
            @prev_module_index = @course_module_index - 1 
            prev_module_lessons = @index_list_of_course[@prev_module_index][2]
        else 
            @prev_module_index = nil
            prev_module_lessons = nil
        end

        if (@course_module_index < (course.module_order.length - 1))
            @next_module_index = @course_module_index + 1 
            next_module_lessons = @index_list_of_course[@next_module_index][2]
        else 
            @next_module_index = nil
            next_module_lessons = nil
        end
                              # third, get the lesson triad, for the flex bar lessons.
        # avoid reloading all the lessons        
        current_module_lessons = @index_list_of_course[@course_module_index][2]
        @lesson_index = module_part.get_lesson_index(lesson.id)
        @current_lesson_triad = current_module_lessons.each_slice(3).to_a.at((@lesson_index/3)) 
        # missing the icons

        if (@prev_module_index.nil? || (prev_module_lessons.length == 0))
            # either prev module doesn't exist or is empty of lessons
            @prev_module_first_lesson = nil
            @prev_lesson = (lesson.id == module_part.lesson_order[0]) ? 
                            nil : current_module_lessons[@lesson_index - 1]
        else 
            @prev_module_first_lesson = prev_module_lessons[0]
            @prev_lesson = (lesson.id == module_part.lesson_order[0]) ? 
                            prev_module_lessons[-1] : current_module_lessons[@lesson_index - 1]
        end

        @last_lesson_of_module = (lesson.id == module_part.lesson_order[-1])
        if (@next_module_index.nil? || (next_module_lessons&.length == 0)) 
            # either next module doesn't exist or is empty of lessons
            @next_module_first_lesson = nil
            @next_lesson = @last_lesson_of_module ? 
                        nil : current_module_lessons[@lesson_index + 1]
        else 
            @next_module_first_lesson = next_module_lessons[0]
            @next_lesson = @last_lesson_of_module ? 
                        next_module_lessons[0] : current_module_lessons[@lesson_index + 1]
        end
    end

    def update_progress(course, progress_event)
        new_lessons_completed_array = progress_event.lessons_completed | [progress_event.most_recent_lesson_id]
        new_progress_percent = (new_lessons_completed_array.length * 100.0) / course.lessons_count      
        # if not, add that lesson to the progress array to indicate it is completed
        # also update the course progress percent field, which may have increased with the new lesson.
        progress_event.assign_attributes(lessons_completed: new_lessons_completed_array, 
                                         progress_percent: new_progress_percent)
        prev_module = ModulePart.find(progress_event.most_recent_module_id)

        if (prev_module.lesson_order - new_lessons_completed_array).empty?
            # this means all the lessons in module lesson order is contained within thecompleted lessons completed as progress.
            progress_event.assign_attributes(modules_completed: (progress_event.modules_completed | [prev_module.id]) )
        end
        # progress_event.save! save progress_event later to avoid double update.
        return progress_event
    end

    def get_progress_event(course)
        is_enrolled = current_student.is_enrolled?(course.id)
        # check if the student is enrolled
        if is_enrolled
            # already enrolled, so return progress_event 
            progress_event = ProgressEvent.find_by(student_id: current_student.id, course_id: course.id)
            return progress_event
        else 
            if (params[:enrol] == "true")
                # create both the enrolment event and the progress tracker.

                # first case is if the student has unenrolled and now re-enrolled into the course.
                progress_event = ProgressEvent.find_by(course_id: course.id, 
                student_id: current_student.id, is_enrolled: false)

                # second case, no progress tracker exists since the enroller is a new student to the course.
                if (progress_event.nil?)
                    progress_event = ProgressEvent.new(course_id: course.id, 
                    student_id: current_student.id, is_enrolled: true)
                else
                    progress_event.assign_attributes(is_enrolled: true)
                end 

                course.increment!(:number_of_enrolments)

                return progress_event
            else 
                # not enrolled and did not click to enrol button, so redirect to enrollment page
                redirect_to course_url(course_token: course.id_token)
                return false
            end
        end
    end

    def record_progress(course, module_part, lesson)
        # 1. get the progress event
        # 2. if continue was clicked, add the previous lesson to progress event record.
        # 3. Either way, get the current module progress at the current stage.
        progress_event = get_progress_event(course)
        if (progress_event == false) # neither enrolled or beginning to enrol.
            return false
        end
        # we want to check the completed lesson array for the presence of the lesson we just finished.
        if ((params[:continue] == "true") &&   
            progress_event.lessons_completed.exclude?(progress_event.most_recent_lesson_id))
            # don't update progress if module or lesson has already been completed before
            progress_event = update_progress(course, progress_event)
        end
        progress_event.assign_attributes(most_recent_lesson_id: lesson.id, 
                                most_recent_module_id: lesson.module_part_id)
        # progress_event.save!
        # save later in the main controller action
        if (progress_event.modules_completed.include?(module_part.id))
            module_progress = 100
        else
            module_progress = ((module_part.lesson_order & 
            progress_event.lessons_completed).length * 100.0) / module_part.lessons_count
        end

        return progress_event, module_progress
    end
end
