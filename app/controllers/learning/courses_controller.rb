class Learning::CoursesController < Learning::StudentsController

    def update_completed
        course = Course.find_by(id_token: params[:course_id_token])
        progress = ProgressEvent.find_by(course_id: course.id, student_id: current_student.id)

        # add the last lesson nad module to the lessons and modules completed.
        # since we know the last page is always lesson text we will default to using LessonText.
        lesson = Lesson.find(course.lesson_order[-1])

        new_lessons_completed = progress.lessons_completed | [lesson.id]
        new_modules_completed = progress.modules_completed | [lesson.module_part_id]
        new_progress_percent = (new_lessons_completed.count * 100.0) / course.lessons_count

        progress.update!(is_completed: true, most_recent_module_id: lesson.module_part_id,
                         most_recent_lesson_id: lesson.id, progress_percent: new_progress_percent,
                         lessons_completed: new_lessons_completed, modules_completed: new_modules_completed)

        respond_to do |format|
            format.js
        end
    end

end