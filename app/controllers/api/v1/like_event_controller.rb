class Api::V1::LikeEventController < ApplicationController
  protect_from_forgery with: :null_session

  def create_like
    student = Student.find_by(id_token: student_id_params[:id_token])
    note = Note.find_by(id_token: params[:note_token])

    if (!LikeEvent.exists?(note_id: note.id, student_id: student.id))
        LikeEvent.create(note_id: note.id, student_id: student.id, like_status: true)
        msg = { status: 'ok' }
    else
        live_event = LikeEvent.find_by(note_id: note.id, student_id: student.id)
        if (live_event.like_status == false)
            live_event.update!(note_id: note.id, student_id: student.id, like_status: true, dislike_status: false)
            msg = { status: 'ok', message: "LikeEvent is now liked (not disliked)" }
        else
            msg = { status: 'error', message: "LikeEvent is already liked!" }
        # like already exists.
        end
    end
    render json: msg
  end

  def create_dislike
    student = Student.find_by(id_token: student_id_params[:id_token])
    note = Note.find_by(id_token: params[:note_token])

    if (!LikeEvent.exists?(note_id: note.id, student_id: student.id))
        LikeEvent.create(note_id: note.id, student_id: student.id, dislike_status: true)
        msg = { status: 'ok' }
    else
        live_event = LikeEvent.find_by(note_id: note.id, student_id: student.id)
        if (live_event.dislike_status == false)
            live_event.update!(note_id: note.id, student_id: student.id, like_status: false, dislike_status: true)
            msg = { status: 'ok', message: "LikeEvent is now disliked (not liked)" }
        else
            msg = { status: 'error', message: "LikeEvent is already disliked!" }
        # like already exists.
        end
    end
    render json: msg
  end

  def destroy
    student = Student.find_by(id_token: student_id_params[:id_token])
    note = Note.find_by(id_token: params[:note_token])

    if (LikeEvent.exists?(note_id: note.id, student_id: student.id))
        LikeEvent.destroy_by(note_id: note.id, student_id: student.id)
        msg = { status: 'ok' }
    else
        msg = { status: 'error', message: "LikeEvent does not exist" }
        # like already exists.
    end
    render json: msg
  end

  def student_id_params
    params.require(:student).permit(:id_token)
  end
end