class Api::V1::SaveEventController < ApplicationController
  protect_from_forgery with: :null_session

  def create
    student = Student.find_by(id_token: student_id_params[:id_token])
    note = Note.find_by(id_token: params[:note_token])

    if (!SaveEvent.exists?(note_id: note.id, student_id: student.id))
        SaveEvent.create(note_id: note.id, student_id: student.id)
        msg = { status: 'ok' }
    else
        msg = { status: 'error', message: "SaveEvent is already liked!" }
        # like already exists.
    end
    render json: msg
  end

  def destroy
    student = Student.find_by(id_token: student_id_params[:id_token])
    note = Note.find_by(id_token: params[:note_token])

    if (SaveEvent.exists?(note_id: note.id, student_id: student.id))
        SaveEvent.destroy_by(note_id: note.id, student_id: student.id)
        msg = { status: 'ok' }
    else
        msg = { status: 'error', message: "SaveEvent does not exist" }
        # like already exists.
    end
    render json: msg
  end

  def student_id_params
    params.require(:student).permit(:id_token)
  end
end