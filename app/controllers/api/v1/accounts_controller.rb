class Api::V1::AccountsController < Api::V1::ApiController
  include ActionController::Cookies
  include ActionController::Flash

  def verify_email
     require "base64"
     # decode the pin depending on if the request is from a email link or web form.
     decoded_pin = (request.method == "GET" ? Base64.strict_decode64(pin_params[:pin]) : pin_params[:pin])
     student = Student.find(params[:student_id])

     # student exists and has sent the correct one_time_pin in the url.
     if (student && (student.one_time_pin == decoded_pin.to_i))
         # if student email is not verified yet, then confirm and redirect to home page.
         if !student.is_email_verified?
            student.update!(is_email_verified: true)
            # now that the account's email is verified, we can assign random colors to the default avatar
            student.update!(background_color: assign_random_color)

            auth_token = JsonWebToken.encode({student_id: student.id})

            cookies[:auth_token] = { value: auth_token, domain: ".#{Notebank::DOMAIN}" }

            cookies[:flash_success] = { value: "Your email has been successfully verified!", domain: ".#{Notebank::DOMAIN}" }
         end
            # msg = { status: "ok", message: "Your email has already been verified.", redirect: "/" }
            # above fails and returns only json page.
         redirect_to root_url(subdomain: false)
     else
         # pin does not match the students one time pin.
         msg = { status: "error", message: "The PIN you entered is incorrect." }
         render json: msg
     end
  end

  def check_username_available
    username = downcase_letters(params[:username]).gsub(/\s+/, '')
    # reject username if it already exists with another account.
    # also reject if doesn't meet other basic criteria but without specific error messages.
    if (username.nil? || (username.length < 6) ||
        !username.match?(Notebank::USERNAME_REGEX) || Student.exists?(["lower(username) = ?", username]))
        msg = { status: 'error', message: "This username is unavailable", input_id: "student_username" }
    else
        msg = { status: "ok", input_id: "student_username" }
    end
    render json: msg
  end

  def change_email
    require "base64"
    decoded_pin = Base64.strict_decode64(params[:pin])
    student = Student.find(params[:student_id])
    # decode the new_email found in the params
    new_email = Base64.strict_decode64(params[:email])
    # student exists and has sent the correct one_time_pin in the url.
    if (student && (student.one_time_pin == decoded_pin.to_i))
        if student.update!(email: new_email)
          cookies[:flash_success] = { value: "Your email has been successfully updated!", domain: ".#{Notebank::DOMAIN}" }
        else
          cookies[:flash_error] = { value: "Your email could not be updated for unknown reasons.", domain: ".#{Notebank::DOMAIN}" }
        end
           # begin a login session
        session[:student_id] = student.id
    else  
        cookies[:flash_error] = { value: "The website URL requested is invalid", domain: ".#{Notebank::DOMAIN}" }
    end
    redirect_to(root_url(subdomain: false))
  end

  def recover_username
    if (Student.exists?(email: recovery_params[:email]))
        student = Student.find_by(email: recovery_params[:email])
        StudentMailer.with(username: student.username, email: recovery_params[:email]).recover_username.deliver_later
        msg = { status: "ok", message: "Username recovery email has been sent.", method: "recover_username" }
    else
        msg = { status: "error", message: "failed to verify student exists purely by email."}
    end
    render json: msg
  end

  def reset_password
    student = Student.find_by(username: recovery_params[:username])
    if (student && (student&.email == recovery_params[:email])) # keep first part to prevent nil == nil true match
        matchdata = request.url.match(/^(http|https):\/\/[^\/]*/)

        password_reset_token = SecureRandom.hex
        PasswordResetDigest.create!(digest: Digest::SHA256.hexdigest(password_reset_token), student_id: student.id)
        StudentMailer.with(username: recovery_params[:username], email: recovery_params[:email],
           domain_url: matchdata[0], password_reset_token: password_reset_token).reset_password.deliver_later
        msg = { status: "ok", message: "Password reset link email has been sent.",
                method: "reset_password", username: "#{student.username}" }
    else
        if (!student.present?)
            msg = { status: "error", message: "username is not valid" }
        else # student is present but email does not match with email params.
            msg = { status: "error", message: "email is not valid" }
        end
    end
    render json: msg
  end

  def reset_password_confirmation
    email = Base64.strict_decode64(token_params[:email_token])

    student = Student.find_by(email: email)
    if (student && (student&.password_reset_digest&.digest == Digest::SHA256.hexdigest(token_params[:token])))
        # therefore token matches with digest
        URLcrypt.key = Rails.application.secrets.secret_key_base
        email_token = URLcrypt.encrypt(email)
        student.password_reset_digest.update!(activated: true)

        redirect_to new_reset_password_url(subdomain: false, email_code: email_token)
    else
        msg = { status: "error", message: "Student not identified" }
        render json: msg
    end
  end

  def create_reset_password
    URLcrypt.key = Rails.application.secrets.secret_key_base
    email = URLcrypt.decrypt(reset_password_params[:email_code])

    student = Student.find_by(email: email)
    if (student&.password_reset_digest.activated &&
       (reset_password_params[:password] == reset_password_params[:password_confirmation]))
        student.update!(password: reset_password_params[:password])
        redirect_to root_url(subdomain: false)
    else
        msg = { status: "error", message: "password cannot be reset" }
        render json: msg
    end
  end

  def not_me
    email = Base64.strict_decode64(params[:email])
    student = Student.find_by(email: email)
    if (student&.report_account_digest&.digest == Digest::SHA256.hexdigest(params[:token]))
        # student matched!
        student.report_account_digest.update!(activated: true)

        URLcrypt.key = Rails.application.secrets.secret_key_base
        email_code = URLcrypt.encrypt(email)

        redirect_to not_me_url(subdomain: false, email_code: email_code)
    else
        render json: {status: 404}
    end
  end

  def not_me_confirmation # deletion
    URLcrypt.key = Rails.application.secrets.secret_key_base
    email = URLcrypt.decrypt(report_imposter_params[:encrypted_string])
    student = Student.find_by(email: email)
    if ((report_imposter_params[:confirmation] == 'true') && student&.report_account_digest.activated)
        #student.destroy! destroy without removing attached documents
        msg = { status: "ok", message: "#{student.username} successfully reported and destroyed", on_success: "completed_report" }
        render json: msg
    else
        render json: {status: 404}
    end
  end

  private

  def reset_password_params
    params.fetch(:student).permit(:password, :password_confirmation, :email_code)
  end

  def token_params
    params.permit(:email_token, :token)
  end

  def report_imposter_params
    params.fetch(:report_imposter).permit(:encrypted_string, :confirmation)
  end

  def recovery_params
    params.fetch(:student).permit(:username, :email)
  end

  def downcase_letters(str)
     arr = str.split();
     arr.each_with_index do |char, index|
       if (char.match?(/[A-Z]/))
          arr[index] = char.downcase
       end
     end
     return arr.join
  end

  def pin_params
     params.permit(:pin)
  end

  def assign_random_color
     # require "rgb"
     random_color = "#" + ("%06x" % rand(0..0xffffff))
     color = RGB::Color.from_rgb_hex(random_color)

     color.l = 0.3
     return color.to_rgb_hex
  end
end
