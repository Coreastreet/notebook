class ApplicationController < ActionController::Base
  require 'json_web_token'

  helper_method :logged_in_as_student?

  helper_method :current_student
  helper_method :local_time
  helper_method :learning_link

  helper_method :current_auth_token
  helper_method :array_of_subjects
  helper_method :hash_of_lesson_elements

  helper_method :current_time_from
  helper_method :single_case

  helper_method :course_dashboard_paths
  helper_method :competition_dashboard_paths

  helper_method :require_guest_uuid

  include ActionController::Flash
  include Pagy::Backend

  def authenticate
    if !logged_in_as_student?
        if (request.url.end_with?("/join")) # competitions join
            request_url = request.url.gsub("/join", "")
        else 
            request_url = request.url
        end

        cookies[:referer] = { value: request_url, domain: ".#{Notebank::DOMAIN}" }

        redirect_to login_url(subdomain: false)
    end
  end

  # used for course#show and learning page links
  
  def learning_link(course_id_token, lesson, continue = false, enrol = false)
      learning_course_lesson_url(course_id_token, 
        lesson.learnable_type.underscore.gsub("_", "-"), lesson.learnable_id, 
        continue: continue, enrol: enrol, subdomain: "learning")
  end

  def local_time(date)
      deadline_date_array = date.strftime("%Y-%m-%d").split("-") 

      utc_deadline = Time.utc(deadline_date_array[0], deadline_date_array[1], deadline_date_array[2], 23, 59)
      local_deadline = utc_deadline.localtime(current_student&.time_zone)

      return local_deadline
  end

  def array_of_subjects
    @array_of_subjects ||=
    [
       #"Learning_project", "Volunteer_project", "Corporate_project" # display all as extra
    ]
  end

  def hash_of_lesson_elements
    @hash_of_lesson_elements ||=
    { :lesson_text => "Text", :lesson_video => "Video", :mc_quiz => "Quiz (Multiple Choice)",
      :sa_quiz => "Quiz (Short Answer)", :fa_quiz => "Quiz (Fill in the answer)", :ma_quiz => "Quiz (Matching)",
      :assignment_text => "Assignment (Text)", :assignment_video => "Assignment (Video)", :practical => "Practical" }
  end

  #def basic_dashboard_paths(student_username)
   # @basic_dashboard_paths ||=
    #{
     # Competitions: dashboard_student_competitions_url(student_username),
      #Courses: content_dashboard_student_courses_path(student_username)     
      # Comments: "#", dashboard_student_comments_url(student_username),
      # Payment_History: dashboard_student_payment_history_path(student_username),
      # Profile: profile_dashboard_student_path(student_username),
    #}
  #end

  def competition_dashboard_paths(competition_id)
    @competition_dashboard_paths ||=
    {
      Details: edit_dashboard_competition_url(competition_id),      
      Comments: "#",
      Video_submissions: video_submissions_dashboard_competition_url(competition_id)
      # Comments: comments_dashboard_course_url(course_id_token)
    }
  end

  def course_dashboard_paths(course_id_token)
    @course_dashboard_paths ||=
    {
      Details: edit_dashboard_course_url(course_id_token),      
      Analytics: analytics_dashboard_course_url(course_id_token),
      Designer: design_dashboard_course_url(course_id_token),
      Intro_and_summary: intro_summary_dashboard_course_url(course_id_token)
      # Comments: comments_dashboard_course_url(course_id_token)
    }
  end

  def current_student
   # Look up the current student based on user_id in the session cookie:
    @current_student ||= Student.find(decoded_auth_token[:student_id]) if decoded_auth_token
  end

  #def current_notifications
  #  @current_notifications ||= current_student&.notifications&.order(created_at: :desc)&.limit(10)
  #end

  def decoded_auth_token
    @decoded_auth_token ||= JsonWebToken.decode(http_auth_cookie)
  end

  def http_auth_cookie
    if cookies[:auth_token].present?
      logger.debug("Cookies present")
      return cookies[:auth_token]
    else
      logger.debug 'Missing token'
    end
  end

  def logged_in_as_student?
    !!current_student
  end

  def require_login
    unless logged_in_as_student?
        flash[]
    end
  end

  def single_case(number, plural_noun)
      string = (number == 1) ? "1 #{plural_noun.singularize}" : "#{number} #{plural_noun}"
      return string
  end

  # return the difference in mins, hours, days between now and the datetime given
  def current_time_from(datetime)
    seconds = Time.current - datetime
    time = ActiveSupport::Duration.build(seconds).parts.first
    return "#{time[1]} #{time[0]} ago"
  end

  def require_guest_uuid 
    if session[:guest_uuid].nil?
        session[:guest_uuid] = SecureRandom.uuid
    end
  end
end
