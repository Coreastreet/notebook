class StudentsController < ApplicationController
  layout "account", only: [:new, :create, :account_recovery, :not_me, :new_reset_password]

  skip_before_action :verify_authenticity_token, only: :create
  # before_action :set_student, only: [:edit, :update, :destroy]
  # GET /students
  # GET /students.json
  def index
    @students = Student.all
  end

  def update_photo
    image = current_student.image

    if (image.attached?) 
        image.purge      
    end
    image.attach(student_image_params[:image])

    @msg = "Your profile image has been updated!"

    respond_to do |format|
      format.js
    end
  end
  # GET /students/1
  # GET /students/1.json
  def home # aka show home page of student account
    #@student = current_student
    respond_to do |format|
      format.html
    end
  end

  # GET /students/new
  def new
    @new_student = Student.new
    respond_to do |format|
      format.html
    end
  end

  # POST /students
  # POST /students.json
  def create
    # validate field length, email validality and equality on the frontend
    # Let validations on the model backend handle any exceptions.
    if (Student.exists?(student_params[:email]))
        msg = { status: "error", message: "Please use a different email", input_id: "student_email" } # since email is already taken
    else
        student = Student.new(student_params)
        respond_to do |format|
            if student.save
              # save a pin for email confirmation
              one_time_pin = pin6_generate
              student.update(one_time_pin: one_time_pin)
              # get origin url
              matchdata = request.url.match(/^(http|https):\/\/[^\/]*/)

              report_account_token = SecureRandom.hex
              ReportAccountDigest.create!(digest: Digest::SHA256.hexdigest(report_account_token), student_id: student.id)
              StudentMailer.with(student: student, origin_url: matchdata[0], report_account_token: report_account_token).welcome_email.deliver_later

              @new_email = student_params[:email]
              @student_id = student.id

              format.js { render "create_success" } #locals: {note: @new_note}}
            else
              @first_error = student.errors.full_messages[0]
              format.js { render "create_error" }
            end
        end
    end
  end

  def new_reset_password
    #pass on the encrypted email to the post url
    @email_code = email_code_params[:email_code]

    URLcrypt.key = Rails.application.secrets.secret_key_base
    @student = Student.find_by(email: URLcrypt.decrypt(@email_code))
  end

  def not_me
      URLcrypt.key = Rails.application.secrets.secret_key_base
      student = Student.find_by(email: URLcrypt.decrypt(params[:email_code]))

      if (student&.report_account_digest&.activated) # user has activated the page from email
        @email = student.email
        @encrypted_string = params[:email_code]
        # return html response
      else
        render status: 404
      end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def email_code_params
      params.permit(:email_code)
    end

    def set_student
      @student = Student.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def student_params
      params.fetch(:student).permit(:username, :password, :password_confirmation, :email, :time_zone)
    end

    def student_image_params
      params.fetch(:student).permit(:image)
    end

    def pin6_generate
      require "securerandom"
      (SecureRandom.random_number(9e5) + 1e5).to_i
    end
end
