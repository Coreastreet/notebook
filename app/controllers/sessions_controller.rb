class SessionsController < ApplicationController
  # protect_from_forgery with: :null_session
  #include ActionController::Cookies

  layout "account", only: [:new]

  def new # new
    # no need for any actions since sessions contain nothing
    #if request.env["HTTP_REFERER"].present?
      #if request.env["HTTP_REFERER"].ends_with?("/cart")
      #    redirect_url = request.env["HTTP_REFERER"].gsub(/(.+)(\/cart)/, '\1/checkout')
      #    @checkout = true
      #else
    # for the purpose of storing the original referer url for links that don't route via actions.
    redirect_url = request.env["HTTP_REFERER"].dup
      #end
      # restore the domain once the web app moves to production.
    if !cookies[:referer].present?
      cookies[:referer] = { value: redirect_url, domain: ".#{Notebank::DOMAIN}" }
    end
    #end
    # meaning the page must also check to ensure it doesn't allow a domain outside the local page.
    @new_student = Student.new
    respond_to do |format|
      format.html
    end
  end

  def create
    # avoid setting the referer as login_path, in order to avoid endless loop.
    if (cookies[:referer].present?)
        request.env["HTTP_REFERER"] = cookies[:referer]
    end
    student = Student.find_by(username: student_params[:username])
    if student && student.authenticate(student_params[:password]) && student.is_email_verified
        # session[:student_id] = student.id
        auth_token = JsonWebToken.encode({student_id: student.id})
        # set the domain in the controller rather than on initializer.
        # use normally { set tld to 1 and 2, the latter for cookies }
        #if (request.subdomain == "dashboard") # i.e request comes from the dashboard subdomain 
        #    cookies[:auth_token] = { value: auth_token, domain: ".#{Notebank::DOMAIN}"}
        if (request.subdomain == "") # any login attempt at creating a session must come from the main domain website.
            cookies[:auth_token] = { value: auth_token, domain: ".#{Notebank::DOMAIN}"}
        end

        # this is a special case for subscribing
        if request.env["HTTP_REFERER"].end_with?("/premium?nbp=guest")
            request.env["HTTP_REFERER"].gsub!(/guest$/, Base64.strict_encode64(student.display_name)); 
        elsif request.env["HTTP_REFERER"].end_with?("/login")
            request.env["HTTP_REFERER"] = nil
        else
        end
        # redis track students who are currently online.
        #Redis.current.zadd("past_24hr_sign_ins", Time.current.to_i, student.id)
        #msg = { status: 'ok', redirect: "/" }
        #render json: msg
        redirect_back(fallback_location: root_url(subdomain: false))
    else
        # if the username does not match an existing account
        if student.nil?
            @error_message = "This username does not exist."
            @input_id = "session_username"
        else
            # account with such username exists but...
            # password is wrong

            if student.authenticate(student_params[:password]) == false
                @error_message = "That password was incorrect. Please try again."
                @input_id = "session_password"
            elsif student.is_email_verified == false # email is not verified
                @error_message = "Your email has not been verified yet."
                @input_id = nil
            else
                @input_id = nil
                @error_message = "Unknown error. Please contact us."
            end
        end

        # msg = {:status => "error", :message => error_message, :input_id => input_id, :redirect => "/" }
        #render json: msg
        respond_to do |format|
          format.js { render "create_session_error" } #, locals: { message: error_message, input_id: input_id } }
        end
    end
  end

  def destroy
    cookies.delete(:auth_token, domain: ".#{Notebank::DOMAIN}")
    cookies.delete(:referer, domain: ".#{Notebank::DOMAIN}")

    current_student = nil

    request_origin = request.env["HTTP_REFERER"]
    if request_origin.include?("dashboard")
        redirect_to root_url(subdomain: false)
    else 
        redirect_to request_origin
    end
  end

  private

  def student_params
    params.fetch(:session).permit(:username, :password)
  end
end
