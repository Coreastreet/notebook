class BasicInfo::MyNotebooksController < ApplicationController
  
    def update_display_name
        current_datetime = DateTime.current
        if (current_student.last_display_name_change.nil? || 
            (((current_datetime.to_time - current_student.last_display_name_change.to_time)/86400) >= 7))
            # then at least one week has passed i.e. 7.days
            if (display_name_params[:display_name].length <= 30)
                current_student.update!(display_name: display_name_params[:display_name], 
                                    last_display_name_change: current_datetime)
                @message = "Display name updated successfully"
            else 
                @message = "Display name cannot be longer than 30 characters"                
            end
        else 
            @message = "Display name can only be updated once a week."
        end
        # @message = "Display name updated successfully"
        respond_to do |format|
          format.js { render "layouts/display_snackbar.js.erb" }
        end
    end

    def update_email
        if (email_params[:email] == email_params[:email_confirmation])
            if (email_params[:email] != current_student.email)
                one_time_pin = pin6_generate
                # assume that the email is valid for now.
                # create a new one_time_pin for later verification via email link
                current_student.update!(one_time_pin: one_time_pin)
                # get origin url; for this matchdata, unlike the others, subdomain
                # should be removed for the origin url, since the form is sent from dashboard.
                # matchdata = request.url.match(/^(http|https):\/\/[^\/]*/)
                origin_domain = "#{Notebank::DOMAIN}:3000"   # matchdata[0].sub!("#{request.subdomain}.", "")
                @message = "An email has been sent to your new email address. Please verify your new email."
                # only the above message is needed, the rest should be frontend validation.
                StudentMailer.with(email: email_params[:email], student: current_student, origin_url: origin_domain).change_email.deliver_later
            else
                @message = "New emails must be different to the current email."
            end
        else
            @message = "Email and confirmation email do not match."
        end

        respond_to do |format|
            format.js { render "layouts/display_snackbar.js.erb" }
        end
    end

    def update_password
        if (current_student.authenticate(password_params[:current_password])) # then the old password is valid/user is verified
            if (password_params[:password] != password_params[:password_confirmation]) # then no match
                @message = "Password and confirmation password do not match."
            else # passwords and confirmation_password match
                if (password_params[:password] == password_params[:current_password])
                    @message = "New passwords must be different to the current password."
                elsif (password_params[:password].length < 8)
                    @message = "New passwords must be at least 8 characters."
                else
                    current_student.update!(password: password_params[:password])
                    @message = "Password successfully updated."
                    # only the message above is needed since all the rest will be indicated with front end.
                end
            end
        else
            @message = "The old password value is incorrect."
        end

        respond_to do |format|
          format.js { render "layouts/display_snackbar.js.erb" }
        end
    end
    
    def disable_account 
        # user must have active account if access.
        current_student.update!(account_status: "inactive",
            reason_for_deactivation: disable_params[:reason_for_deactivation])
        # log the user out.
        cookies.delete(:auth_token, domain: ".#{Notebank::DOMAIN}")
        current_student = nil
        # leave a notification of what happened.
        cookies[:flash_success] = { value: "Your Notebook account has now been disabled", domain: ".#{Notebank::DOMAIN}" }

        redirect_to(root_url(subdomain: false))
        #respond_to do |format|
        #   format.js
        #end
    end

    def delete_account 
        # don't actually delete the account
        if (current_student.authenticate(delete_params[:password]))
            # identity verified
            current_student.update!(account_status: "deleted",
                reason_for_deletion: delete_params[:reason_for_deletion])
            # log the user out.
            cookies.delete(:auth_token, domain: ".#{Notebank::DOMAIN}")
            current_student = nil

            # Use cookies instead of flash for cross-domain
            cookies[:flash_success] = { value: "Your Notebook account has now been deleted", domain: ".#{Notebank::DOMAIN}" }

            redirect_to(root_url(subdomain: false))
        else 
            @message = "The password you entered is incorrect."
            
            respond_to do |format|
                format.js { render "layouts/display_snackbar.js.erb" }
            end
        end
        # maybe freeze the object to prevent future modification.
    end

    def show_delete_account
        # display the delete account form.
        @student = current_student
    end

    def show_delete_modal
        @student = current_student
        respond_to do |format|
            format.js
        end
    end

    private

        def pin6_generate
            require "securerandom"
            (SecureRandom.random_number(9e5) + 1e5).to_i
        end
        
        def display_name_params
            params.fetch(:student).permit(:display_name)
        end

        def email_params
            params.fetch(:student).permit(:email, :email_confirmation)
        end

        def password_params
            params.fetch(:student).permit(:current_password, :password, :password_confirmation)
        end

        def disable_params
            params.fetch(:student).permit(:reason_for_deactivation)
        end

        def delete_params
            params.fetch(:student).permit(:reason_for_deletion, :password)
        end

end
  