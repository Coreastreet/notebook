class BankAccountsController < ApplicationController
    def create
        current_student.create_bank_account(bank_account_params)

        @msg = "Bank account details have been uploaded"
        respond_to do |format|
            format.js
        end
    end

    def update
        bank_account = current_student.bank_account
        bank_account.update!(bank_account_params)

        @msg = "Bank account details have been updated"
        respond_to do |format|
            format.js
        end
    end

    def bank_account_params
        params.fetch(:bank_account).permit(:account_holder_name, :account_number, :bsb)
    end
end