class CompetitionsController < ApplicationController
  before_action :authenticate, only: [:join]

  def index
    current_date = Date.current
    @current_competitions = Competition.where(deadline_date: current_date..) # deadline is today, tomorrow or after.
    @current_videos = @current_competitions.map(&:video)

    @past_competitions = Competition.where(deadline_date: ...current_date) # deadline is yesterday or before
    @past_videos = @past_competitions.map(&:video)

    respond_to do |format|
      format.html
      format.js
    end
  end

  #  def paid
  #    @paid_competitions = Competition.where(has_prize: true)
  #    @videos = @paid_competitions&.map(&:video)

  #    respond_to do |format|
        # format.html
  #      format.js
  #    end
  #  end

  #  def volunteer
  #    @volunteer_competitions = Competition.where(has_prize: false)
  #    @videos = @volunteer_competitions&.map(&:video)

  #    respond_to do |format|
        # format.html
  #      format.js
  #    end
  #  end

  def show
    @competition = Competition.find(params[:id])
    deadline_date_array = @competition.deadline_date.strftime("%Y-%m-%d").split("-") 
    results_date_array = @competition.results_date.strftime("%Y-%m-%d").split("-") 
    # deadline_time_array = @competition.deadline_time.strftime("%H:%M").split(":")

    @utc_deadline = Time.utc(deadline_date_array[0], deadline_date_array[1], deadline_date_array[2], 23, 59)
    @utc_results_date = Time.utc(results_date_array[0], results_date_array[1], results_date_array[2], 23, 59)

    #offset = (current_student&.time_zone || "+00:00") # assume the user is in sydney
    #@local_deadline = utc_deadline.getlocal(offset)

    @client = @competition.student

    if logged_in_as_student?
        @student_upvoted_comment_ids = current_student&.upvotes.where(upvoteable_type: "Comment").pluck(:upvoteable_id)
        @competition_event = CompetitionEvent.exists?(student_id: current_student&.id, competition_id: params[:id]) ?
                             CompetitionEvent.find_by(student_id: current_student&.id, competition_id: params[:id]) : nil    
        if (VideoSubmission.exists?(student_id: current_student&.id, competition_id: params[:id]))                             
            @video_submission = VideoSubmission.find_by(student_id: current_student&.id, competition_id: params[:id])             
            @video_submission_video = @video_submission.video
        else 
            @video_submission = nil            
            @video_submission_video = nil
        end
    else  
        @competition_event = nil
    end
    @save_event = SaveEvent.exists?(student_id: current_student&.id, saveable_type: "Competition",
                saveable_id: params[:id]) ? SaveEvent.find_by(student_id: current_student&.id, saveable_type: "Competition",
                saveable_id: params[:id]) : nil               

    @requirements = @competition.requirements

    all_comments = @competition.comments.order(created_at: :desc)
    my_comments = all_comments.where(student_id: current_student&.id)
    other_comments = all_comments.excluding(my_comments)

    @comments = my_comments + other_comments 

    if @comments.present?
        @comment_authors = @comments.map(&:student)
        @comment_replies = @comments.map(&:comments)
        @comment_reply_authors = @comment_replies.map{ |collection| collection.map(&:student) if collection.present? }
    end

    @array_of_contestant_ids = @competition.competition_events.pluck(:student_id)
    
    @video = @competition.video

    # code for loading all of the video submissions for a competition
    @competition_video_submissions = @competition.video_submissions
    @video_submission_submitters = @competition_video_submissions.map(&:student)
    @competition_submission_videos = @competition_video_submissions.map(&:video)

    submission_placeholder_count = 4 - (@competition_video_submissions.count % 4)
    @submission_placeholders = (submission_placeholder_count == 4) ? [] : [*1..submission_placeholder_count]

    respond_to do |format|
      format.html
    end
  end

  # competition event
  def join
    @competition = Competition.find(params[:id])

    @msg = "You have already joined the competition"
    
    if (CompetitionEvent.exists?(student_id: current_student&.id, competition_id: @competition.id)) 
        @joined = false
    else 
        new_comp = CompetitionEvent.new(student_id: current_student&.id, competition_id: @competition.id)
        @joined = new_comp.save
        if (@joined)
           @msg = "You have joined the competition"
        else 
           @msg = "Something went wrong"
        end
    end

    respond_to do |format|
      format.js
    end
  end

  def withdraw
    @competition = Competition.find(params[:id])
    
    if (CompetitionEvent.exists?(student_id: current_student&.id, competition_id: @competition.id)) 
        CompetitionEvent.find_by(student_id: current_student&.id, competition_id: @competition.id).destroy!
        @msg = "You withdrew from #{@competition.title}"
    else 
        @msg = "You have not joined the selected competition previously"
    end

    respond_to do |format|
      format.js
    end
  end
end
