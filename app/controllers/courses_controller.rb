class CoursesController < ApplicationController
  protect_from_forgery with: :null_session
  # before_action :set_note, only: [:show, :edit, :update, :destroy]
  # GET /notes
  # GET /notes.json
  before_action :authenticate, only: [:show]

  def index
    @courses = Course.all # get all notes
    @show_leftbar = true
    @show_course_nav = false

    @subject = "All"
    # if flash message received from email or other
    if cookies[:flash_success].present?
        @flash_success = cookies[:flash_success]
        cookies.delete(:flash_success, domain: ".#{Notebank::DOMAIN}")
    elsif cookies[:flash_error].present?
        @flash_error = cookies[:flash_error]
        cookies.delete(:flash_error, domain: ".#{Notebank::DOMAIN}")
    else 
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  def index_by_subject
    @show_leftbar = true
    @subject = params[:subject].to_sym
    @course = nil

    @show_course_nav = hash_of_courses[@subject].present? ? true : false
    @notes = Note.where(subject: params[:subject]) # get all notes belonging to a subject

    respond_to do |format|
      format.html { render "index" }
      format.js { render "index_by_subject", locals: { notes: @notes } }
    end
  end

  def index_by_course
    @show_leftbar = true
    @show_course_nav = true
    @notes = Note.where(course: params[:course]) # get all notes belonging to a subject and course.

    @course = params[:course]
    @subject = (Note.subjects.keys[(Note.courses[@course])/10]).to_sym

    respond_to do |format|
      format.html { render "index" }
      format.js { render "index_by_course", locals: { course_notes: @notes } }
    end
  end

  # GET /notes/1.json
  def show
    @course = Course.find_by(id_token: params[:course_token])
    @module_parts = ModulePart.find(@course.module_order[1...-1])

    @student = @course.student
    @subject = @course.subject.to_sym

    # comments orders by most recent at the top.
    # @comments = @course.comments.order(created_at: :desc)
    # @number_of_comments = @comments.count

    # code to indicate the second nav must not be shown.
    # if logged in, check if the note already exists in the cart. Max one note in cart.
    # @item_in_cart = Redis.current.zscore(session[:guest_uuid], @course.id).nil? ?  false : true

    #Redis.current.zadd("past_24hr_viewEvents", Time.current.to_i, current_student.id)
    #Redis.current.zadd("student:#{current_student.id}", Time.current.to_i, "note:#{@note.id}")
    @course_saved = current_student.has_saved?(@course.id)
    @course_enrolled = current_student.is_enrolled?(@course.id)

    # variables for creating the link for the course.

    @bg_premium = false
    if (@course_enrolled)
        # get the most recent lesson
        progress_event = ProgressEvent.find_by(course_id: @course.id, student_id: current_student.id)
        #last_completed_lesson = Lesson.find(progress_event.lessons_completed.max)
        # check if last completed lesson is the last lesson.
        most_recent_lesson = Lesson.find(progress_event.most_recent_lesson_id)

        if ((most_recent_lesson.learnable_type == "McQuiz") && most_recent_lesson.has_content)
            @link = learning_course_quiz_question_url(@course.id_token, "mc-quiz", 
                    most_recent_lesson.learnable_id, 
                    (progress_event.most_recent_quiz_question_index
                                  .has_key?(most_recent_lesson.learnable_id.to_s) ? 
                     progress_event.most_recent_quiz_question_index[most_recent_lesson.learnable_id.to_s] : 1), 
                     subdomain: "learning")           
        else 
            @link = learning_link(@course.id_token, most_recent_lesson)
        end
        @text_link = "Resume Course"
    else 
        # get the first lesson of the course
        first_lesson = Lesson.find(@course.lesson_order[0])

        if (@course.is_premium && (current_student.is_subscribed == false))
            # premium course is not started and student is not a premium member, so paywall it.
            @link = premium_url(course_id_token: @course.id_token)
            @text_link = "Go premium"
            @bg_premium = true
        else 
            # all other cases, where course is free (aka. not premium) regardless of membership 
            # or premium but the student is a premium member too.
            @link = learning_link(@course.id_token, first_lesson, false, true) # enrol true to allow enrolment on commencement.
            @text_link = "Start Course"
        end 
    end
    # if this note had been bought by the logged-in student previously, notify accordingly. 
    # @purchased = Order.exists?(buyer_id: current_student.id, course_id: @course.id)
  end

  def load_slider_notes
    #convert string of array token ids into a 2d array with sub-array length of 3 max.
    # use zero-base index
    array_of_page_tokens = load_other_notes_params[:slider_notes_array].split(",")
                                .each_slice(3).to_a
    @current_slider_index = load_other_notes_params[:slider_page_id].to_i

    # make sure index is greater than or equal to 0.
    if (@current_slider_index >= 0)
      slider_notes = Note.where(id_token: array_of_page_tokens[@current_slider_index])

      # get the index for the all the other notes to be displayed.
      # values depend on which action (next or previous is taken).
      slider_notes_indicies = [*(@current_slider_index * 3)..(@current_slider_index * 3 + 2)]
                                  .slice(0, slider_notes.length)
      # combine the indicies and the note objects for easier iteration.
      @hash_of_next_slider_notes = Hash[slider_notes_indicies.zip(slider_notes)]
    end

    respond_to do |format|
      format.js
    end

  end

  # GET /notes/new
  def new
    @note = Note.new
  end

  # GET /notes/1/edit
  def edit
  end

  # POST /notes
  # POST /notes.json
  def create
    @note = Note.new(note_params)

    respond_to do |format|
      if @note.save
        format.html { redirect_to @note, notice: 'Note was successfully created.' }
        format.json { render :show, status: :created, location: @note }
      else
        format.html { render :new }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notes/1
  # PATCH/PUT /notes/1.json
  def update
    respond_to do |format|
      format.js
    end
  end

  # DELETE /notes/1
  # DELETE /notes/1.json
  def destroy
    @note.destroy
    respond_to do |format|
      format.html { redirect_to notes_url, notice: 'Note was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_note
      @note = Note.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def note_params
      params.fetch(:note, {})
    end

    def load_other_notes_params
      params.require(:note).permit(:slider_page_id, :slider_notes_array)
    end
end
