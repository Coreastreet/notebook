class CommentsController < ApplicationController

  before_action :authenticate, only: [:upvote]

  def create
      if (params[:competition_id].present?)
          @new_comment = Comment.create(body: comment_params[:body],
                                        commentable_type: "Competition",
                                        commentable_id: params[:competition_id],
                                        student_id: current_student.id)
          competition = Competition.find(params[:competition_id])
          @video_submission_poster = nil

          # add a notification for the original uploader of the competition.
          if (currrent_student.id != competition.student_id)
              Notification.create(message: "#{current_student.username} commented: #{@new_comment.body}", 
                                  notifiable_type: "Competition",
                                  notifiable_id: competition.id,
                                  student_id: competition.student_id)
          end
      elsif (params[:video_submission_id].present?)
          @new_comment = Comment.create(body: comment_params[:body],
                                        commentable_type: "VideoSubmission",
                                        commentable_id: params[:video_submission_id],
                                        student_id: current_student.id)
          video_submission = VideoSubmission.find(params[:video_submission_id])
          video_submission.increment!(:comments_count)

          competition = video_submission.competition
          @video_submission_poster = video_submission.student

          # add a notification for the original uploader of the video.
          if (current_student.id != video_submission.student_id)
              Notification.create(message: "#{current_student.username} commented: #{@new_comment.body}",
                                  notifiable_type: "VideoSubmission",
                                  notifiable_id: video_submission.id,
                                  student_id: video_submission.student_id)
          end
      else 
          # error in user post params
      end
      
      @competition_client = competition.student
      @array_of_contestant_ids = competition.contestants.pluck(:id)

      @comment_author = current_student

      if (@new_comment.present?)
        @msg = "Your comment has been posted"
      else 
        @msg = "Your comment could not be posted"
      end

      respond_to do |format|
        format.js
      end
  end

  def create_reply 
      @comment = Comment.find(params[:id])

      @comment_reply = Comment.new(body: comment_params[:body], student_id: current_student.id,
                    commentable_id: @comment.id, commentable_type: "Comment")
      @saved = @comment_reply.save

      @comment_author = current_student

      if (@comment.commentable_type == "Competition")
          competition = Competition.find(@comment.commentable_id)
          @video_submission_poster = nil
      elsif (@comment.commentable_type == "VideoSubmission") 
          video_submission = VideoSubmission.find(@comment.commentable_id)
          competition = video_submission.competition
          @video_submission_poster = video_submission.student
      else
      end

      @competition_client = competition.student
      @array_of_contestant_ids = competition.contestants.pluck(:id)
      
      if (@saved)
        # add a notification for the author of the original comment.
        # Notification for the original comment poster.
        if (@comment.student_id != current_student.id)
            Notification.create(message: "#{@comment_author.username} replied: #{@comment_reply.body}",
                                notifiable_type: @comment.commentable_type, 
                                notifiable_id: @comment.commentable_id,
                                student_id: @comment.student_id)
        end
        @msg = "Comment posted"
      else
        @msg = "Comment could not be posted"
      end

      respond_to do |format|
        format.js
      end
  end

  def update
      @comment = Comment.find(params[:id])
      update = @comment.update(body: comment_params[:body])

      if update.present?
        @msg = "Your comment has been updated"
      else 
        @msg = "Your comment could not be updated"
      end

      respond_to do |format|
        if (@comment.commentable_type != "Comment")
            format.js
        else
            format.js { render "update_reply" }
        end
      end
  end

  def upvote
      comment = Comment.find(params[:id])
      comment.increment!(:upvotes_count)
    
      @comment_id = params[:id].to_i
      upvote = Upvote.new(upvoteable_type: "Comment", upvoteable_id: params[:id], student_id: current_student.id)      
      @saved = upvote.save

      respond_to do |format|
          format.js
      end
  end

  def remove_upvote 
      comment = Comment.find(params[:id])
      comment.decrement!(:upvotes_count)

      @comment_id = params[:id].to_i
      upvote = Upvote.find_by(upvoteable_type: "Comment", upvoteable_id: params[:id], student_id: current_student.id)      
      @destroyed = upvote.destroy

      respond_to do |format|
          format.js
      end
  end

  def destroy
      @comment = Comment.find(params[:id])
      deletion = @comment.destroy

      if (@comment.commentable_type == "VideoSubmission")
          VideoSubmission.find(@comment.commentable_id).decrement!(:comments_count)
      else 

      end

      if deletion.present?
          @msg = "Your comment has been removed"
      else
          @msg = "Your comment could not be destroyed"
      end

      respond_to do |format|
        if (@comment.commentable_type != "Comment")
            format.js
        else
            format.js { render "destroy_reply" }
        end
      end
  end

  private

  def comment_params
    params.require(:comment).permit(:body)
  end

end
