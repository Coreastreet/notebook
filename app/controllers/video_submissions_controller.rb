class VideoSubmissionsController < ApplicationController

  before_action :authenticate, only: [:upvote]

  def create
    @competition_id = params[:competition_id].to_i
    @submission = VideoSubmission.create(competition_id: params[:competition_id], student_id: current_student.id,
                                        title: video_submission_params[:title], description: video_submission_params[:description])

    @video = ActiveStorage::Blob.find_by(id: video_submission_key_param[:video_key])
            
    if (@video.present? && @submission.present?)
        @submission.video.purge
        @submission.video.attach(@video) 
        @msg = "Video successfully submitted!"
    else
        @msg = "Video could not be submitted"
    end

    respond_to do |format|
        format.js
    end
  end

  def show
    @video_submission = VideoSubmission.find(params[:id])
    @video_submission_video = @video_submission.video

    @competition = @video_submission.competition
    @video = @competition.video
    @student = @competition.student

    @array_of_contestant_ids = @competition.competition_events.pluck(:student_id)

    if (logged_in_as_student?)
        @student_upvoted_comment_ids = current_student&.upvotes.where(upvoteable_type: "Comment").pluck(:upvoteable_id)
        @student_upvoted_video_ids = current_student&.upvotes.where(upvoteable_type: "VideoSubmission").pluck(:upvoteable_id)
    end

    all_comments = @video_submission.comments.order(created_at: :desc)
    my_comments = all_comments.where(student_id: current_student&.id)
    other_comments = all_comments.excluding(my_comments)

    @comments = my_comments + other_comments 

    if @comments.present?
        @comment_authors = @comments.map(&:student)
        @comment_replies = @comments.map(&:comments)
        @comment_reply_authors = @comment_replies.map{ |collection| collection.map(&:student) if collection.present? }
    end

    @more_submissions = @competition.video_submissions
    @submission_students = @more_submissions.map(&:student)
    @submission_videos = @more_submissions.map(&:video)

    @more_competitions = Competition.find((Competition.pluck(:id) - [@competition.id]).sample(4))
    @competition_students = @more_competitions.map(&:student)
    @competition_videos = @more_competitions.map(&:video)


    respond_to do |format|
        format.html
    end
  end

  def edit
    @video_submission = VideoSubmission.find(params[:id])
    @video = @video_submission.video

    respond_to do |format|
        format.js
    end
  end

  def upload_video
    return true
  end

  def update
    video_submission = VideoSubmission.find(params[:id])
    video = video_submission.video 

    video_submission.update(video_submission_params)

    if (video.blob_id != video_submission_key_param[:video_key].to_i) # video not updated
        @new_video = ActiveStorage::Blob.find(video_submission_key_param[:video_key])

        video.purge
        video.attach(@new_video) 
    end

    respond_to do |format|
        format.js
    end
  end

  def upvote
      video_submission = VideoSubmission.find(params[:id])
      video_submission.increment!(:upvotes_count)

      upvote = Upvote.new(upvoteable_type: "VideoSubmission", upvoteable_id: params[:id], student_id: current_student.id)      
      @saved = upvote.save

      respond_to do |format|
          format.js
      end
  end

  def remove_upvote 
      video_submission = VideoSubmission.find(params[:id])
      video_submission.decrement!(:upvotes_count)

      upvote = Upvote.find_by(upvoteable_type: "VideoSubmission", upvoteable_id: params[:id], student_id: current_student.id)      
      @destroyed = upvote.destroy

      respond_to do |format|
          format.js
      end
  end

  def destroy
  end

  def video_submission_params
      params.fetch(:video_submission).permit(:title, :description)
  end

  def video_submission_key_param
      params.fetch(:video_submission).permit(:video_key)
  end
end
