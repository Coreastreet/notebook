class TransactionController < ApplicationController
  def create
    # Get your login and password by going to: https://connect.squareup.com/apps
    #credentials = {
    #  login: 'sandbox-sq0idb-ouC_bLRxDFdhrlTOdznwPg',
    #  password: 'sandbox-sq0csb-Q6Y3XaXxTCPbyJTpEokiPyro5sYm3Sbc9O6Kz_sKoyM',
      # How to get your location ID, see: https://docs.connect.squareup.com/articles/faq-lookup-my-location-id
    #  location_id: 'LMX6TRKH0S295',
    #}

    # price in cents determined here since only one type of subscription is offered.
    premium_notebook = 999 # aka $9.99

    # aka referring to myself via my square account.
    client = Square::Client.new(
        access_token: "EAAAEBDCEmrU0PdNO12GAcmOyFqNcMy43wAIMfwMCPA9HYjyefj08D5Yey281oE_",
        environment: "sandbox"
    )

    response = client.payments.create_payment(
         body: {
            source_id: transaction_params[:card_nonce],
            idempotency_key: SecureRandom.uuid,
            amount_money: {
              amount: amount_cents,
              currency: "AUD"
            }
            # app fee money - only needed if you intend allow other sellers to sign up. 
            # aka if you intend to convert your website store into a marketplace, which it is not at the moment.

            # app_fee_money: {
            #  amount: 200,
            #  currency: "AUD"
            #},
         }
    )

    if response.success?
       puts "Successfully charged #{amount_cents}"
    else
       raise StandardError, response.errors
    end

    respond_to do |format|
        format.html { redirect_to transaction_success_url(transaction_id: 10, ) }
    end
  end

  def success

    respond_to do |format|
        format.html
    end
  end

  def show
  end

  private

  def transaction_params
    params.fetch(:transaction).permit(:card_nonce)
  end
end
