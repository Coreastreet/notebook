$(document).on('turbolinks:load', function () {

      var recovery_email;
      $("form").on("click", "#continue_account_recovery", function() {
          recovery_email = $("#recovery_student_email");
          if (recovery_email.hasClass("completed-field")) {
              var form_box = $(this).closest(".form-box");
              form_box.addClass("is-hidden");

  //            console.log("clicked");
              $("#account_recovery_subtitle").text(form_box.data("subtitle"));
              $("#recovery_email_holder").text($("#recovery_student_email").val());
              $("#account_recovery span.recovery_email_copy").text($("#recovery_student_email").val());

              form_box.next().removeClass("is-hidden");
          } else {
              if (recovery_email.val().length == 0) {
                    addStatus(recovery_email, "error", "Please enter a valid email");
              }
          }
      });

      $("form#account_recovery_form").on("click", "i#edit_recovery_email", function(response) {
            var form_box = $(this).closest(".form-box");

            form_box.addClass("is-hidden");
            $("#account_recovery_subtitle").text(form_box.data("subtitle"));

            form_box.prev().removeClass("is-hidden");

            $("#account_recovery_subtitle").text(form_box.data("subtitle"));
      });

      function addStatus(div, status, message = "") {
          if (status == "error") {
              div.removeClass("completed-field");
              div.addClass("is-danger");
              div.parent().find(".fa-exclamation-triangle").removeClass("is-hidden");
              div.parent().find(".fa-check").addClass("is-hidden");
              div.parent().next().text(message);
          } else {
              div.removeClass("is-danger");
              div.parent().next().text("");

              if (status == "success") {
                   div.parent().find(".fa-exclamation-triangle").addClass("is-hidden");
                   div.parent().find(".fa-check").removeClass("is-hidden");
                   div.addClass("completed-field");
               } else if (status == "neutral") {
                   div.parent().find(".fa-exclamation-triangle").addClass("is-hidden");
                   div.parent().find(".fa-check").addClass("is-hidden");
               } else {

               }
          }
      }


});
