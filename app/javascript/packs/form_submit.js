$(document).on('turbolinks:load', function () {

// display name of
    var form;
    var form_elements;
    var mandatory_incomplete_fields;
    var submit_input;
    var field_holder;
    $('form.completed-form').on("click", "input[type=submit]", function(e) {
        form = this.closest("form");
        form_elements = Array.from(form.elements);
        submit_input = form_elements.pop();
        mandatory_incomplete_fields = form_elements.filter(field => (!field.classList.contains('completed-field') &&
                                                                     !field.classList.contains('optional') &&
                                                                     field.getAttribute("type") != "hidden"));
        if (mandatory_incomplete_fields.length == 0) {
            // do nothing allow submission to continue.
            console.log("submission ok!");
        } else {
            console.log("submission not ok!");
            $(form).find(".notification").text("Please complete the missing fields.").removeClass("is-hidden");
            if (mandatory_incomplete_fields.length != 0) {
              $(mandatory_incomplete_fields).each(function(i){
                  field_holder = $(mandatory_incomplete_fields[i]);
                  console.log(field_holder);
                  if (field_holder.hasClass("student_username")) {
                      if (!checkMinLength(field_holder, 6) || (!checkAlphaNumeric(field_holder))) {
                          // do nothing; allow form submission to pass.
                          if (!checkMinLength(field_holder, 6)) {
                              addStatus(field_holder, "error", "Username must be at least 6 characters.");
                          } else {
                              addStatus(field_holder, "error", "Username must be contain only alphanumeric characters.");
                          }
                      }
                  }
                  if (field_holder.hasClass("password")) {
                      if (!checkMinLength(field_holder, 8)) {
                          addStatus(field_holder, "error", "Password must be at least 8 characters.");
                      }
                  }

                  if (field_holder.hasClass("password_confirmation")) {
                      if (!checkMatch(field_holder, $(form).find("input.password"))) {
                          addStatus(field_holder, "error", "Confirmation password must match the password.");
                      }
                  }

                  if (field_holder.hasClass("email")) { // email
                      if (!checkEmailFormat(field_holder)) {
                          addStatus(field_holder, "error", "Please enter a valid email.");
                      }
                  }
              });
            }
            return false;
        }
    })

    var reset_form;
    var reset_action;
    $("form#account_recovery_form").on("click", "input#reset_password", function(response) {
          reset_form = $(this).closest("form");
          reset_action = reset_form.attr("action").replace("recover_username", "reset_password");
          reset_form.attr("action", reset_action);
          return true
    });

    function addStatus(div, status, message = "") {
        if (status == "error") {
            div.removeClass("completed-field");
            div.addClass("is-danger");
            div.parent().find(".fa-exclamation-triangle").removeClass("is-hidden");
            div.parent().find(".fa-check").addClass("is-hidden");
            div.parent().next().text(message);
        } else {
            div.removeClass("is-danger");
            div.parent().next().text("");

            if (status == "success") {
                 div.parent().find(".fa-exclamation-triangle").addClass("is-hidden");
                 div.parent().find(".fa-check").removeClass("is-hidden");
                 div.addClass("completed-field");
             } else if (status == "neutral") {
                 div.parent().find(".fa-exclamation-triangle").addClass("is-hidden");
                 div.parent().find(".fa-check").addClass("is-hidden");
             } else {

             }
        }
    }

    function checkMinLength(div, length) {
        if (div.val() >= length) {
          return true;
        } else {
          return false;
        }
    }

    function checkEmailFormat(div) {
        return (/^[a-zA-Z0-9_]+$/.test(div.val()) ? true : false);
    }

    function checkMatch(div1, div2) {
        return ((div1.val() == div2.val()) ? true : false);
    }

    function checkAlphaNumeric(div) {
        if (/^[a-zA-Z0-9_]+$/.test(div.val())) {
          return true;
        } else {
          return false;
        }
    }

    /* var JSON_object_login;
    $('#signup-form').on("ajax:success", function(response) {
        JSON_object_login = response.detail[0];
        console.log(JSON_object_login);
    }) */

});
