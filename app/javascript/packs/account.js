$(document).on('turbolinks:load', function () {

      var linked_body_name;
      var linked_body;
      // make the modal function in switching between tabs.
      $(".form-box").on('click', '.tabs li', function() {
          // top header tabs done.
          $(this).siblings().each( function(){
              $(this).removeClass("is-active");
          })
          $(this).addClass("is-active");//.removeClass("is-active");

          // switching the linked body.
          linked_body_name = $(this).data("linked-body");

          if (linked_body_name == "login-form") {
              $("#form-title").text("Log in to Notebank");
          } else { // signup-form
              $("#form-title").text("Join Notebank");
          }

          linked_body = $(`#${linked_body_name}`);
          $(".form-box .tabs-body").each( function() {
            $(this).addClass("is-hidden");
          })
          linked_body.removeClass("is-hidden");
      });

      var username_tick;
      var username_cross;
      var username_value;
      var student_username_help;
      // sending an ajax request to check if username is available and sending success or error in response
      $("form#signup-form, form#signup-modal").on('change', '.student_username', function() {
          var form_id = $(this).closest("form").attr('id');
          //error_notification = $(`#${form_id} .notification`);
          username_tick = $(`#${form_id} .student_username`).parent().find(".fa-check");
          username_cross = $(`#${form_id} .student_username`).parent().find(".fa-exclamation-triangle");
          username_value = $(this).val();
          student_username_help = $(this).parent().next();

          if (username_value.length >= 6 && (/^[a-zA-Z0-9_]+$/.test(username_value))) { // has minimum 6 alphanumeric characters, then can check availability.
              getUrl(`/api/v1/accounts/:student_id/check_username_available?username=${username_value}`)
                .then( json => {
                      console.log(json);

                      if (json.status == "error") {

                          //error_notification.removeClass("is-hidden").addClass("error-border");
                          //error_notification.text(json.message);
                          student_username_help.text(json.message);
                          //$(`#${form_id} #${json.input_id}`).addClass("is-danger");
                          username_cross.removeClass("is-hidden");
                          username_tick.addClass("is-hidden");
                          $(this).removeClass("completed-field").addClass("is-danger");
                          //$(this).addClass("is-danger");
                      } else {

                          student_username_help.text("")
                          //error_notification.addClass("is-hidden").removeClass("error-border");
                          //error_notification.text("");
                          username_cross.addClass("is-hidden");
                          username_tick.removeClass("is-hidden");
                          $(this).addClass("completed-field").removeClass("is-danger");
                      }
                });
          } /* input listener will catch error; only need to send the request.
            else {
              if (username_value.length < 6) {
                  student_username_help.text("Username must be at least 6 characters.")
              } else { // (/^[a-z0-9]+$/.test(value)) must be false
                  student_username_help.text("Username must only contain alphanumeric values");
              }
              username_cross.removeClass("is-hidden");
              username_tick.addClass("is-hidden");
              $(this).removeClass("completed-field");
              $(this).addClass("is-danger");
          }; */
      });

      // remove success or error styling when user is typing a new username.
      $("form").on('input', '.student_username', function() {

          if ($(this).val().length >= 6 && (/^[a-zA-Z0-9_]+$/.test($(this).val()))) {
              $(this).addClass("completed-field");
              $(this).removeClass("is-danger");
              $(this).parent().find(".fa-exclamation-triangle").addClass("is-hidden");
              $(this).parent().find(".fa-check").removeClass("is-hidden");
              //$(this).parent().find(".fa-check").removeClass("is-hidden");
              $(this).parent().next().text("");
          } else {
              $(this).removeClass("completed-field").addClass("is-danger");
              $(this).parent().find(".fa-exclamation-triangle").removeClass("is-hidden");
              $(this).parent().find(".fa-check").addClass("is-hidden");

              if ($(this).val().length < 6) {
                  $(this).parent().next().text("Username must be at least 6 characters.");
              } else { // must not have only alphanumeric values.
                  $(this).parent().next().text("Username must only contain alphanumeric values");
              }
          }
      });

      // code for password field
      var password_value;
      //var password_tick= $("#signup-form #student_password").parent().find(".fa-check");
      var password_tick;
      var password_cross;
      var password_help;
      var password_confirmation;
      $("form").on('input', '.password', function() {
          var form_id = $(this).closest("form").attr('id');
          password_confirmation = $(`#${form_id} .password_confirmation`);

          password_help = $(this).parent().next();

          password_tick = $(`#${form_id} .password`).parent().find(".fa-check");
          password_cross = $(`#${form_id} .password`).parent().find(".fa-exclamation-triangle");
          password_value = $(this).val();

          if ((password_confirmation.val().length > 0) && password_value != password_confirmation.val()) {
              addStatus(password_confirmation, "error", "Confirmation password must match the password.");
          } else {
             if (password_value == password_confirmation.val()) {
                  addStatus(password_confirmation, "success");
             }
          }

          if (password_value.length < 8) {
              //password_tick.addClass("is-hidden");
              if (!$(this).hasClass("is-danger")) {
                  addStatus($(this), "error", "Password must be at least 8 characters.");
                /*  $(this).addClass("is-danger").removeClass("completed-field");
                  password_cross.removeClass("is-hidden");
                  password_tick.addClass("is-hidden");
                  password_help.text() */
              } // else do nothing since error status already indicated
          } else {
              if ($(this).hasClass("is-danger")) { // no is-success to use.
                  //password_tick.removeClass("is-hidden");
                  addStatus($(this), "success");
              } // else do nothing since the success status is already indicated.
          }
      });

// extra input function not needed.
/*      $("form").on('input', '.password', function() {
          $(this).removeClass("is-danger is-success completed-field");
          $(this).parent().find(".is-right i").addClass("is-hidden");
          $(this).parent().next().text("");
          if ($(this).length < 8) {
              $(this).removeClass("completed-field");
          }
      });
*/
      // code for confirmation field
      var confirmation_value;
      var original_value;
      var confirmation_tick;
      var confirmation_cross;
      var confirmation_help;
      //var original_field_id;
      $("form").on('input', '.password_confirmation', function() {
          var form_id = $(this).closest("form").attr("id");
          //original_field_id = $(this).attr("id").replace("_confirmation", "");
          //original_arr = original_field_id.split("_");
          //field_name = original_arr[original_arr.length - 1];

          confirmation_tick = $(`#${form_id} .password_confirmation`).parent().find(".fa-check");
          confirmation_cross = $(`#${form_id} .password_confirmation`).parent().find(".fa-exclamation-triangle");
          confirmation_help = $(this).parent().next();

          confirmation_value = $(this).val();
          original_value = $(`#${form_id} .password`).val();
          //console.log($("#signup-form #student_password_confirmation").parent());
          if (confirmation_value != original_value) {
              if (!$(this).hasClass("is-danger")) {
                  if (confirmation_value.length != 0) {
                      addStatus($(this), "error", "Confirmation password must match the password.")
                  } else { // confirmation value == 0
                      addStatus($(this), "neutral");
                  }
              }
          } else {
              if (confirmation_value.length == 0) {
                  addStatus($(this), "neutral");
              } else {
                  addStatus($(this), "success");
              }
          }
      });

/*
      $("form").on('input', '.confirmation', function() {
          $(this).removeClass("is-danger is-success completed-field");
          $(this).parent().find(".is-right i").addClass("is-hidden");
          $(this).parent().next().text("");
          if ($(this).length < 8) {
              $(this).removeClass("completed-field");
          }
      });
*/
      // code for email field
      var email_value;
      var email_tick;
      var email_cross;
      var email_help;
      var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
      $("form").on('input', '.email', function() {
          var form_id = $(this).closest("form").attr("id");

          email_tick = $(`#${form_id} .email`).parent().find(".fa-check");
          email_cross = $(`#${form_id} .email`).parent().find(".fa-exclamation-triangle");
          email_help = $(this).parent().next();

          email_value = $(this).val();
          //console.log($("#signup-form #student_password_confirmation").parent());
          if (mailformat.test(email_value)) {
                  addStatus($(this), "success");
          } else {
              if (!$(this).hasClass("is-danger")) {
                  addStatus($(this), "error");
              }
          }
      });

/*
      $("form").on('input', '.email', function() {
          $(this).removeClass("is-danger is-success completed-field");
          $(this).parent().find(".is-right i").addClass("is-hidden");
          $(this).parent().next().text("");
      });
*/
      async function getUrl(url) {
        const response = await fetch(url, {
          method: "GET",
          headers: { "Content-type": "application/js" }
        });
        return response.json();
      }

      function addStatus(div, status, message = "") {
          if (status == "error") {
              div.removeClass("completed-field");
              div.addClass("is-danger");
              div.parent().find(".fa-exclamation-triangle").removeClass("is-hidden");
              div.parent().find(".fa-check").addClass("is-hidden");
              div.parent().next().text(message);
          } else {
              div.removeClass("is-danger");
              div.parent().next().text("");

              if (status == "success") {
                   div.parent().find(".fa-exclamation-triangle").addClass("is-hidden");
                   div.parent().find(".fa-check").removeClass("is-hidden");
                   div.addClass("completed-field");
               } else if (status == "neutral") {
                   div.parent().find(".fa-exclamation-triangle").addClass("is-hidden");
                   div.parent().find(".fa-check").addClass("is-hidden");
               } else {

               }
          }
      }


});
