$(document).on('turbolinks:load', function () {

    //var note_slider_container = $("section.note_slider_container");
    //var note_slider = note_slider_container.find(".note_slider");

    var current_slider_index;
    var slider_notes_array;
    var max_pages;

    var form_button;

    var note_slider_container;
    var note_slider;

    if ($(".note_slider_container")[0] !== null) { // then the slider exists on page.

        $("#dashboard-body").on("submit", "section.note_slider_container form", function(e) { // for the right hand slider
              note_slider = $(this).parent().find(".note_slider");
              current_slider_index = parseInt(note_slider.data("current_slider_index"));
              slider_notes_array = note_slider.data("notes_id_array");

              $("<input />").attr("type", "hidden")
                  .attr("name", "note[slider_page_id]")
                  .attr("value", current_slider_index)
                  .appendTo($(this));

              $("<input />").attr("type", "hidden")
                  .attr("name", "note[slider_notes_array]")
                  .attr("value", slider_notes_array)
                  .appendTo($(this));

              $("<input />").attr("type", "hidden")
                  .attr("name", "note[note_type]")
                  .attr("value", note_slider.data("note_type"))
                  .appendTo($(this));

              return true;
        });

        $("#dashboard-body").on("click", "section.note_slider_container .left_slider_button", function(e) {
              // make it zero index
              note_slider_container = $(this).closest("section.note_slider_container");
              note_slider = $(this).parent().find(".note_slider");
              current_slider_index = parseInt(note_slider.data("current_slider_index")) - 1;
              var prev_slider_index = current_slider_index - 1;

              note_slider_container.find(".other_notes_page_counter").text(current_slider_index);
              note_slider.data("current_slider_index", current_slider_index)
              max_pages = parseInt(note_slider.data("slider_max_pages"));

              if (current_slider_index == 1) {
                  note_slider_container.find(".left_slider_button").attr("disabled", true);
                  note_slider_container.find(".right_slider_button").removeAttr("disabled");                                  
              }

              [current_slider_index * 3, ((current_slider_index * 3) + 1),
                       ((current_slider_index * 3) + 2)].forEach(function(index) {
                        console.log(index);
                    note_slider.find(`.column[data-card-index='${index}']`)
                                .addClass("is-hidden").removeClass("is-invisible");
              }); 


              [prev_slider_index * 3, ((prev_slider_index * 3) + 1),
                       ((prev_slider_index * 3) + 2)].forEach(function(index) {
                    note_slider.find(`.column[data-card-index='${index}']`)
                                .removeClass("is-hidden");
              }) 

        });






    }
});