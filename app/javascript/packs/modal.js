$(document).on('turbolinks:load', function () {

    // no validations required for the login form
    $("nav").on('click', '.modal-button', function() {
        // button contains the index of the corresponding tab in signup/login modal
        tab_index = parseInt($(this).data("tab-link"));

        //console.log(tab_index);
        $("#sign_in_or_register_modal").addClass("is-active");

        matching_tab = $("#sign_in_or_register_modal .tabs li.tab");

        matching_tab.eq(tab_index).trigger("click");

    });

    /* closing the modal function
    $("#sign_in_or_register_modal").on('click', '.m-close', function() {
        $(this).closest("#sign_in_or_register_modal").addClass("is-invisible");//.removeClass("is-active");
    }); */


    // ensure top right cross closes any modal currently open
    $("body").on("click", ".modal.is-active .m-close", function() {
        $(this).closest(".modal").removeClass("is-active");
        $("html").removeClass("overflow-y-hidden");
    });

})
