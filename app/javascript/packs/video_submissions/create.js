
$(document).on('turbolinks:load', function () {

    // display video player inside the video input field when a video file is uploaded.
    $("#new_video_submission_modal_body").on("change", "#upload_video_submission input[type='file']", function(event) {
        var file = event.target.files[0];
        var blobURL = URL.createObjectURL(file);
        var video = $("#new_video_submission_modal_body video.video-player");
        var video2 = $("#new_video_submission_modal_body video.video-player-two");

        video.attr("src", blobURL);
        video2.attr("src", blobURL);
        video.removeClass("is-hidden");

        var upload_text = $("#new_video_submission_modal_body .file-cta > span.file-label");
        upload_text.addClass("is-hidden");

        var figure_image = $("#new_video_submission_modal_body .control.file figure.image");
        figure_image.addClass("is-hidden");

        $(this).closest(".file").find(".video-delete-bar").removeClass("is-hidden");
        $("#new_video_submission_modal_body .below-video-info .video-title").text(file.name);
    });

    $("#new_video_submission_modal_body .video-delete-bar").on("click", ".delete-video", function() {
            var control_file = $(this).closest(".file")
            var input = control_file.find("input[type='file']");
            input[0].value = null;

            control_file.find(".video-delete-bar").addClass("is-hidden");
            var cta = control_file.find(".file-cta");

            cta.find("video").addClass("is-hidden");
            cta.find(".below-video-info .video-title").empty();

            cta.find("span.file-label").removeClass("is-hidden");
            cta.find("figure.image").removeClass("is-hidden");
    });

    /* $("#new_video_submission_modal_body").on("click", "footer button[type='submit']", function() {
            var form = $("#new_video_submission_modal_body form:not(.is-hidden)");
            Rails.fire(form[0], "submit");
            const input = $("#new_video_submission_modal_body #new_video_submission_video_input");
            const file = input[0].files[0];
            const url = input.data("direct-upload-url");
            const upload = new DirectUpload(file, url);

            upload.create((error, blob) => { 
            // handle errors OR persist to the model using 'blob.signed_id'
                    console.log(error);
                    console.log(blob);
                    console.log("directly uploaded without form");
            })
    }); */

    $("#new_video_submission_modal_body #upload_video_submission input[type='file']").on("direct-upload:start", function(event) {
            var progressElement = $("#new_video_submission_modal_body label progress.is-success"); 
            progressElement.removeClass("is-hidden");

            $("#upload_video_submission footer input[type='submit']")[0].disabled = true;
    });

    $("#new_video_submission_modal_body #upload_video_submission input[type='file']").on("direct-upload:before-blob-request", function(event) {            
            const { id, file, xhr } = event.detail;
            xhr.onreadystatechange = function (evt) {
                if (xhr.readyState !== 4) {
                    return;
                } else {
                    console.log(xhr.response.id);
                    $("form#upload_video_submission_form input#video_submission_video_key")[0].value = xhr.response.id;
                    //$("form#upload_video input#initial_video_key").val(xhr.response.id);
                }
            };
    });

    $("#new_video_submission_modal_body #upload_video_submission input[type='file']").on("direct-upload:progress", function(event) {
            const { id, progress } = event.detail;
            var progressElement = $("#new_video_submission_modal_body label progress.is-success"); 
            progressElement.attr("value", `${progress}`);
    });

    $("#new_video_submission_modal_body #upload_video_submission input[type='file']").on("direct-upload:end", function(event) {
            var progressElement = $("#new_video_submission_modal_body label progress.is-success"); 
            progressElement.addClass("is-hidden");

            var submit_button = $("#upload_video_submission footer input[type='submit']");
            submit_button[0].disabled = false;
            submit_button.text("Create competition");

            $("#new_video_submission_modal_body form#upload_video_submission").addClass("is-hidden");
            $("#new_video_submission_modal_body form#upload_video_submission_form").removeClass("is-hidden");
    });
});