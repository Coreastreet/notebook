
$(document).on('turbolinks:load', function () {

   $("#show-video-submission .video-description").on("click", ".read-link", function() {
            var opened = $(this).data("read-more");
            if (opened == "true") {
                $("#requirements_text").removeClass("h-auto");
                $(this).find("span").text("SHOW MORE");
                $(this).find("i").removeClass("fa-angle-right");
                $(this).find("i").addClass("fa-angle-left");
                $(this).data("read-more", false);
            } else { // false
                $("#requirements_text").addClass("h-auto");
                $(this).find("span").text("SHOW LESS");
                $(this).find("i").removeClass("fa-angle-left");
                $(this).find("i").addClass("fa-angle-right");
                $(this).data("read-more", true);
            }
    });

    
})