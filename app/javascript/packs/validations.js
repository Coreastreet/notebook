$(document).on('turbolinks:load', function () {

      // code for removing the error on input for signup.
      $("form#login-modal").on("input", "#session_username, #session_password", function(){
          if ($(this).hasClass("is-danger")) {
              addStatus($(this), "neutral");
              $(this).closest("form").find(".notification").addClass("is-hidden").text("");
          }
      });

      // check new password is > 8 character, different from the old one and matches.
      $("form#change_password_form").on("input", "input#student_old_password", function() {
            if (this.value.length < 8 || this.value.length > 150) {
              $(this).addClass("is-danger");
              $(this).parent().next().text("Passwords must contain at least 8 characters.");
            } else { // length of value is greater than 6 or more characters.
              $(this).removeClass("is-danger");
              $(this).parent().next().empty();
            }
      });

      $("form#change_password_form").on("input", "input#student_password", function() {
          if (this.value.length < 8 || this.value.length > 150) {
            $(this).addClass("is-danger");
            $(this).parent().next().text("Passwords must contain at least 8 characters.");
          } else { // length of value is greater than 6 or more characters.
            if ($(this).val() == $("input#student_old_password").val()) {
                $(this).addClass("is-danger");
                $(this).parent().next().text("New passwords must different from the old password.");
            } else {
                $(this).removeClass("is-danger");
                $(this).parent().next().empty();
            }
          }
      });

      var password_value;
      $("form#change_password_form").on("input", "input#student_password_confirmation", function() {
          if ($(this).val() == $("input#student_password").val()) {
            $(this).removeClass("is-danger");
            $(this).parent().next().empty();
          } else { // length of value is greater than 6 or more characters.
            $(this).addClass("is-danger");
            $(this).parent().next().text("Passwords must match.");
          }
      });

      // check email is valid

      $("form#change_email_form").on("input", "input#student_email", function() {
          if (validateEmail($(this).val())) {
            $(this).removeClass("is-danger");
            $(this).parent().next().empty();
          } else { // length of value is greater than 6 or more characters.
            $(this).addClass("is-danger");
            $(this).parent().next().text("Please enter a valid email.");
          }
      });

      $("form#change_email_form").on("input", "input#student_email_confirmation", function() {
          if ($(this).val() == $("input#student_email").val()) {
            $(this).removeClass("is-danger");
            $(this).parent().next().empty();
          } else { // length of value is greater than 6 or more characters.
            $(this).addClass("is-danger");
            $(this).parent().next().text("Emails must match.");
          }
      });

      function validateEmail(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
      }

      function addStatus(div, status, message = "") {
          if (status == "error") {
              div.removeClass("completed-field");
              div.addClass("is-danger");
              div.parent().find(".fa-exclamation-triangle").removeClass("is-hidden");
              div.parent().find(".fa-check").addClass("is-hidden");
              div.parent().next().text(message);
          } else {
              div.removeClass("is-danger");
              div.parent().next().text("");

              if (status == "success") {
                   div.parent().find(".fa-exclamation-triangle").addClass("is-hidden");
                   div.parent().find(".fa-check").removeClass("is-hidden");
                   div.addClass("completed-field");
               } else if (status == "neutral") {
                   div.parent().find(".fa-exclamation-triangle").addClass("is-hidden");
                   div.parent().find(".fa-check").addClass("is-hidden");
               } else {

               }
          }
      }

});
