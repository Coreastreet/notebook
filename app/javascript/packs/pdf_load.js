$(document).on('turbolinks:load', function () {

      $("body").on("submit", ".upload_modal.is-active form", function(event) {
           if( checkFormFilled(this) ) {
              // don't create a <button> element, since it will prevent a remote rails submit
           } else {
             return false;
           }
      });

      function checkFormFilled(form) {
          var formFilled = true;
          var inputNodes = form.querySelectorAll(".field .control");
          var inputNode;
          var choices;
          var closest_control;
          for (let i = 0; i < inputNodes.length; i++) {
              inputNode = inputNodes[i].querySelector("input[type='text']:not(.optional),\
                                                          input[type='number']:not(.optional),\
                                                          input[type='file']:not(.optional),\
                                                          input[type='hidden']:not(.optional),\
                                                          textarea:not(.optional)");
              console.log(inputNode);
              if (inputNode !== null) {// meaning that an input tag exists
                  if (inputNode.matches("input[type='file']") && (inputNode.files.length == 0)){
                      closest_control = $(inputNode).closest(".control");
                      closest_control.addClass("is-danger");
                      closest_control.find(".help").text(`${$(inputNode).data("missing-error")}`);
                      formFilled = false;
                  } else { // matches other types i.e. number, text and textarea.
                    if (inputNode.value.length === 0) { // meaning that a compulsory input field has not been filled out.
                        if (!inputNode.matches("input[type='hidden']")) {
                            inputNode.classList.add("is-danger");
                            inputNode.parentElement.nextElementSibling.textContent = inputNode.dataset.missingError
                            formFilled = false;
                        } else {
                            formFilled = false;
                        }
                    }
                  }
              } else { // check other field types which contain multiple options and require at least one choice.
                  var one_choice_picked = false;
                  choices = inputNodes[i].querySelectorAll("input[type='radio']");
                  if (choices.length > 1) {
                      for (let j = 0; j < choices.length; j++) {
                          if (choices[j].checked) {
                            one_choice_picked = true;
                          }
                      }
                      if (one_choice_picked === false) {  // no option picked
                          console.log("no option picked!");
                          formFilled = false;
                      }
                  }
              }
          }
          return formFilled;
      }

      // validation client-side on form for modal uploads.
      // check length of title is between 6 and 150 characters.
      $("body").on("input", ".upload_modal.is-active form input#note_title", function() {
          if (this.value.length < 6 || this.value.length > 150) {
            $(this).addClass("is-danger");
            $(this).parent().next().text("Titles must be between than 6 and 150 characters.");
          } else { // length of value is greater than 6 or more characters.
            $(this).removeClass("is-danger");
            $(this).parent().next().empty();
          }
      });

      // for restricting the input to only numbers only
      $("body").on("keypress", ".upload_modal.is-active form input#note_price", function(event) {
          return isNumberKey(event);
      });

      $("body").on("input", ".upload_modal.is-active form input#note_price", function() {
          if ($(this).val().length > 0) {
              $(this).removeClass("is-danger");
              $(this).parent().next().empty();
          } else {
              $(this).addClass("is-danger");
              $(this).parent().next().text("Please enter a price.");
          }
      });

      function isNumberKey(evt){
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57))) {
              return false;
          }
          return true;
      }

      // Check category entered matches one of the options.
      var subject_options_array = [];
      $("datalist#subjects option").each(function() {
          subject_options_array.push($(this).attr("value"));
      });

      var datalist_courses = $("datalist#courses");
      var course_options_array = [];
      var notes_course = document.querySelector(".upload_modal.is-active form input#note_course");

  /*  $("body").on("input", ".upload_modal.is-active form input#note_subject", function() {
          if (!subject_options_array.includes(this.value)) { // if value does not match with one of the options
              $(this).parent().next().text("Subjects must match one of the options provided.");
              $(this).addClass("is-danger");
              datalist_courses.empty();
              notes_course.disabled = true;
          } else {
              $(this).removeClass("is-danger");
              $(this).parent().next().empty();
              course_options_array = $(`datalist#subjects option[value=${this.value}]`).data("courses");
              for (const course of course_options_array) {
                    datalist_courses.append(`<option value="${course}"></option>`)
              };
              notes_course.disabled = false;
          }
      }); */
      var data_subject;
      $("body").on("input", ".upload_modal.is-active form input#note_course", function() {
          $("datalist#courses option").each(function() {
              course_options_array.push($(this).attr("value"));
          });
          if (!course_options_array.includes(this.value)) { // if value does not match with one of the options
              $(this).parent().next().text("Courses must match one of the options provided.");
              $(this).addClass("is-danger");
          } else {
              $(this).removeClass("is-danger");
              $(this).parent().next().empty();
              data_subject = $(`datalist#courses option[value="${this.value}"]`).data("subject");
              $(this).closest("form").find("input#note_subject").val(data_subject);
          }
      });

      // on upload of file, display the pdf.
      //$('form').on("change", ".file_upload input[type=file]", function() {

      //    if (this.files.length > 0) {
            // get url of local pdf file uploaded
            // instead of replacing name, we replace with a preview of the pdf
              //var url = 'https://raw.githubusercontent.com/mozilla/pdf.js/ba2edeae/examples/learning/helloworld.pdf';
              // Loads the actual pdf
              // Asynchronous download of PDF

              /* use readURL later for pdfs 
              read_pdf_url(this, this.closest("form"));
              */
          //    read_image_url(this, this.closest("form"));
          //    $("#general_modal_body form .alert").empty();

          //}
          //console.log("testing if this code is running!");
      //});

       $("#upload_modal_form").on("input", "input#course_title", function() {
            if ($(this).val().length > 6 && $(this).val().length < 50) {
                $(this).removeClass("is-danger");
                $(this).closest(".field").find(".help.is-danger").text("");
            }
       });

       $("#upload_modal_form").on("input", "textarea#course_description", function() {
            if ($(this).val().length > 20 && $(this).val().length < 200) {
                $(this).removeClass("is-danger");
                $(this).closest(".field").find(".help.is-danger").text("");
            }
       });

       $("#upload_modal_form").on("input", "input#course_subject", function() {
            if ($(`datalist#subjects option[value="${this.value}"]`).length == 1) {
                $(this).removeClass("is-danger");
                $(this).closest(".field").find(".help.is-danger").text("");
            }
       });

      $("#image_delete_bar").on("click", ".delete-image", function() {
            var control_file = $(this).closest(".control.file")
            var input = control_file.find("input[type='file']")[0];
            input.value = null;

            $(input).closest("label").removeClass("is-hidden");
            $("#image_holder").parent().addClass("is-hidden");
            $("#blank_image_holder").parent().removeClass("is-hidden");
      });

      function read_image_url(input, form) {

          if (input.files && input.files[0]) {
              var reader = new FileReader();
              var image_holder = $(form).find('img#image_holder');
              var blank_image_holder = $(form).find('img#blank_image_holder');
              var file_control = blank_image_holder.closest(".file.control");

              var file_name = input.files[0].name;
              var file_byte_size = input.files[0].size;

              reader.onload = function (e) {
                  blank_image_holder.parent().addClass("is-hidden");
                  $(form).find('label.file-label').addClass("is-hidden");

                  image_holder
                  .attr('src', e.target.result)

                  // assign values to hidden fields.
                  $(form).find("#course_file_name").val(file_name);
                  $(form).find("#course_file_size").val(file_byte_size);

                  image_holder.parent()
                  .removeClass("is-hidden");

                  // remove the danger error if it exists
                  file_control.removeClass("is-danger");
                  file_control.find(".help.is-danger").text("");

              }

              reader.readAsDataURL(input.files[0]);
          }
      }

      function read_pdf_url(input, form) {
            if (input.files && input.files[0]) {
              var file_type = validate_file(input.files[0], form);
              var upload_file_name = input.files[0].name;
              var reader = new FileReader();

              if (file_type == "pdf") {
                  // above: file upload both matches pdf or doc/docx and is less than 5MB.
                  // use this later for file name
                  reader.onload = function(e) {
                      var blob = new Blob([e.target.result], {type: 'application/pdf'});
                      //$('#blah').attr('src', e.target.result);
                      url = window.URL.createObjectURL(blob);
                      console.log(url);

                      var loadingTask = pdfjsLib.getDocument(url);

                      // implement functions for next/prev pages in pdf.
                      var pdfDoc = null,
                          pageNum = 1,
                          pageRendering = false,
                          pageNumPending = null,
                          scale = 1,
                          canvas = form.querySelector('canvas.pdf_container'),
                          ctx = canvas.getContext('2d'),
                          //canvas_full = document.getElementById("full_hidden_canvas"),
                          //ctx_full = canvas_full.getContext('2d'),
                          viewport,
                          new_scale,
                          new_viewport;


                      function renderPage(num, pdfDoc, first_page_to_image) {
                          pageRendering = true;
                          // Using promise to fetch the page
                          pdfDoc.getPage(num).then(function(page) {
                            viewport = page.getViewport({scale: scale});
                            //scale = canvas.width/viewport.width;
                            // for the first render new_scale is undefined
                            if (new_scale == undefined) {
                                new_scale = canvas.width / viewport.width;
                                //console.log(`first render ${canvas.width}, ${viewport.width}, ${new_scale}`);
                                new_viewport = page.getViewport({scale: new_scale});
                            }
                            //redefine the viewport based on the new scale
                            canvas.height = 420;//new_viewport.height;
                            canvas.width = 300;//new_viewport.width;

                            // Render PDF page into canvas context
                            var renderContext = {
                              canvasContext: ctx,
                              viewport: new_viewport
                            };

                            var renderTask = page.render(renderContext);

                            /*canvas_full.height = viewport.height;
                            canvas_full.width = parseFloat((viewport.height / 1.4).toFixed(2));

                            var renderContext_full = {
                              canvasContext: ctx_full,
                              viewport: viewport
                            };

                            var renderTask_full = page.render(renderContext_full);
                            */

                            // Wait for rendering to finish
                            renderTask.promise.then(function() {
                              pageRendering = false;
                              $(form).find(".control.file_upload").addClass("absolute-behind");
                              $(form).find(".canvas_holder").removeClass("absolute-behind");
                              $(form).find(".pdf_title_holder").text(upload_file_name);
                              // get image of first page
                              /* if (first_page_to_image === false) {
                                  $("input#note_pdf_thumbnail").val(canvas.toDataURL('image/webp'))
                                  first_page_to_image === true;
                              } */
                              if (pageNumPending !== null) {
                                // New page rendering is pending
                                renderPage(pageNumPending, pdfDoc, true);
                                pageNumPending = null;
                              }
                            });

                            /* Wait for rendering to finish
                            renderTask_full.promise.then(function() {
                              // get image of first page - make sure it is full page to ensure good resolution
                              if (first_page_to_image === false) {
                                  $(form).find(".file_upload").addClass("is-hidden");
                                  // pdf preview gotten from the previewer instead.
                                  //$("input#note_pdf_thumbnail").val(canvas_full.toDataURL('image/webp'))
                                  first_page_to_image === true;
                              }
                            }); */
                          });

                          // Update page counters
                          $('#page_num').text(num);
                      }

                      function queueRenderPage(num, pdfDoc) {
                          if (pageRendering) {
                            console.log(`current page still rendering. Wait`)
                            pageNumPending = num;
                          } else {
                            console.log(`rendering page ${num}`)
                            renderPage(num, pdfDoc, true);
                          }
                      }

                      loadingTask.promise.then(function(pdf) {
                        $('#page_count').text(pdf.numPages);
                        //$('#note_number_of_pages').val(pdf.numPages);
                        // Initial/first page rendering
                        //console.log(pageNum);
                        renderPage(pageNum, pdf, false);


                        $('#prev').on('click', function() {
                            //onPrevPage(pageNum, pdf);
                            // directly insert function so have access to outer variables
                            if (pageNum > 1) {
                              pageNum--;
                              queueRenderPage(pageNum, pdf);
                            } else {
                              return;
                            }
                        });
                        $('#next').on('click', function(){
                            //onNextPage(pageNum, pdf);
                            // same as above
                            if (pageNum < pdf.numPages) {
                              pageNum++;
                              queueRenderPage(pageNum, pdf);
                            } else {
                              return;
                            }
                        });
                        window.URL.revokeObjectURL(url);
                      }, function (reason) {
                        // PDF loading error
                          console.log(reason);
                      });
                    }

                  reader.readAsArrayBuffer(input.files[0]); // convert to base64 string
               } else if (file_type == "docx") { // which includes both docx and doc. Only display the name of the file.
                  reader.onload = function(e) {
                      $(form).find("a.doc-title")
                              .text(upload_file_name)
                               .attr('href', e.target.result)
                                .removeClass("is-hidden");
                      $(form).find("input#note_number_of_pages").addClass("optional");
                  };
                  reader.readAsDataURL(input.files[0]);
               } else { // upload does not match the file types allowed or exceeds the size limit.
                 // No upload is thus allowed.
                    console.log(file_type);
                    return false;
               }
            }
      }

      function validate_file(file, form) {
            if (!["application/pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"].includes(file.type)) {
                $(form).find(".file_upload .help").text("Upload file type does not match any of those allowed.")
                $(form).find(".file_upload").addClass("is-danger");
                console.log(`no file type match ${file.type}`);
                return false;
            } else if (file.size > 5000000) { // 5 megabytes
                $(form).find(".file_upload .help").text("Upload file size exceeeds the limit of 5MB.")
                $(form).find(".file_upload").addClass("is-danger");
                console.log(`file size > 5MB ${file.size}`);
                return false;
            } else {
                if (file.type == "application/pdf") {
                  return "pdf";
                } else { // must be docx format then
                  return "docx";
                }
            }
      }
});
