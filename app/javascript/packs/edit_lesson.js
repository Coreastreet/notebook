$(document).on('turbolinks:load', function () {

    window.addEventListener("trix-file-accept", function(event) {
        const acceptedTypes = ['image/jpeg', 'image/jpg', 'image/webp', 'image/png']
        if (!acceptedTypes.includes(event.file.type)) {
          event.preventDefault()
          alert("This text editor only supports the attachment of jpeg, webp or png image files")
        } else {
            const maxFileSize = 1024 * 1024; // 1MB 
            if (event.file.size > maxFileSize) {
                event.preventDefault()
                alert("This text editor only supports attachment files upto size 1MB!");
            }
        }
    });

    // fire the submit lesson text form.
    $("#edit-lesson-body").on("click", "button#update_lesson_details", function() {
            var form = $("#edit-lesson-body #edit_lesson_form");
            Rails.fire(form[0], "submit");
    });

    // display video player inside the video input field when a video file is uploaded.
    $("#edit-lesson-body").on("change", "#lesson_video_input", function(event) {
            var file = event.target.files[0];
            var blobURL = URL.createObjectURL(file);
            var video = $("#edit-lesson-body video.video-player");

            var edit_lesson_form_cta = $("#edit-lesson-body #edit_lesson_form .file-cta > span");
            edit_lesson_form_cta.addClass("is-hidden");

            video.attr("src", blobURL);
            video.removeClass("is-hidden");

            $(this).closest(".file").find(".video-delete-bar").removeClass("is-hidden");

            $("#edit_lesson_form .below-video-info .video-title").text(file.name);
            
            var progressElement = $("#edit-lesson-body #edit_lesson_form .below-video-info");
            progressElement.removeClass("is-hidden");
    });

    $("#edit-lesson-body #lesson_video_input").on("direct-upload:start", function(event) {
            var progressElement = $("#edit_lesson_form progress.is-success"); 
            progressElement.removeClass("is-hidden");

            $("#heading-fixed-bar button#update_lesson_details")[0].disabled = true;
    });

    $("#edit-lesson-body #lesson_video_input").on("direct-upload:before-blob-request", function(event) {            
            const { id, file, xhr } = event.detail;
            xhr.onreadystatechange = function (evt) {
                if (xhr.readyState !== 4) {
                  return;
                } else {
                    console.log(xhr.response.id);
                    $("form#update_lesson_video_form input#lesson_video_key").val(xhr.response.id);
                }
            };
    });

    $("#edit-lesson-body #lesson_video_input").on("direct-upload:progress", function(event) {
            const { id, progress } = event.detail;
            var progressElement = $("#edit_lesson_form progress.is-success"); 
            progressElement.attr("value", `${progress}`);
    });

    $("#edit-lesson-body #lesson_video_input").on("direct-upload:end", function(event) {
            var progressElement = $("#edit_lesson_form progress.is-success"); 
            progressElement.addClass("is-hidden");

            $("#heading-fixed-bar button#update_lesson_details")[0].disabled = false;
            var form = $("form#update_lesson_video_form");
            Rails.fire(form[0], "submit");
    });

    // display video player inside the video input field when a video file is uploaded.
    $("#edit_lesson_form .video-delete-bar").on("click", ".delete-video", function() {
            var control_file = $(this).closest(".file")
            var input = control_file.find("input#lesson_video_input");
            input[0].value = null;

            control_file.find(".video-delete-bar").addClass("is-hidden");
            var cta = control_file.find(".file-cta");

            cta.find("video").addClass("is-hidden");
            cta.find("span").removeClass("is-hidden");

            var progressElement = $("#edit-lesson-body #edit_lesson_form .below-video-info");
            progressElement.addClass("is-hidden");
    });

});