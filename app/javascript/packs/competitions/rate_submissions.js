$(document).on('turbolinks:load', function () {
    $("#video_submissions_table").on("click", ".rating-bars option", function() {
            var rating_bars = $(this).parent().children();
            var rating_value = parseInt($(this).attr("value"));

            var selected_bars = rating_bars.filter(function(index) {
                return (index < rating_value);
            });

            var not_selected_bars = rating_bars.filter(function(index) {
                return (index >= rating_value);
            });

            selected_bars.addClass("selected");
            not_selected_bars.removeClass("selected");

            // set the value to rating
            var rating_holder = $(this).parent().find(".rating-holder");
            var rating_input = $(this).closest("td").find("#video_submission_rating");

            rating_holder.text(rating_value);
            rating_input.val(rating_value);
    });

    $("#video_submissions_table").on("click", "form.submit-rating .rating-dropdown-menu .dropdown-item", function() {
            var place = parseInt($(this).data("place"));
            var award_holder = $(this).closest(".dropdown").find(".award-type");
            var ranking_input = $(this).closest("form.submit-rating").find("#video_submission_ranking");
            console.log(ranking_input);
            if (place == 1) {
                award_holder.text("1st place");
                ranking_input.val(1);
            } else if (place == 2) {
                award_holder.text("2nd place");
                ranking_input.val(2);
            } else if (place == 3) {
                award_holder.text("3rd place");
                ranking_input.val(3);
            } else { // no award
                award_holder.text("No award");
                ranking_input.removeAttr("value");
            }
    });

});