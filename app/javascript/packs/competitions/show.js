$(document).on('turbolinks:load', function () {
        // any tag bar
        $(".tag-bar .tabs").on("click", "li", function() {
            $(this).siblings().removeClass("is-active");
            $(this).addClass("is-active");

            var chosen_content = $(`#content-tab-box > [data-content=${$(this).data("content-link")}]`);
            chosen_content.siblings().not(".modal").addClass("is-hidden");
            chosen_content.removeClass("is-hidden");
        });

        $("#requirements_section").on("click", ".read-link", function() {
            var opened = $(this).data("read-more");
            if (opened == "true") {
                $("#requirements_text").removeClass("h-auto");
                $(this).find("span").text("SHOW MORE");
                $(this).find("i").removeClass("fa-angle-right");
                $(this).find("i").addClass("fa-angle-left");
                $(this).data("read-more", "false");
            } else { // false
                $("#requirements_text").addClass("h-auto");
                $(this).find("span").text("SHOW LESS");
                $(this).find("i").removeClass("fa-angle-left");
                $(this).find("i").addClass("fa-angle-right");
                $(this).data("read-more", "true");
            }
        });

        $("section#header_box").on("click", "button#upload_video_submission_button", function() {
                $("#content-tab-box > .modal.new_video_submission").addClass("is-active");
        });
});
