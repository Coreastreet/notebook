$(document).on('turbolinks:load', function () {

        $("#my-notebook-page-holder").on("click", ".card span.new-submission-button", function() {
                $(this).closest(".card").find(".modal").addClass("is-active");
        });

        $("#show_competition_container .modal-content.video-submission").on("change", ".video_file_submission input[type='file']", function(event) {
            var file = event.target.files[0];
            var blobURL = URL.createObjectURL(file);
            var modal_content = $(this).closest(".modal-content");
            var video = modal_content.find("video.video-player");
            var video2 = modal_content.find("video.video-player-two");
    
            video.attr("src", blobURL);
            video2.attr("src", blobURL);
            video.removeClass("is-hidden");
    
            var upload_text = modal_content.find(".file-cta > span.file-label");
            upload_text.addClass("is-hidden");
    
            var figure_image = modal_content.find(".control.file figure.image");
            figure_image.addClass("is-hidden");
    
            $(this).closest(".file").find(".video-delete-bar").removeClass("is-hidden");
            modal_content.find(".below-video-info .video-title").text(file.name);
        });
    
        $("#show_competition_container .modal-content.video-submission").on("click", ".video-delete-bar .delete-video", function() {
                var control_file = $(this).closest(".file")
                var input = control_file.find("input[type='file']");
                input[0].value = null;
    
                control_file.find(".video-delete-bar").addClass("is-hidden");
                var cta = control_file.find(".file-cta");
    
                cta.find("video").addClass("is-hidden");
                cta.find(".below-video-info .video-title").empty();
    
                cta.find("span.file-label").removeClass("is-hidden");
                cta.find("figure.image").removeClass("is-hidden");

                var modal_content = $(this).closest(".modal-content");
                var edit_details_button = modal_content.find("footer #edit_details");
                if (edit_details_button.length == 1) { // modal is the update modal
                    edit_details_button.addClass("is-hidden"); // edit_details button is hidden
                    modal_content.find("footer input[type='submit']")[0].disabled = false;
                }
        });

        $("#show_competition_container .modal-content.video-submission").on("direct-upload:start", ".video_file_submission input[type='file']", function(event) {
                var modal_content = $(this).closest(".modal-content");
                var progressElement = modal_content.find("label progress.is-success"); 
                progressElement.removeClass("is-hidden");

                $("#upload_video_submission footer input[type='submit']")[0].disabled = true;
        });

        $("#show_competition_container .modal-content.video-submission").on("direct-upload:before-blob-request", ".video_file_submission input[type='file']", function(event) {            
                const { id, file, xhr } = event.detail;
                xhr.onreadystatechange = function (evt) {
                    if (xhr.readyState !== 4) {
                        return;
                    } else {
                        console.log(xhr.response.id);
                        $("#show_competition_container .modal.is-active form.video_submission_form input#video_submission_video_key")[0].value = xhr.response.id;
                        //$("form#upload_video input#initial_video_key").val(xhr.response.id);
                    }
                };
        });

        $("#show_competition_container .modal-content.video-submission").on("direct-upload:progress", ".video_file_submission input[type='file']", function(event) {
                const { id, progress } = event.detail;
                var modal_content = $(this).closest(".modal-content");
                var progressElement = modal_content.find("label progress.is-success"); 
                progressElement.attr("value", `${progress}`);
        });

        $("#show_competition_container .modal-content.video-submission").on("direct-upload:end", ".video_file_submission input[type='file']", function(event) {
                var modal_content = $(this).closest(".modal-content");
                var progressElement = modal_content.find("label progress.is-success"); 
                progressElement.addClass("is-hidden");

                var submit_button = $("#upload_video_submission footer input[type='submit']");
                submit_button[0].disabled = false;
                submit_button.text("Create competition");

                modal_content.find("form.video_file_submission").addClass("is-hidden");
                modal_content.find("form.video_submission_form").removeClass("is-hidden");
        });

        $("#show_competition_container .modal-content.video-submission").on("click", "footer #edit_details", function() {
                var modal_content = $(this).closest(".modal-content");
                console.log(modal_content);
                modal_content.find("form.video_file_submission").addClass("is-hidden");
                modal_content.find("form.video_submission_form").removeClass("is-hidden");
        });
});
