$(document).on('turbolinks:load', function () {
        // the code below is the course details edit page.

        // enable selection of subject through tags
        $("#subject_field").on("click", ".tags .tag", function() {
            $(this).siblings().removeClass("selected");
            $(this).addClass("selected");

            var subject = $(this).data("subject");
            var selected_checkbox = $("#subject_field").find(`input[data-subject=${subject}]`);
            selected_checkbox.siblings().attr("checked", false);
            selected_checkbox.attr("checked", true);
        });

        // add and remove learning objectives when buttons clicked.
        $("#objective_fields").on("click", "#add_objective", function(){
            var objective_clone = $("#model_objective").clone();
            objective_clone.removeClass("is-hidden").removeAttr("id");
            objective_clone.find("input").attr("name", "course[objectives][]");
            objective_clone.insertBefore($("#objective_fields p.help"));
        })

        $("#objective_fields").on("click", "i.fa-times.hover-red", function(){
            $(this).closest(".control").remove();
        })

        $("#edit_competition_form").on("click", ".radio_box", function() {
            var hidden_box_form = $(this).closest(".popup_field").find(".hidden_box_form");
            hidden_box_form.removeClass("is-hidden");
        });

        const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        $("#edit_competition_form").on("click", ".hidden_box_form.deadline button.update_box_form", function() {
            var hidden_box_form = $(this).closest(".hidden_box_form");
            var date_value = hidden_box_form.find("input[type='date']").val();
            var date_numbers = date_value.match(/\d+/g);
            console.log("update-box");

            if (hidden_box_form.find("input[type='time']").length == 1) {
                var time_value = hidden_box_form.find("input[type='time']").val();            
                var time_numbers = time_value.match(/\d+/g);

                display_value = `${date_numbers[2]} ${months[parseInt(date_numbers[1]) - 1]}, ${date_numbers[0]}` + 
                                ` ( ${(parseInt(time_numbers[0]) % 12) || 12}:${time_numbers[1]} ${(time_numbers[0] < 12) ? 'am' : 'pm'} )`; 
                
                hidden_box_form[0].dataset.initialTime = time_value;
                
            } else {
                display_value = `${date_numbers[2]} ${months[parseInt(date_numbers[1]) - 1]}, ${date_numbers[0]}`;
            }

            hidden_box_form[0].dataset.initialDate = date_value;
            hidden_box_form.parent().find(".radio_box .display_value").text(display_value);
            hidden_box_form.addClass("is-hidden");
        });

        $("#edit_competition_form").on("click", ".hidden_box_form.prize_money button.update_box_form", function() {
            var hidden_box_form = $(this).closest(".hidden_box_form");

            var amount = hidden_box_form.find("input[type='number']").val();            

            hidden_box_form[0].dataset.initialValue = amount;
            hidden_box_form.parent().find(".radio_box .display_value").text(amount);
            hidden_box_form.addClass("is-hidden");
        });

        $("#edit_course_form").on("click", ".radio_box", function() {
            var hidden_box_form = $(this).closest(".popup_field").find(".hidden_box_form");
            
            var current_value = hidden_box_form[0].dataset.initialValue;
            hidden_box_form.find(`input[value=${current_value}]`)[0].checked = true;

            hidden_box_form.removeClass("is-hidden");
        });

        // close popup field
        $("#edit_course_form, #edit_competition_form").on("click", ".hidden_box_form i.fa-times", function() {
            var hidden_box_form = $(this).closest(".hidden_box_form");
            hidden_box_form.addClass("is-hidden");
        });        

        // update difficulty level and close
        $("#edit_course_form").on("click", ".hidden_box_form button.update_box_form", function() {
            var hidden_box_form = $(this).closest(".hidden_box_form");
            var checked_radio = hidden_box_form.find("input[type='radio']:checked");

            var display_value;
            var true_value;

            if (checked_radio.attr("value") == "true") {
                display_value = hidden_box_form.find("input.underline-input").val();
                true_value = "true";
            } else {
                display_value = checked_radio.data("display-value");
                true_value = checked_radio.val();
            }

            hidden_box_form[0].dataset.initialValue = true_value;
            hidden_box_form.parent().find(".radio_box .display_value").text(display_value);
            hidden_box_form.addClass("is-hidden");
        });

        $("#dashboard-body #save-lesson-order").on("click", function() {
            var form = $("#edit_course_form")[0];
            Rails.fire(form, 'submit');
        });

        $("#edit_course_form, #edit_competition_form").on("change", "input#course_outline_input", function(event) {
                var file = event.target.files[0];
                if (["application/pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"].includes(file.type)) {
                    $(this).closest("label").find(".display_value").text(file.name);
                } else {
                    $(this).val(null);
                    return false;
                }
        });

        // below is code for my notebook page.

        $("#my-notebook-page-holder").on("click", "span.unenroll-course", function() {
                var url = $(this).data("course-unenroll-url");
                var unenroll_course_modal = $(this).closest("#my-notebook-page-holder").find("#unenroll_course_modal");
                
                unenroll_course_modal.find("footer a#unenroll-course-button").attr("href", url);
                var course_title = $(this).closest(".card-title-holder").find("p.card-title").text();

                unenroll_course_modal.find("span.unenroll-course-title").text(course_title);
                unenroll_course_modal.addClass("is-active");
        });
});