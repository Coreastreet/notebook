$(document).on('turbolinks:load', function () {
        // below are functions for McQuiz edit.
        $("#new_mc_quiz_question_modal #number_of_options_field").on("click", ".tags .tag", function() {
                        var input = $(this).find("input");
                        
                        input[0].checked = true;

                        $(this).siblings().removeClass("selected");
                        $(this).addClass("selected");
                        
                        var controls = $("#mcq_options_fields .control");
                        var number_of_choices = controls.length;
                        var chosen_number = parseInt(input.val());

                        var input_control = controls.first();
                        input_control.find("input").val("");

                        var correct_option_control = $("#correct_option_index_fields .tags .tag").first();
                        var correct_option_clone;

                        var input_clone;
                        var new_number_bullet = number_of_choices;

                        if (number_of_choices < chosen_number) {
                        //clicked 4 or 5 when on 3 currently.
                        while ($("#mcq_options_fields .control").length < chosen_number) {
                                new_number_bullet += 1;

                                input_clone = input_control.clone();
                                input_clone.find(".number-bullet-points").text(`${new_number_bullet}.`);
                                input_clone.find("input[type='radio']").removeAttr("checked").attr("disabled", true);
                                input_clone.find("input[type='text']").removeClass("is-success");
                                $("#new_mc_quiz_question_form #mcq_options_fields").append(input_clone);

                                // add option for correct answer.
                                correct_option_clone = correct_option_control.clone();
                                correct_option_clone.removeClass("selected");
                                correct_option_clone.find("span").text(`Option ${new_number_bullet}`);
                                correct_option_clone.find("input").attr("id", `lesson_correct_answer_index_${new_number_bullet}`);
                                correct_option_clone.find("input").attr("value", new_number_bullet-1);
                                $("#correct_option_index_fields .tags").append(correct_option_clone);
                        }
                        } else if (number_of_choices > chosen_number) {
                        
                        while ($("#mcq_options_fields .control").length > chosen_number) {
                                $("#mcq_options_fields .control").last().remove();

                                // remove option for correct answer.
                                $("#correct_option_index_fields .tags .tag").last().remove();
                        }
                        } else {
                        // equal in which case do nothing.
                        }
                        // recheck the original correct answer option.
                        var correct_radio_option = $("#new_mc_quiz_question_modal #mcq_options_fields input[type='text'].is-success")
                        .closest(".control").find("input[type='radio']");

                        if (correct_radio_option.length == 1) {
                                correct_radio_option[0].checked = true;
                        } else {
                            // means no option selected
                                correct_option_control.addClass("selected");
                                input_control.find("input[type='radio']").removeAttr("disabled")[0].checked = true;
                                input_control.find("input[type='text']").addClass("is-success");
                        }
        });

        $("#new_mc_quiz_question_modal #correct_option_index_fields").on("click", ".tags .tag", function() {
                var input = $(this).find("input");
                input[0].checked = true;

                $(this).siblings().removeClass("selected");
                $(this).addClass("selected");

                var selected_option_input = $("#mcq_options_fields .control").eq(parseInt(input.val()));
                selected_option_input.siblings().find("input[type='text']").removeClass("is-success");
                selected_option_input.find("input[type='text']").addClass("is-success");

                var radio = selected_option_input.find("input[type='radio']");
                radio.removeAttr("disabled");
                radio[0].checked = true;

                selected_option_input.siblings().find("input[type='radio']").attr("disabled", "true");
        });

        $("#edit_lesson_form").on("click", "button#add_mcq_to_quiz", function() {
                var mc_question_form = $("#edit-lesson-body #new_mc_quiz_question_modal");
                mc_question_form.find("form.card")[0].reset();
                mc_question_form.addClass("is-active");
        });

        $("#edit_lesson_form").on("mouseover", ".questions .question", function() {
                $(this).find(".question-button").removeClass("is-hidden");
        });
        
        $("#edit_lesson_form").on("mouseout", ".questions .question", function() {
                $(this).find(".question-button").addClass("is-hidden");
        });

        // fire the submit lesson text form.
        $("#edit-lesson-body").on("click", "button#update_quiz_details", function() {
                var question_order_control = $("#edit-lesson-body #lesson_question_order_field .control");
                var template_question_order = $("#lesson_question_order_field #lesson_mc_quiz_question_order");
                var question_order;

                question_order_control.empty();

                $("#edit-lesson-body .questions .question:not(.is-hidden)").each(function() {
                        question_order = template_question_order.clone().removeAttr("id").removeAttr("disabled");
                        question_order.val($(this).data("mc-question-id"));                        
                        question_order_control.append(question_order);
                });
                // template_question_order.remove();

                var form = $("#edit-lesson-body #edit_lesson_form");
                Rails.fire(form[0], "submit");
        });

        // code for re-ordering question blocks
        // active and collapsible does not apply here.
        $("#edit-lesson-body").on("drop", ".questions .question", function(ev){ 
                ev.preventDefault();
                var questions = $(this).closest(".questions");
                var question_id = parseInt(ev.dataTransfer.getData("mc-question-id"));
                questions.find(`.question[data-mc-question-id='${question_id}']:not(.question-dashed)`).remove();
                questions.find(".question.question-dashed").removeClass("question-dashed");
                //$(this).closest(".questions").find(".question.question-dashed").removeClass("question-dashed");
        });
    

        $("#edit-lesson-body").on("dragstart", ".questions .question", function(ev) { 
                ev.dataTransfer.setData("mc-question-id", $(this).data("mc-question-id"));
                ev.dataTransfer.setData("index", $(this).index() - 1);
                //ev.dataTransfer.setData("move-type", "question");
        }); 

        $("#edit-lesson-body").on("dragover", ".questions", function(ev){ 
                ev.preventDefault();
        });    

        $("#edit-lesson-body").on("dragenter", ".questions .question:not(.question-dashed)", function(ev) { 
                ev.preventDefault();
                var questions = $(this).closest(".questions");
                //var module_title = ev.dataTransfer.getData("text");
                var question_id = parseInt(ev.dataTransfer.getData("mc-question-id"));
                var index = parseInt(ev.dataTransfer.getData("index"));
                var question_clone;

                if (!$(this).next().hasClass("question-dashed")) {
                        question_clone = questions.find(`.question[data-mc-question-id='${question_id}']`).clone();
                        
                        $(question_clone).addClass("question-dashed"); //.removeClass("is-hidden");

                        if (!($(this).data("mc-question-id") == question_id)) {
                                // remove the previous placeholder.
                                var dashed = questions.find(".question-dashed");
                                if (dashed.length != 0) {
                                        dashed.remove();
                                }
                                console.log(($(this).index() - 1), index);
                                if (($(this).index() - 1) > index) {
                                        $(question_clone).insertAfter($(this));
                                } else {
                                        // make sure only one placeholder exists at a time.
                                        if (dashed.length == 0) {
                                                $(question_clone).insertBefore($(this)); 
                                        }
                                }
                        }
                        //console.log(index);
                }
        });

        $("#edit-lesson-body").on("dragexit", ".questions .question", function(ev) { 
                ev.preventDefault();
                var next = $(this).next();
                //var prev = $(this).prev();

                if (next.hasClass("question-dashed")) {
                        next.remove();
                }
        });

        // enable tag clicking for editing question form.

        $("#edit-question-body .number_of_options_field").on("click", ".tags .tag", function() {
                var input = $(this).find("input");
                
                input[0].checked = true;

                $(this).siblings().removeClass("selected");
                $(this).addClass("selected");

                var form = $(this).closest("form");
                
                var controls = form.find(".mcq_options_fields .control");
                var number_of_choices = controls.length;
                var chosen_number = parseInt(input.val());

                var input_control = controls.first();

                var correct_option_control = form.find(".correct_option_index_fields .tags .tag").first();
                var correct_option_clone;

                var input_clone;
                var new_number_bullet = number_of_choices;

                console.log(number_of_choices, chosen_number);
                if (number_of_choices < chosen_number) {
                //clicked 4 or 5 when on 3 currently.
                        while (form.find(".mcq_options_fields .control").length < chosen_number) {
                                new_number_bullet += 1;

                                input_clone = input_control.clone();
                                input_clone.find(".number-bullet-points").text(`${new_number_bullet}.`);
                                input_clone.find("input[type='radio']").removeAttr("checked").attr("disabled", true);
                                input_clone.find("input[type='text']").removeClass("is-success").val("");
                                form.find(".mcq_options_fields").append(input_clone);

                                // add option for correct answer.
                                correct_option_clone = correct_option_control.clone();
                                correct_option_clone.removeClass("selected");
                                correct_option_clone.find("span").text(`Option ${new_number_bullet}`);
                                correct_option_clone.find("input").attr("id", `lesson_correct_answer_index_${new_number_bullet}`);
                                correct_option_clone.find("input").attr("value", new_number_bullet-1);
                                form.find(".correct_option_index_fields .tags").append(correct_option_clone);
                        }
                } else if (number_of_choices > chosen_number) {
                        // when 5 or 4 but user chooses to have 4 or 3 options only.
                        while (form.find(".mcq_options_fields .control").length > chosen_number) {
                                form.find(".mcq_options_fields .control").last().remove();

                                // remove option for correct answer.
                                form.find(".correct_option_index_fields .tags .tag").last().remove();
                        }
                } else {
                // equal in which case do nothing.
                }
                // recheck the original correct answer option.
                var correct_radio_option = form.find(".mcq_options_fields input[type='text'].is-success")
                                                .closest(".control").find("input[type='radio']");
                
                if (correct_radio_option.length == 1) {
                        correct_radio_option[0].checked = true;
                } else {
                    // means no option selected
                        correct_option_control.addClass("selected");
                        input_control.find("input[type='radio']").removeAttr("disabled")[0].checked = true;
                        input_control.find("input[type='text']").addClass("is-success");
                }
        });


        $("#edit-question-body .correct_option_index_fields").on("click", ".tags .tag", function() {
                var input = $(this).find("input");
                input[0].checked = true;

                $(this).siblings().removeClass("selected");
                $(this).addClass("selected");

                var form = $(this).closest("form");

                var selected_option_input = form.find(".mcq_options_fields .control").eq(parseInt(input.val()));
                selected_option_input.siblings().find("input[type='text']").removeClass("is-success");
                selected_option_input.find("input[type='text']").addClass("is-success");

                var radio = selected_option_input.find("input[type='radio']");
                radio.removeAttr("disabled");
                radio[0].checked = true;

                selected_option_input.siblings().find("input[type='radio']").attr("disabled", "true");
        });

        // fire the submit lesson text form.
        $("#edit-question-body").on("click", "button#update_question_details", function() {
                var form = $("#edit-question-body form#edit_quiz_question_form");
                Rails.fire(form[0], "submit");
        });
});