$(document).on('turbolinks:load', function () {
    // code for the username field
    // for signup username check username available on change
    //var error_notification;
    /*Rails.refreshCSRFTokens();

    var formData;
    var error_notification;
    var error_field;
    var former_fields;
    $("form#login-form, form#login-modal").on("click","input[type=submit]", function(e) {
          e.preventDefault();
          var form = $(this).closest("form");
          var username_input = $(form).find("input.session_username");
          var password_input = $(form).find("input.session_password");
          formData = JSON.stringify({ "session": {
                "username": username_input.val(),
                "password": password_input.val()
            }
          });
          var csrf_token = form.find('input[name=authenticity_token]').val();
          //console.log(form);
          //formData.set(`${username_input.attr("name")}`, `${username_input.val()}`);
          //formData.set(`${password_input.attr("name")}`, `${password_input.val()}`);
          postForm(`${$(form).attr("action")}`, formData, csrf_token).then( json => {
                if (json.status == "ok") {
                  // window.location.replace(`${json.redirect}`);
                } else {  // if not ok, then must be error
                    //console.log("login failed");

                }
          });
          Rails.ajax({
              type: "POST",
              url: "/login",
              data: JSON.stringify({ 'session': "blah" }),
              beforeSend(xhr, options) {
                xhr.setRequestHeader('Cache-Control', 'no-store');
                // Workaround: add options.data late to avoid Content-Type header to already being set in stone
                // https://github.com/rails/rails/blob/master/actionview/app/assets/javascripts/rails-ujs/utils/ajax.coffee#L53
                return true
              },
              success: function(repsonse){},
              error: function(repsonse){}
          })
    }); */

    $("form#signup-form").on("ajax:success", function(response) {
          var JSON_object_login = response.detail[0];

          if (JSON_object_login.status == "error") {

              error_notification = $("#login-form .notification");
              error_notification.removeClass("is-hidden").addClass("error-border");
              error_notification.text(JSON_object_login.message);

              $(`#login-form #${JSON_object_login.input_id}`).addClass("is-danger");
          }
    });

    $("form#report_not_me_form").on("ajax:success", function(response) {
          var json = response.detail[0];

          if (json.status == "ok") {
              $("#complete_report_confirmation").addClass("is-hidden");
              $(`#${json.on_success}`).removeClass("is-hidden");
              $("#not-me-title").text($(`#${json.on_success}`).data("title"));
          } else {
            console.log(json);
          }
    });

    $("form#account_recovery_form").on("ajax:success", function(response) {
          var json = response.detail[0];

          if (json.status == "ok") {
              $("#account_recovery_form").addClass("is-hidden");
              $("#account_recovery_subtitle").text("An email has been sent.").addClass("has-text-centered");

              if (json.method == "recover_username") {
                    $("#username_recovery_complete").removeClass("is-hidden");
              } else if (json.method == "reset_password") {
                    $("#password_recovery_complete").removeClass("is-hidden");
              } else { // "reset_password"

              }
          } else {
            console.log(json);
          }
    });

    $("a#dashboard-logout-button, a#logout-button").on("ajax:success", function(response) {
          var json = response.detail[0];

          if (json.status == "ok") {
              window.location.replace(`${json.redirect}`);
              console.log("redirected!");
          }
    });

    async function postForm(url, formData) {
      const response = await fetch(url, {
        method: "POST",
        headers: { 'Accept': 'application/json',
                   'Content-Type': 'application/json' },
        body: formData,
        credentials: 'include'
      });
      return response.json();
    }
});
