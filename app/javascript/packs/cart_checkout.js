$(document).on('turbolinks:load', function () {

    $("#checkout_container .payment-card ").on("click", "input[type='radio']", function() {
        if (!this.checked && !$(this).closest(".payment-card").hasClass("is-filled")) {
          $(this).parent().find("form#card_details_form").removeClass("is-hidden");
        }
    });

    /* 
    $("#checkout_container .billing-card ").on("click", ".change-link", function() {
        if (!$(this).closest(".billing-card").hasClass("is-filled")) {
          $(this).parent().find("form#billing_address_form").removeClass("is-hidden");
        }
    });
    */
});