$(document).on('turbolinks:load', function () {

    // note this is primarily for free downloads.
    // should send a request for a generated preview for paid notes to prevent stealing whole file.
    var preview_canvas_holder = document.querySelector("#preview_canvas_holder");
    var canvas_page = document.createElement("canvas");
    var canvas_page_divider = document.createElement("div");

    $("section#pdf_previewer nav").on("input", "input.page_num", function() {
          var page_index = parseInt($(this).val());
          var page_count = parseInt($(this).parent().find(".page_count").text());
          if ((typeof(page_index)=="number") && (page_index > 0) && (page_index <= page_count)) {
              document.querySelector(`#preview_canvas_holder canvas[data-page-index='${page_index}']`).scrollIntoView(true);
              console.log("scrolled!");
          }
    });

    $("section#pdf_previewer nav").on("click", "span.prev.pdf_controls_button", function() {
          var page_num = $(this).closest("nav").find("input.page_num");
          var page_index = parseInt(page_num.val());

          if (page_index > 1) {
              page_num.val(page_index-1);
              page_num.trigger("input");           
          }
    });

    $("section#pdf_previewer nav").on("click", "span.next.pdf_controls_button", function() {
          var page_num = $(this).closest("nav").find("input.page_num");
          var page_index = parseInt(page_num.val());
          var page_count = parseInt($(this).closest("nav").find(".page_count").text());

          if (page_index < page_count) {
              page_num.val(page_index+1);
              page_num.trigger("input");                       
          }
    });

    $("section#pdf_previewer nav").on("click", "span.zoomin.pdf_controls_button", function() {
           var basic_info = $("#notes_basic_info");
           var new_zoom_level = parseFloat($(this).parent().data("zoom-level")) + 0.25;

           var scroll_div = document.querySelector("#preview_canvas_scroll");
           var scroll_distance_percent = scroll_div.scrollTop/scroll_div.scrollHeight;
           console.log(scroll_distance_percent);

           if (new_zoom_level <= 3.0) {
               console.log(new_zoom_level);
               $(preview_canvas_holder).empty();
               insertCanvases(preview_canvas_holder, parseInt(basic_info.data("page-count")), 
                               parseInt(basic_info.data("preview-page-count")));
               preview_pdf($("#right_column_note_info button.pdf_preview_button").data("url"), preview_canvas_holder, 
                    parseFloat(basic_info.data("hw-ratio")), parseInt(basic_info.data("page-count")), 
                    parseInt(basic_info.data("preview-page-count")), new_zoom_level, scroll_distance_percent);
             
               $(this).parent().data("zoom-level", new_zoom_level);
           }
    });

    $("section#pdf_previewer nav").on("click", "span.zoomout.pdf_controls_button", function() {
           var basic_info = $("#notes_basic_info");
           var new_zoom_level = parseFloat($(this).parent().data("zoom-level")) - 0.25;

           var scroll_div = document.querySelector("#preview_canvas_scroll");
           var scroll_distance_percent = scroll_div.scrollTop/scroll_div.scrollHeight;
           console.log(scroll_distance_percent);
          
           if (new_zoom_level >= 0.5) {
               console.log(new_zoom_level);
               $(preview_canvas_holder).empty();
               insertCanvases(preview_canvas_holder, parseInt(basic_info.data("page-count")), 
                          parseInt(basic_info.data("preview-page-count")));
               preview_pdf($("#right_column_note_info button.pdf_preview_button").data("url"), preview_canvas_holder, 
                    parseFloat(basic_info.data("hw-ratio")), parseInt(basic_info.data("page-count")), 
                    parseInt(basic_info.data("preview-page-count")), new_zoom_level, scroll_distance_percent);

               $(this).parent().data("zoom-level", new_zoom_level);
           }
    });

    $("#right_column_note_info").on("click", "button.pdf_preview_button", function() {
                /* if ($(preview_canvas_holder).querySelectorAll("canvas").length == 0) {
                    var sample_page_count = parseInt($(this).data("sample-page-count")); 
                    var mid_pdf_sample_index = get_mid_index(sample_page_count);
                    for (var i = 0; i < sample_page_count; i++) {
                        if (i == mid_pdf_sample_index) {
                            preview_canvas_holder.appendChild(canvas_page_divider);                     
                        }
                        preview_canvas_holder.appendChil  d(canvas_page.cloneNode(true));
                    }
                    //$(preview_canvas_holder).hasClass("is-loaded");
                    // preview_pdf($(this).data("url"), preview_canvas, parseFloat($(this).data("hw-ratio")));                  
                } */

                if (!$(preview_canvas_holder).hasClass("is-loaded")) {
                    insertCanvases(preview_canvas_holder, parseInt($(this).data("page-count")), 
                      parseInt($(this).data("preview-page-count")));

                    preview_pdf($(this).data("url"), preview_canvas_holder, 
                      parseFloat($(this).data("hw-ratio")), parseInt($(this).data("page-count")), 
                      parseInt($(this).data("preview-page-count")), 1.5);
                }

                $("html").addClass("overflow-y-hidden");

                $("section#pdf_previewer nav .pdf_title_holder").text($("#notes_basic_info").data("pdf-title"));
                $("section#pdf_previewer nav .page_count").text($("#notes_basic_info").data("preview-page-count"));
                
                $(preview_canvas_holder).closest(".modal").addClass("is-active");
    });

    $("#preview_canvas_scroll").on("scroll", function() {
          var scrollHeight = this.scrollHeight;
          var scrollPosition = this.scrollTop;
          var num_preview_pages = parseInt($("#notes_basic_info").data("preview-page-count"));
          var current_page_index = Math.ceil(scrollPosition/((scrollHeight-112)/num_preview_pages)); 
          
          if (($("#pdf_previewer nav input.page_num").val() != current_page_index) && 
              (current_page_index > 0)) {
              $("#pdf_previewer nav input.page_num").val(current_page_index);
          }
    });

    function insertCanvases(canvas_holder, page_count, preview_page_count) {
          //    pageRendering = true;
                var preview_mid_index = get_mid_index(preview_page_count);
                var mid_index = get_mid_index(page_count);

                var divider = document.createElement("div");
                var divider_text = document.createElement("p");

                divider.classList.add("bg-color-platinum", "pdf_divider", "is-hidden", "mb-4", "is-justify-content-center");
                divider_text.classList.add("py-2");

                divider_text.textContent = `Pages ${preview_mid_index + 1} - ${mid_index} have been omitted.`;
                divider.appendChild(divider_text);

                var bottom_divider = divider.cloneNode(true);
                bottom_divider.firstElementChild.textContent = `Pages ${(preview_page_count - preview_mid_index) + (mid_index + 1)} - ${page_count} have been omitted.`;
                
                for (var i = 0; i < preview_page_count; i++) {

                    if (get_mid_index(preview_page_count) == i) {
                        canvas_holder.appendChild(divider);
                    }
                    canvas_holder.appendChild(document.createElement("canvas"));
                }
                canvas_holder.appendChild(bottom_divider);            
    }

    function preview_pdf(url, canvas_holder,
                         hw_ratio, page_count,
                         preview_page_count, scale, scroll_percent = 0) {
            // use this later for file name
            //var readerBlob = new FileReader();
                //$('#blah').attr('src', e.target.result);
                var loadingTask = pdfjsLib.getDocument(url);
                // implement functions for next/prev pages in pdf.
                var pdfDoc = null,
                    pageNum = 1,
                    pageRendering = false,
                    pageNumPending = null,
                    viewport,
                    canvas,
                    ctx;


                //function renderPages(pdf_page_count, pdfDoc) {
                
                const renderPages = async (pdf_page_count, pdfDoc, scale, canvas_holder) => {
                  let counter = 1;
                  var page;
                  var canvas_collection = canvas_holder.querySelectorAll("canvas");
                  canvas_holder.classList.add("is-invisible");
                  while (counter <= pdf_page_count) {
                    console.log(counter);
                    page = await pdfDoc.getPage(counter);
                    insertPage(page, counter, scale, canvas_collection[counter-1]);

                    //if (get_mid_index(pdf_page_count) == counter) {
                    //    canvas_holder.appendChild(divider);
                    //}
                    counter++;
                  }
                  canvas_holder.querySelectorAll(".pdf_divider").forEach(function(divider) {
                      $(divider).removeClass("is-hidden").addClass("is-flex");  
                  }); 
                  console.log('done');
                }
                    // Using promise to fetch the page
                    //for (var i = 1; i <= pdf_page_count; i++) {
                    //}

                    // Update page counters
                    //$('#page_num').text(num);
                //}

                function insertPage(page, index, scale, canvas) {
                      //pdfDoc.getPage(i).then(function(page) {
                          viewport = page.getViewport({scale: scale});

                          canvas.height = viewport.height;
                          canvas.width = parseFloat((viewport.height / hw_ratio).toFixed(2));
                          canvas.style.height = `${viewport.height}px`;
                          canvas.classList.add("mb-4");
                          canvas.setAttribute("data-page-index", index);

                          //canvas_holder.appendChild(canvas);

                          ctx = canvas.getContext('2d');

                          var renderContext = {
                            canvasContext: ctx,
                            viewport: viewport
                          };

                          var renderTask = page.render(renderContext);

                          // Wait for rendering to finish
                          renderTask.promise.then(function() {
                            pageRendering = false;

                            preview_canvas_holder.classList.add("is-loaded");
                            //$("html").addClass("overflow-y-hidden");
                            //$(`[data-canvas=${canvas.id}]`)[0].classList.add("is-active");
                            /*if (pageNumPending !== null) {
                              // New page rendering is pending
                              renderPage(pageNumPending, pdfDoc);
                              pageNumPending = null;
                            }*/
                          });
                        //});
                }

                /*
                function queueRenderPage(num, pdfDoc) {
                    if (pageRendering) {
                      console.log(`current page still rendering. Wait`)
                      pageNumPending = num;
                    } else {
                      console.log(`rendering page ${num}`)
                      renderPage(num, pdfDoc);
                    }
                } */

                loadingTask.promise.then(function(pdf) {

                  var scroll_div = document.querySelector("#preview_canvas_scroll");

                  renderPages(pdf.numPages, pdf, scale, canvas_holder).then(function() {
                      scroll_div.scrollTop = scroll_div.scrollHeight * scroll_percent;
                      canvas_holder.classList.remove("is-invisible");
                  });
                  
                  /*$('#prev').on('click', function() {
                      if (pageNum > 1) {
                        pageNum--;
                        queueRenderPage(pageNum, pdf);
                      } else {
                        return;
                      }
                  });

                  $('#next').on('click', function(){
                      if (pageNum < pdf.numPages) {
                        pageNum++;
                        queueRenderPage(pageNum, pdf);
                      } else {
                        return;
                      }
                  }); */

                  window.URL.revokeObjectURL(url);
                }, function (reason) {
                      // PDF loading error
                      console.error(reason);
                });
            //readerBlob.readAsArrayBuffer(input.files[0]); // convert to base64 string
    }

    function get_mid_index(num) {
        var middle_index;
        if (num % 2 == 0) {
            middle_index = (num/2);          
        } else {
            middle_index = Math.round(num/2);
        }
        return middle_index;
    }
});
