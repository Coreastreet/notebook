$(document).on('turbolinks:load', function () {

      var children;

        $("#competitions_table_section").on("mouseover", "tbody tr", function() {
            children = $(this).find(".note-column").children();
            children.each( function() {
                $(this).addClass("is-hidden");
            });
            children.last().removeClass("is-hidden");
        });

        $("#competitions_table_section").on("mouseout", "tbody tr", function() {
            children = $(this).find(".note-column").children();
            children.each( function() {
                $(this).removeClass("is-hidden");
            });
            children.last().addClass("is-hidden");
        });

      $("#uploads_table_section").on("mouseover", "tbody tr", function() {
          children = $(this).find(".note-column").children();
          children.each( function() {
              $(this).addClass("is-hidden");
          });
          children.last().removeClass("is-hidden");
      });

      $("#uploads_table_section").on("mouseout", "tbody tr", function() {
          children = $(this).find(".note-column").children();
          children.each( function() {
              $(this).removeClass("is-hidden");
          });
          children.last().addClass("is-hidden");
      });

       // highlight the note row when checkbox selected.
      $("#uploads_table tbody").on("click", "input[type='checkbox']", function() {
            $(this).closest("tr").toggleClass("background-selected");
      });

      var selected_counter;
      var checkbox_counter = $("#uploads_table tbody tr input[type='checkbox']");
      var note_result_holder = $("#note_result_holder");
      var number_results = note_result_holder.find(".number_results");
      var select_page_notes = $("#select_page_notes")[0];
      var selected_actions = $("#results_selected_actions");
      var select_all_notes_link = $("#select_all_notes_link");

      $("#uploads_table thead").on("click", "input[type='checkbox']#select_page_notes", function() {
            if (this.checked) {
                  checkbox_counter.each(function() {
                      this.checked = true;
                      $(this).closest("tr").addClass("background-selected");
                  });
                  note_result_holder.data("selected-counter", checkbox_counter.length)
                  number_results.text(selected_note_text(checkbox_counter.length));
                  selected_actions.removeClass("is-hidden");
                  if (checkbox_counter.length == parseInt(note_result_holder.data("total-uploads"))) {
                      select_all_notes_link.addClass("is-hidden");
                  } else {
                      select_all_notes_link.removeClass("is-hidden");
                  }
            } else {
                  $("#uploads_table tbody tr input[type='checkbox']:checked").each(function() {
                      this.checked = false;
                      $(this).closest("tr").removeClass("background-selected");
                  });
                  note_result_holder.data("selected-counter", 0);
                  number_results.text(uploaded_note_text(note_result_holder.data("total-uploads")));
                  selected_actions.addClass("is-hidden");
                  select_all_notes_link.addClass("is-hidden");
            }
            //$(this).closest("table#uploads_table").find("tbody tr input[type='checkbox']").trigger("click");
      });

      $("#uploads_table_section").on("click", "tbody tr input[type='checkbox']", function() {
          selected_counter = parseInt(note_result_holder.data("selected-counter"));
          if ($(this)[0].checked) {
              selected_counter++;
          } else {
              selected_counter--;
          }
          note_result_holder.data("selected-counter", selected_counter);

          if (selected_counter > 0) {
              number_results.text(selected_note_text(selected_counter)); 

              selected_actions.removeClass("is-hidden");

              if (selected_counter == parseInt(note_result_holder.data("total-uploads"))) {
                  select_all_notes_link.addClass("is-hidden");
              } else {
                  select_all_notes_link.removeClass("is-hidden");
              }
              //$("#select_all_notes")[0].indeterminate = true; 
          } else {
              number_results.text(uploaded_note_text(note_result_holder.data("total-uploads")));
              select_page_notes.checked = false;
              selected_actions.addClass("is-hidden");
              select_all_notes_link.addClass("is-hidden");
          }
      });

      function selected_note_text(note_count) {
          note_count = parseInt(note_count);

          var text;
          if (note_count != 1) {
              text = `${note_count} courses selected`;
          } else {
              text = `${note_count} course selected`;
          }  
          return text;
      }

      function uploaded_note_text(note_count) {
          note_count = parseInt(note_count);

          var text;
          if (note_count != 1) {
              text = `${note_count} courses created`;
          } else {
              text = `${note_count} course created`;
          } 
          return text;
      }

      // for dropdowns

      $("body").on("click", ".dropdown", function() {
          $(this).toggleClass("is-active");
      })

      $("#topNavbar").on("click", "button#dashboard_upload_modal_button", function() {
          $("#general_modal_body").closest(".modal").addClass("is-active");
      });

      $("#topNavbar").on("click", "button#upload_competition_button", function() {
            $("#new_competition_modal_body").closest(".modal").addClass("is-active");
      });

      // direct uploads
      $("#general_modal_body").on("direct-uploads:start", "form", function() {
          console.log("form with direct upload submitted");
      });

      $("#general_modal_body").on("direct-upload:start", "input#note_pdf", function() {
          console.log("direct upload has started");
          $("#upload_modal_progress").removeClass("is-invisible");
          $("#upload_updates").text("Uploading...");
      });

      $("#general_modal_body").on("direct-upload:progress", "input#note_pdf", function(e) {
          console.log("direct upload is progressing");
          $("#upload_modal_progress progress").val(e.detail.progress);
      });

      $("#general_modal_body").on("direct-upload:end", "input#note_pdf", function(e) {
          console.log("direct upload has ended");

          $("#upload_updates").text("Finalising details");
          $("#upload_modal_progress progress").addClass("is-hidden");
          $("#upload_modal_progress .css-loader").removeClass("is-hidden");

          //var student_username = $("#topNavbarDropdown .username_holder").text();

          var new_note_form = $(this).closest("form");
          //var formData = new FormData(new_note_form[0]);
          //formData.delete("note[pdf]");
          // disable the pdf input since pdf is already uploaded
          new_note_form.find("input[name='note[pdf]']").attr("disabled", true);

          ["name", "lastModified", "size"].forEach( function(value, index) {
               new_note_form.find(`#note_file_${value}`).val(e.detail.file[value]);
          })

          Rails.fire(new_note_form[0], "submit");

          new_note_form.find("input[name='note[pdf]']").attr("disabled", false)
      });

      $("#general_modal_body").on("direct-upload:error", "input#note_pdf", function(e) {
          console.log("An error has occurred.");
          console.log(e.detail);
      });

      $("#general_modal_body").on("direct-uploads:end", "form", function(e) {
          console.log("form with direct upload ended");
      });

      async function postForm(url, formData) {
        const response = await fetch(url, {
          method: "POST",
          headers: { 'Accept': 'application/js' },
          body: formData,
          credentials: 'include'
        });
        return response.json();
      }

});
