// This js file is for all code relating to dray and drop functions.
$(document).on('turbolinks:load', function () {

    // The following code is for the design course page.

    // drag and drop requires three main parts
        // -- dragstart event
            // specify what happens when the element is dragged.
            // Important to setdata() to allow for data transfer.
        // -- dragover event
            // -- we must prevent the default to allow for elements to be dropped on other elements.
        // -- drop event

        // code for allowing drop/reordering of modules in the module list.

    // 1. code for enabling the rearrangement of the order of existing lessons within a module.
        $("#design_course_page").on("dragstart", ".modules .module .lesson", function(ev) { 
              
        });

        // 1.5. code for enabling the addition of new lessons to a module.
        $("#design_course_page").on("dragstart", ".lessons .lesson", function(ev) { 
            ev.dataTransfer.setData("lesson-type", $(ev.target).data("lesson-type"));
            ev.dataTransfer.setData("move-type", "new-lesson");
        });

        // allow dragging over the dropdown contents of a module slider.
        $("#design_course_page").on("dragover", ".modules .content", function(ev){ 
            ev.preventDefault();
        });

         // dragover is split into two parts: drag-enter and drag-exit. Need to specify the separate
         // events when different actions required.
         // class active is used to indicate the module is expanded and this can accept new lessons.

         // insert a temp placeholder lesson as the first lesson in an existing module.
        $("#design_course_page").on("dragenter", ".modules .collapsible.active", function(ev){ 
                ev.preventDefault();
                var move_type = ev.dataTransfer.getData("move-type");
                var question;
                var placeholder_lesson;
                var content_block = $(this).closest(".module").find(".content");
                var question_not_first = true;
                // check if the placeholder is already in place and ensure only the lesson block triggers it.
                if (!$(this).next().hasClass("lesson-dashed") && 
                    (move_type != "module")) {

                    if (move_type == "new-lesson") {
                        console.log("new lesson");
                        var questionType = ev.dataTransfer.getData("lesson-type");
                        question = $(`#design_course_page .lessons .lesson[data-lesson-type='${questionType}']`);
                    } else {
                        console.log("existing-lesson");
                        //  must be an existing lesson being rearranged.
                        var questionId = ev.dataTransfer.getData("lesson-id");
                        question = $(`#design_course_page .module .lesson[data-lesson-id='${questionId}']`);
                        if (question.index() == 0) {
                            question_index = false;
                        }
                    }
                    
                    if (question_not_first) { // aka not first existing lesson.
                        placeholder_lesson = question.clone();
                        // make the lesson dashed and insert the placeholder lesson.
                        $(placeholder_lesson).addClass("lesson-dashed");
                        content_block.prepend(placeholder_lesson); 

                        // increase the height of the modules expanded content.
                        var new_height = content_block.data("max") + question.innerHeight();

                        content_block.css("height", new_height);
                        content_block.css("max-height", new_height);
                        content_block.data("max", new_height);
                    }
                }
        });

        $("#design_course_page").on("dragexit", ".modules .collapsible.active", function(ev){ 
                ev.preventDefault();
                var content_block = $(this).closest(".module").find(".content");
                var top_placeholder_lesson = content_block.children().first();
                var new_height = content_block.data("max") - top_placeholder_lesson.innerHeight();
                // if a temporary lesson placeholder is already present, 
                // remove this when you move the cursor out of range.
                if (top_placeholder_lesson.hasClass("lesson-dashed")) {
                    // return the module's expandible height to normal.
                    content_block.css("height", new_height);
                    content_block.css("max-height", new_height);
                    content_block.data("max", new_height);

                    top_placeholder_lesson.remove();
                }
        });

        $("#design_course_page").on("drop", ".modules .collapsible.active", function(ev){ 
                ev.preventDefault();
                var module = $(ev.target).closest(".module");
                var content_block = module.find(".content");
                var lesson_id;
                var lessons;
                var placeholder_lesson;
                var original_lesson;
                var new_height;
                
                if (ev.dataTransfer.getData('move-type') == "new-lesson") {
                    placeholder_lesson = module.find(".lesson.lesson-dashed");
                    placeholder_lesson.removeClass("lesson-dashed");
                } else { // existing lesson                    
                    lesson_id = ev.dataTransfer.getData('lesson-id');
                    lessons = module.find(`.lesson[data-lesson-id='${lesson_id}'`);
                    placeholder_lesson = lessons.filter('.lesson-dashed');
                    original_lesson = lessons.filter('.lesson:not(.lesson-dashed)');;
                    
                    placeholder_lesson.removeClass("lesson-dashed");
                    original_lesson.remove();

                    new_height = content_block.data("max") - placeholder_lesson.innerHeight();
                    content_block.css("height", new_height);
                    content_block.css("max-height", new_height);
                    content_block.data("max", new_height);
                }
        });

        // 2. code below for moving lesson element under a modal section.
        $("#design_course_page").on("dragenter", ".module .lesson:not(.lesson-dashed)", function(ev){ 
                ev.preventDefault();

                var questionType;
                var questionId;
                var placeholder_lesson;
                var placeholder_lesson_clone;
                var lesson_block = $(ev.target).closest(".lesson");
                var content_block = $(this).closest(".module").find(".content");
                var new_height;
                //var lesson_block_index = lesson_block.index();
                console.log(ev.dataTransfer.getData("lesson-id"));

                if (ev.dataTransfer.getData('move-type') == "new-lesson") {
                    questionType = ev.dataTransfer.getData("lesson-type");
                    placeholder_lesson = $(`#design_course_page .lessons .lesson[data-lesson-type='${questionType}']`)
                    placeholder_lesson_clone = placeholder_lesson.clone();

                    // check if the placeholder is already in place and ensure only the lesson block triggers it.
                    if (!$(this).next().hasClass("lesson-dashed")) {                    
                        $(placeholder_lesson_clone).addClass("lesson-dashed");
                        $(placeholder_lesson_clone).insertAfter(lesson_block);                

                        // increase the height of the modules expanded content.
                        new_height = content_block.data("max") + placeholder_lesson.innerHeight();

                        content_block.css("height", new_height);
                        content_block.css("max-height", new_height);
                        content_block.data("max", new_height);
                    }
                } else if (ev.dataTransfer.getData('move-type') == "existing-lesson") { // must be existing lesson or, in an unlikely case, module
                    questionId = ev.dataTransfer.getData("lesson-id");
                    //console.log(questionId);
                    placeholder_lesson = $(`#design_course_page .module .lesson[data-lesson-id='${questionId}']`);
                    placeholder_lesson_clone = placeholder_lesson.clone();
                    // check if the placeholder is already in place and ensure only the lesson block triggers it.
                    if ($(this).next().hasClass("lesson-dashed") || 
                       (placeholder_lesson.index() == lesson_block.index()) || 
                       (placeholder_lesson.index() == lesson_block.index() + 1)) {
                        // do not insert placeholder lesson.               
                    } else {
                        // console.log(questionId);
                        // console.log(placeholder_lesson.index(), lesson_block.index());
                        $(placeholder_lesson_clone).addClass("lesson-dashed");
                        $(placeholder_lesson_clone).insertAfter(lesson_block); 
                        //placeholder_lesson.remove();
                        // increase the height of the modules expanded content.
                        new_height = content_block.data("max") + placeholder_lesson.innerHeight();

                        content_block.css("height", new_height);
                        content_block.css("max-height", new_height);
                        content_block.data("max", new_height);
                    }
                } else {
                }
        });

        $("#design_course_page").on("dragexit", ".modules .lesson:not(.lesson-dashed)", function(ev){ 
                ev.preventDefault();
                var content_block = $(this).closest(".module").find(".content");
                var next_lesson = $(ev.target).next();
                if (next_lesson.hasClass("lesson-dashed")) {

                    new_height = content_block.data("max") - next_lesson.innerHeight();

                    content_block.css("height", new_height);
                    content_block.css("max-height", new_height);
                    content_block.data("max", new_height);
                    next_lesson.remove();
                }
        });

        // allow the dropping of lessons dragged onto existing lessons in modules that have been expanded.
        // code should cover the two cases
            // -- one case where the lesson is new
            // -- one case where the lesson is already existing.
        $("#design_course_page").on("drop", ".module .content", function(ev){ 
                ev.preventDefault();
                var move_type = ev.dataTransfer.getData("move-type");
                var questionType;
                var questionId;
                var lesson;
                var original_lesson;
                var lesson_clone;
                var new_height;

                if (move_type == "new-lesson") {
                    questionType = ev.dataTransfer.getData("lesson-type");
                    lesson = $(`#design_course_page .lessons 
                                    .lesson[data-lesson-type='${questionType}']`);
                } else { // existing-lesson
                    questionId = ev.dataTransfer.getData("lesson-id");
                    lesson = $(`#design_course_page .module 
                                    .lesson[data-lesson-id='${questionId}']`);
                }

                lesson_clone = lesson.clone();
                //lesson_clone = lesson.clone();
                //console.log(ev.target);
                // applies in the case when dropping in a new or existing lesson into the drop area.
                if (($(ev.target).closest(".drop-area").length == 1) && !(lesson.next().hasClass("drop-area"))) {
                    var drop_area = $(ev.target).closest(".drop-area");
                    new_height = $(this).height() + lesson.outerHeight() + 1; 
                    // if a new lesson was added increase the height.
                    if (move_type == "new-lesson") {
                        //console.log($(this).data("max"));
                        $(this).css("height", new_height);
                        $(this).css("max-height", new_height);
                        $(this).data("max", new_height);

                        console.log(lesson_clone);
                    } else { // if an existing lesson was rearranged, delete the original lesson to 
                        // avoid duplicates.
                        //console.log("lesson drag area dropped!")
                        lesson.remove();
                    }
                    lesson_clone.insertBefore(drop_area); 
                } else { // applies in the case when lesson is dropped onto an existing lesson.
                    // when a new lesson is dropped onto existing lessons in the content.
                    if (move_type == "new-lesson") {
                        //console.log($(this).data("max"));
                        $(this).find(".lesson-dashed").removeClass("lesson-dashed");
                    } else { // when an existing lesson is dropped onto an existing lesson aka.Rearrangement.
                        //console.log("missing");
                        if ($(this).find(".lesson-dashed").length == 1) { // if a dashed placeholder is present, then insert
                            lesson_clone.insertBefore(drop_area);
                            original_lesson = lesson.filter(".lesson:not(.lesson-dashed)");
                            lesson.filter(".lesson.lesson-dashed").removeClass("lesson-dashed");
                            new_height = $(this).height() - original_lesson.filter(".lesson:not(.lesson-dashed)").outerHeight() + 1; 
                            original_lesson.remove();

                            $(this).css("height", new_height);
                            $(this).css("max-height", new_height);
                            $(this).data("max", new_height); 
                        }  
                    }
                }
        });

        // allow dragging over the dropdown contents of a module slider.
        // allow dropping of new lessons onto the contents of an existing module.
        // code for dragover and drag event is already covered above.

    // ------- code below is for rearranging modules. ----------- //

    // 2. code for enabling the rearrangement of existing modules within a course.
        $("#design_course_page").on("dragstart", ".modules .module", function(ev) { 
            var lesson;
            if ($(ev.target).hasClass("module") && !$(ev.target).hasClass("module-dashed")) {
                ev.dataTransfer.setData("module-id", $(this).data("module-id"));
                ev.dataTransfer.setData("index", $(this).index() - 2);
                ev.dataTransfer.setData("move-type", "module");
            } else if ($(ev.target).hasClass("lesson")) {
                lesson = $(ev.target);
                ev.dataTransfer.setData("lesson-id", parseInt(lesson.data("lesson-id")));
                ev.dataTransfer.setData("index", lesson.index() - 2);
                ev.dataTransfer.setData("move-type", "existing-lesson");
                ev.dataTransfer.setData("lesson-type", lesson.data("lesson-type"));   
            } else {

            }
            console.log($(ev.target));
        });
    
        // allow dragging over both unexpanded/inactive module sliders 
        // and expanded/active module sliders together with their contents.
        $("#design_course_page").on("dragover", ".modules .collapsible", function(ev){ 
            ev.preventDefault();
        });

        // use module instead of collapsible and merge
        // allow the rearrangement of modules dragged onto modules that have not been expanded.
        $("#design_course_page").on("drop", ".modules .module .collapsible", function(ev) { 
            ev.preventDefault();
            var modules = $(this).closest(".modules");
            var module_id = parseInt(ev.dataTransfer.getData("module-id"));
            var moving_modules = modules.find(`.module[data-module-id='${module_id}']`);
            // var affected_module_id = parseInt($(this).data("module-id"));
            // different module drop or
            // hovering over the module's dashed placeholder itself when inserting above.
            var placeholder_module = moving_modules.filter(".module-dashed");
            var original_module = moving_modules.filter(".module:not(.module-dashed)");
            if (placeholder_module.length == 1) {
                placeholder_module.removeClass("module-dashed");
                original_module.remove();
            }
        });

        // 2.5 allow the reordering of modules
        $("#design_course_page").on("dragenter", ".modules .module .collapsible", function(ev) { 
                ev.preventDefault();
                //console.log(ev.dataTransfer);
                if (ev.dataTransfer.getData("move-type") == "module") {
                    var modules = $(this).closest(".modules");
                    var module = $(this).parent();
                    //var module_title = ev.dataTransfer.getData("text");
                    var module_id = parseInt(ev.dataTransfer.getData("module-id"));
                    //var index = ev.dataTransfer.getData("index");
                    //console.log(module.next());
                    if (!module.next().hasClass("module-dashed")) {
                            var module_clone = this.closest(".modules").querySelector(`.module[data-module-id='${module_id}']`).cloneNode(true);
                            
                            $(module_clone).addClass("module-dashed") //.removeClass("is-hidden");

                            // if an existing module is dragged onto itself.
                            if ((module_id == module.data("module-id")) || 
                                (module.next().data("module-id") == module_id)) {
                                console.log("onto self");
                                if (module.hasClass("module-dashed")) {
                                    module.remove();
                                    //console.log("remove dashed module");
                                } else {
                                    modules.find(".module-dashed").remove();
                                    // don't show any placeholder at all
                                }
                            } else {
                                //console.log(module.next().data("module-id"), module_id);
                                var dashed = modules.find(".module-dashed");
                                if (dashed.length != 0) {
                                        dashed.remove();
                                }
                                //if (($(this).index() - 2) > index) {
                                $(module_clone).insertAfter(module);
                            } 
                    }
                }
        });

        $("#design_course_page").on("dragexit", ".modules .module", function(ev) { 
            ev.preventDefault();
            var next = $(this).next();
            //var prev = $(this).prev();

            if (next.hasClass("module-dashed")) {
                next.remove();
            }
        });

        // implement dropping onto of the top and bottom drag areas.

        $("#design_course_page").on("dragover", ".modules .drop-area", function(ev){ 
            ev.preventDefault();
            $(this).parent().find(".module-dashed").remove();
        });

        $("#design_course_page").on("drop", ".modules > .drop-area", function(ev){ 
            var module_id = ev.dataTransfer.getData("module-id");
            var original_module = $(this).parent().find(`.module[data-module-id='${module_id}']`);
            var clone_module = original_module.clone();

            //console.log($(this).prev()[0], original_module[0]);
            if ($(this).attr('id') == "module-bottom-drop-area") {
                if ($(this).prev()[0].isSameNode(original_module[0])) {
                    
                } else {
                    clone_module.insertBefore($(this));
                    original_module.remove();
                }
            } else {
                if ($(this).next()[0].isSameNode(original_module[0])) {
                    
                } else {
                    clone_module.insertBefore($(this).parent().find(".module:not(.is-hidden)").first());
                    original_module.remove();
                }
            }
        });

});

