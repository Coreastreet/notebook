$(document).on('turbolinks:load', function () {

    // display modal when delete button on module is clicked.
    $("#edit_lesson_form").on("click", ".questions span.trigger_delete_question_modal", function() { 
            var question = $(this).closest(".question");
            var question_id = question.data("mc-question-id");
            var delete_question_modal = $("#edit-lesson-body #delete_question_modal");
            
            delete_question_modal.find(".question-number").text(`Delete Question ${question.index()}`);
            delete_question_modal.addClass("is-active");

            var delete_question_form = delete_question_modal.find("form");

            delete_question_form.attr("action", `/mc_quiz_questions/${question_id}`);
            delete_question_form.data("mc-question-id", question_id);
    });

    // modal popup for creating another course.
    $("#upload_success_modal").on("click", "button#upload_again_modal_button", function() {
            var success_popup = $(this).closest(".modal.is-active");
            var upload_modal = $(".upload_modal.general");

            upload_modal.find("form.card")[0].reset();
            upload_modal.addClass("is-active");

            success_popup.removeClass("is-active");
    }); 

    $("figure.image").on("load", "img#pdf_thumbnail_success", function(){
            $(this).parent().find("#image_loading_placeholder").remove();
            alert("image_loaded");
    })
});