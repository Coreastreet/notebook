$(document).on('turbolinks:load', function () {
    $("#right_column_learning_continue").on("click", 
        "#module-progress-holder a.continue-button", function(e) {
        var progress_bar = $("#topLearningNavbar").find("#module-progress progress");
            // i.e html link and 
        if (!$(this).data("remote") && (parseInt(progress_bar[0].style.width) != 100)) {
            e.preventDefault();
            // increment progress bar.
            var course_index_list = $("#course-index-list");
            var new_module_progress;
            var current_module_list_lessons = course_index_list.find("ul.module-list.current li > a");
            var current_module_completed_lessons = current_module_list_lessons.filter("[data-completed='true']");
            
            new_module_progress = ((current_module_completed_lessons.length + 1) * 100)/
                                    (current_module_list_lessons.length);
            new_module_progress = parseFloat(new_module_progress.toFixed(3));
            progress_bar.css("width", `${new_module_progress}%`);
            course_index_list.find("li.current > a").data("completed", true);
            // redirect to the other url.
            // Simulate a mouse click:
            window.location.href = $(this).attr("href");
            return true;
        };
        /*
        var current_module = $("#course-index-list").find("ul.module-list.current > ul.lesson-list"); 
        var current_lesson = current_module.find("li.current > a"); 

        if (!current_lesson.data("completed")) {
            // increment the progress bar.
            var new_module_progress = 
                ((current_module.find("a[data-completed='true']").length + 1) * 100) /
                current_module.children().length;
            new_module_progress = parseFloat(new_module_progress.toFixed(3));
            var progress_bar = $("#topLearningNavbar").find("#module-progress progress");

            if (new_module_progress != progress_bar.val()) {
                progress_bar.css("width", `${new_module_progress}%`);
            } 
            // else do nothing
        }
        return true; */
            // leave progress bar alone.
    }); 
   
});