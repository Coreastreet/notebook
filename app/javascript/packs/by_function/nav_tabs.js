$(document).on('turbolinks:load', function () {

    $("nav#intro-summary-tabs").on("click", "li", function() {
        var lesson_type = $(this).data("type");
        $(this).siblings().removeClass("is-active");
        $(this).addClass("is-active");
        
        if (lesson_type == "summary") {
            $("#edit_intro_lesson_form").addClass("is-hidden");
            $("#edit_summary_lesson_form").removeClass("is-hidden");
        } else {
            $("#edit_intro_lesson_form").removeClass("is-hidden");
            $("#edit_summary_lesson_form").addClass("is-hidden");
        }
    });

    $("#edit-intro-summary-body").on("click", "button#update_intro_summary_details", function() {
        var active_tab = $("nav#intro-summary-tabs li.is-active");
        var form;
        if (active_tab.data("type") == "intro") {
            form = $("#edit_intro_lesson_form");
        } else {
            form = $("#edit_summary_lesson_form");
        }
        Rails.fire(form[0], "submit");
    });

    // homepage
    $("#subject_tag_bar .tabs").on("click", "li", function() {
            $(this).siblings().removeClass("is-active");
            $(this).addClass("is-active");
    });
});