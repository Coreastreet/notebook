$(document).on('turbolinks:load', function () {

  // on upload of file, display the pdf.
  $('#profile_photo_file').on("change", "input[type=file]", function() {
      readURL(this, "preview_profile_photo");
      $("#photo_modal").addClass("is-active");
      //$("#edit_profile_photo").parent().css("background-color", "initial");
  });

  function readURL(input, image_id) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $(`#${image_id}`).attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]);
      }
  }

  var newItem;
  $("form").on("click", ".replace_loading", function(){
      newItem = document.createElement('button');
      newItem.classList.add("button", "is-link", "is-loading", "my-2");
      newItem.textContent = "Save";
      $(this).hide();
      $(newItem).insertAfter(this);
  });

  var tab_prefix;
  var li;
  $("#update_student_tabs").on("click", "li > a", function(){
      li = $(this).parent();
      li.siblings().removeClass("is-active");
      $("#update_profile_page section.update_tab_container").addClass("is-hidden");
      li.addClass("is-active");
      tab_prefix = li.data("tab");
      $(`#${tab_prefix}_section`).removeClass("is-hidden");
  });

});
