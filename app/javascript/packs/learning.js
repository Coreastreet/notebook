$(document).on('turbolinks:load', function () {
    
    // show the index of lessons in the course.
    var list_holder = $("#right_column_learning_continue #course-index-list-holder");
    var module_progress = $("#right_column_learning_continue #module-progress-holder");

    $("#right_column_learning_continue").on("click", "#contents-button", function() {
        module_progress.addClass("is-hidden");
        list_holder.removeClass("is-hidden");
        // list_background.removeClass("is-hidden");
        // hide the lesson progress content
    });

    list_holder.on("click", "i.blue-close", function() {
        list_holder.addClass("is-hidden");
        //list_background.addClass("is-hidden");
        module_progress.removeClass("is-hidden");
    });

    // submit quiz answer and return result.
    $("#right_column_learning_continue").on("submit", "form.is-form-mcq", function(e) {
        if ($(this).find("input[type='radio']:checked").length == 0) {

            var answer_box = $(this).find("#answer-box");
            answer_box.text("Please select an answer");
            answer_box.removeClass("is-hidden");
            
            return false;
        }
    });
    

    $("#right_column_learning_continue").on("click", "button#show-explanation", function() {
        $(this).addClass("is-hidden");
        $(this).parent().find("button#hide-explanation").removeClass("is-hidden");

        $("#middle_column_learning").find("#explanation-box").removeClass("is-hidden");
    });

    $("#right_column_learning_continue").on("click", "button#hide-explanation", function() {
        $(this).addClass("is-hidden");
        $(this).parent().find("button#show-explanation").removeClass("is-hidden");

        $("#middle_column_learning").find("#explanation-box").addClass("is-hidden");
    });
});