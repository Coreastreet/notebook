$(document).on('turbolinks:load', function () {

      var student_token = $("#topNavbar .username_holder").data("id-token");
      var tokenFormData = JSON.stringify({
          "student": {
            "id_token":`${student_token}`,
          }
      });

      $("#show_competition_container").on("click", ".svg_holder.like", function() {
          console.log("clicked");

          var dislike_button = $(this).parent().find(".svg_holder.dislike");

          var svg_like_counter = $(this).find(".svg_counter");
          var svg_dislike_counter = dislike_button.find(".svg_counter");

          if ($(this).hasClass("clicked")) {
              postForm(`${window.location.href}/remove_like`, tokenFormData).then( json => {
                    if (json.status == "ok") {
                        $(this).removeClass("clicked");
                        svg_like_counter.text(parseInt(svg_like_counter.text()) - 1);
                    } else {  // if not ok, then must be error
                        console.log("like failed");
                    }
              });
          } else {
              postForm(`${window.location.href}/like`,tokenFormData).then( json => {
                    if (json.status == "ok") {
                        $(this).addClass("clicked");
                        svg_like_counter.text(parseInt(svg_like_counter.text()) + 1);

                        dislike_button.removeClass("clicked");
                        if (parseInt(svg_dislike_counter.text()) > 0) {
                            svg_dislike_counter.text(parseInt(svg_dislike_counter.text()) - 1);                          
                        }
                    } else {  // if not ok, then must be error
                        console.log("remove like failed");
                    }
              });
          }

      });

      $("#show_competition_container").on("click", ".svg_holder.dislike", function() {

          var like_button = $(this).parent().find(".svg_holder.like");

          var svg_like_counter = like_button.find(".svg_counter");
          var svg_dislike_counter = $(this).find(".svg_counter");
          if ($(this).hasClass("clicked")) {
              postForm(`${window.location.href}/remove_dislike`, tokenFormData).then( json => {
                    if (json.status == "ok") {
                        $(this).removeClass("clicked");
                        svg_dislike_counter.text(parseInt(svg_dislike_counter.text()) - 1);
                    } else {  // if not ok, then must be error
                        console.log("dislike failed");
                    }
              });
          } else {
              postForm(`${window.location.href}/dislike`, tokenFormData).then( json => {
                    if (json.status == "ok") {
                        $(this).addClass("clicked");
                        svg_dislike_counter.text(parseInt(svg_dislike_counter.text()) + 1);

                        like_button.removeClass("clicked");
                        if (parseInt(svg_like_counter.text()) > 0) {
                            svg_like_counter.text(parseInt(svg_like_counter.text()) - 1);                          
                        }
                    } else {  // if not ok, then must be error
                        console.log("remove dislike failed");
                    }
              });
          }

      });

    /*
      $("#show_notes_container").on("click", "#save_note_button", function() {
          if ($(this).hasClass("saved")) {
              postForm(`${window.location.href}/remove_save`, tokenFormData).then( json => {
                    if (json.status == "ok") {
                        $(this).removeClass("saved");
                        $(this).find("span.button-text").text("Save");          
                    } else {  // if not ok, then must be error
                        console.log("remove save failed");
                    }
              });
          } else {
              postForm(`${window.location.href}/save`, tokenFormData).then( json => {
                    if (json.status == "ok") {
                        $(this).addClass("saved");          
                        $(this).find("span.button-text").text("Saved");          
                    } else {  // if not ok, then must be error
                        console.log("save failed");
                    }
              });
          }
      });
    */

      /*
      var note_slider = $("#show_notes_container .note_slider");
      var note_slider_page;
      var slider_notes_array;

      $("#other_notes_slider").on("submit", ".left_slider_button form", function() {
            note_slider_page = parseInt(note_slider.data("other-notes-page"));
            slider_notes_array = note_slider.data("other-notes-id");

            $("<input />").attr("type", "hidden")
                .attr("name", "note[slider_page_id]")
                .attr("value", note_slider_page - 2)
                .appendTo(this);

            $("<input />").attr("type", "hidden")
                .attr("name", "note[slider_notes_array]")
                .attr("value", slider_notes_array)
                .appendTo(this);
            return true;
      }); */

      $("#note_comments_section").on("focus", "#add_note_comment_box", function() {
          $("#note_comments_section nav .buttons").removeClass("is-hidden");
      });

      var comment_text;
      var br_count;
      $("#note_comments_section").on("keydown", "#add_note_comment_box", function(e) {
          if (e.key == "Enter") {
              $(this).css("height", $(this).height() + 25);
              $(this).data("newline_count", $(this).data("newline_count") + 1);
          } else {
              comment_text = `${$(this).html()}`;
              br_count = comment_text.match(/<br>/g)?.length;
              if (br_count < $(this).data("newline_count")) {
                  $(this).css("height", $(this).height() - 23);
                  $(this).data("newline_count", $(this).data("newline_count") - 1);
              }
          }
      });
      $("#note_comments_section").on("input", "#add_note_comment_box", function(e) {
          $("#note_comments_section #hidden_note_comment").val($(this).text());
      });

      $("#note_comments_section").on("click", "#cancel_note_comment", function() {
          $("#note_comments_section nav .buttons").addClass("is-hidden");
          $("#note_comments_section #add_note_comment_box").empty().trigger("blur");
      });

      async function postForm(url, formData) {
        const response = await fetch(url, {
          method: "POST",
          headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
          body: formData,
          credentials: 'include'
        });
        return response.json();
      }

});
