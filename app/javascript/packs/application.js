// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

import Rails from "@rails/ujs"
import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import "channels"
import "bulma"
import "@fortawesome/fontawesome-free/css/all"
//import { fromPath } from "pdf2pic";
import $ from "cash-dom";
import {} from 'jquery-ujs';
import { DirectUpload } from "@rails/activestorage";
//= require jquery
//= require jquery-ujs

// for ajax pagination
//import 'packs/pagy.js.erb'
//global.Pagy = Pagy
//import pdfjsLib from "pdfjs-dist/webpack";

window.$ = $;
window.Rails = Rails;

window.docxConverter = require('docx-pdf');

require ('packs/modal');
require ('packs/dropdown');
require ('packs/dashboard_aside');
require ('packs/dashboard_form');
require ('packs/dashboard_course');
require ('packs/form_submit');
require ('packs/uploads');
require ('packs/profile');
require ('packs/validations');
require ('packs/callbacks');
require ('packs/account');
require ('packs/account_recovery');
require ('packs/toasts');
require ('packs/index_panel');
require ('packs/pdf_load');
require ('packs/show_note');
require ('packs/preview_note');
require ('packs/note_slider');
require ('packs/cart_checkout');
require ('packs/design_course');
require ('packs/edit_lesson');
require ('packs/quiz_questions');
require ('packs/learning'); 
require ('packs/by_function/dashboard_popups'); 
require ('packs/by_function/drag_and_drop');
require ('packs/by_function/nav_tabs'); 
require ('packs/by_function/sliding'); 
require ('packs/by_function/video_upload'); 

require ('packs/competitions/show'); 
require ('packs/competitions/rate_submissions'); 
require ('packs/video_submissions/new'); 
require ('packs/video_submissions/create'); 
require ('packs/video_submissions/show'); 
require ('packs/common_elements'); 

require ('packs/my_notebook/competitions'); 
//require ('packs/sqPaymentForm');


Rails.start()
Turbolinks.start()
ActiveStorage.start()

require("trix");
require("@rails/actiontext");