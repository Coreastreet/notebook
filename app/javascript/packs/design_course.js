$(document).on('turbolinks:load', function () {

    // show content lessons and hide too.
    $("#design_course_page").on("click", ".module .collapsible div, .module .collapsible span.collapse-button", function(ev) {
            //console.log(ev.target);

            var collapse = $(this).closest(".collapsible");
            collapse[0].classList.toggle("active");
            var content = collapse[0].nextElementSibling;

            var lessons_length = $(content).find(".lesson[saved='true']").length;
            var standard_height = $("#design_course_page .lessons .lesson").outerHeight() + 1;
            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                if ( lessons_length > 0) {
                    $(content).data("max", (lessons_length * standard_height) + 80);
                    content.style.maxHeight = ((lessons_length * standard_height) + 80) + "px";
                    content.style.height = ((lessons_length * standard_height) + 80) + "px";
                } else {
                    content.style.maxHeight = content.dataset.max + "px";
                    content.style.height = content.dataset.max + "px";
                }
            }

            collapse.find(".cross")[0].classList.toggle("is-hidden");
            collapse.find(".minus")[0].classList.toggle("is-hidden");
    });

    $("#edit-dashboard-body").on("click", "button#update_competition_details", function() {
            var form = $("#edit-dashboard-body #edit_competition_form");
            Rails.fire(form[0], "submit");
    })

    $("#edit-dashboard-body").on("click", "button#update_course_details", function() {
            var form = $("#edit-dashboard-body #edit_course_form");
            Rails.fire(form[0], "submit");
    })

    // hide and show module-buttons for each module.
    // hide and show lesson-buttons for each lesson under a module.

    $("#design_course_page").on("mouseover", ".module .collapsible", function() {
            $(this).find(".module-button").removeClass("is-hidden");
    });

    $("#design_course_page").on("mouseout", ".module .collapsible", function() {
            $(this).find(".module-button").addClass("is-hidden");
    });

    // show only delete button for unsaved lessons.
     
    $("#design_course_page").on("mouseover", ".module .content .lesson", function() {
            $(this).find(".delete_lesson_modal").removeClass("is-hidden");
    });

    $("#design_course_page").on("mouseout", ".module .content .lesson", function() {
            $(this).find(".delete_lesson_modal").addClass("is-hidden");
    });

    // show the edit button for saved lessons.

    $("#design_course_page").on("mouseover", ".module .content .lesson[saved='true']", function() {
            $(this).find(".edit_lesson_modal").removeClass("is-hidden"); 
    });

    $("#design_course_page").on("mouseout", ".module .content .lesson", function() {
            $(this).find(".edit_lesson_modal").addClass("is-hidden");
    });
   
    // show modal for new module.

    $("#design-dashboard-body").on("click", "button#add_module_button", function() {
            $("#design_course_page #add_module_modal").addClass("is-active"); 
    });
    
    // code for making the module draggable.
    $("#design_course_page").on("mouseover", ".modules .module svg[name='reorder-dots']", function(ev) { 
            var module = $(this).closest(".module");
            module.attr("draggable", "true");
    });

    $("#design_course_page").on("mouseout", ".modules .module svg[name='reorder-dots']", function(ev) { 
            var module = $(this).closest(".module");
            module.attr("draggable", "false");
    });

    // code below for allowing the drag movement of modules for reordering.

    // just after clicking on the save module order, fill in the details for the new module order
    // and then remote submit the form.

    $("#design-dashboard-body").on("click", "#update_module_positions", function() { 
          var modules = $("#design-dashboard-body .modules .module:not(.is-hidden)");
          var module;

          var form = $("#save_module_order_form");
          var order_input = form.find("input[id='course_module_order']").first();
          order_input.val("");

          form.find("input[id='course_module_order']").remove();

          var course_module_order;

          modules.each(function(modulePart){
                course_module_order = order_input.clone();
                module = modules.eq(modulePart);
                course_module_order.val(module.data("module-id"));
                course_module_order.appendTo(form);
          });
          Rails.fire(form[0], 'submit');
    });

    $("#design_course_page").on("click", ".module .save_module_lessons", function() { 
            var module = $(this).closest(".module");

            var form = module.find("form.module_new_lessons_form");
            var lesson_type_input = form.find("input.lesson_type_input").first();
            var input_clone;

            var all_lessons = module.find(".content .lesson");

            var lesson_type;
            var lesson_id;

            var unsaved_lessons = all_lessons.filter("[saved='false']");
            // allow saving order in all cases.
            // only filter out unnecessary cases at the controller level.
            if (unsaved_lessons.length > 0) {
                all_lessons.each(function() {
                        input_clone = lesson_type_input.clone();
                        lesson_id = $(this).data("lesson-id");
                        // console.log(lesson_id);

                        if (Number.isInteger(lesson_id)) {
                        input_clone.val(lesson_id);
                        } else { // must be a newly inserted lesson
                        lesson_type = $(this).data("lesson-type"); 
                        input_clone.val(lesson_type);                          
                        }

                        form.append(input_clone);
                });

                lesson_type_input.attr("disabled", true);
                Rails.fire(form[0], 'submit');
                lesson_type_input.removeAttr("disabled");
                lesson_type_input.siblings().remove();
            } else {
                // show snackbar message
                var snackbar = document.createElement("div");
                $(snackbar).attr("id", "snackbar");
                $(snackbar).text('Add a lesson before saving');
                $("body").append($(snackbar));
                $(snackbar).addClass("show");
                setTimeout(function(){
                   $(snackbar).removeClass("show");
                }, 4000);
            }
      });

    // display modal when delete button on module is clicked.
    $("#design_course_page").on("click", ".module .trigger_delete_module_modal", function() { 
          var module = $(this).closest(".module");
          var module_id = module.data("module-id");
          var delete_module_modal = $(this).closest(".modules").find("#delete_module_modal");
          
          delete_module_modal.find(".module-number").text(`Delete Module ${module.index() + 1}`)
          delete_module_modal.addClass("is-active");

          var delete_module_form = delete_module_modal.find("form");

          delete_module_form.attr("action", `/module_parts/${module_id}`);
          delete_module_form.data("module-id", module_id);
    });

    // display modal when edit button on module is clicked.
    $("#design_course_page").on("click", ".module .trigger_edit_module_modal", function() { 
          var module = $(this).closest(".module");
          var module_id = module.data("module-id");
          var module_title = module.data("module-title");
          var module_subtitle = module.data("module-subtitle");
          var module_image_src = module.data("module-image-src");
          var module_image_filename = module.data("module-image-filename");

          var edit_module_modal = $(this).closest(".modules").find("#edit_module_modal");
          var edit_module_form = edit_module_modal.find("form");

          if (module_image_src == "") {
                // no picture is attached
                // hide delete bar and show the click file icons
                edit_module_form.find(".file-cta > span").removeClass("is-hidden");
                edit_module_form.find(".file .image-delete-bar").addClass("is-hidden");
                edit_module_form.find(".below-image-info").addClass("is-hidden");
                edit_module_form.find("figure.module-image").addClass("is-hidden")
          } else {
                // picture is attached.
                // load the picture if present first.
                edit_module_form.find("figure.module-image").removeClass("is-hidden")
                .find("img").attr("src", module_image_src);

                edit_module_form.find(".file-cta > span").addClass("is-hidden");
                edit_module_form.find(".file .image-delete-bar").removeClass("is-hidden");
                edit_module_form.find(".below-image-info").removeClass("is-hidden")
                                .find(".image-title").text(module_image_filename);
          }

          edit_module_form.attr("action", `/module_parts/${module_id}`);
          edit_module_form.data("module-id", module_id);

          edit_module_form.find("input#module_part_title").val(module_title);
          edit_module_form.find("textarea#module_part_subtitle").val(module_subtitle);

          edit_module_modal.addClass("is-active");
    });

    $("#design_course_page").on("click", "#delete_module_modal_body #module_deletion_button", function() {
          var form = $(this).closest("form");
          Rails.fire(form[0], "submit");
    });

    $("#design_course_page").on("click", ".modules .lesson[saved='true'] span.delete_lesson_modal", function() {
          // copy the title of the lesson
          var lesson = $(this).closest(".lesson");
          var lesson_id = lesson.data("lesson-id");
          var lesson_title = lesson.find(".lesson-title-content").text();
          var delete_lesson_modal = $("#design_course_page #delete_lesson_modal");
          delete_lesson_modal.find(".lesson-title-content").text(lesson_title.trim());

          delete_lesson_modal.data("module-id", lesson.closest(".module").data("module-id"));
          delete_lesson_modal.find("form").attr("action", `/lessons/${lesson_id}`);
          delete_lesson_modal.addClass("is-active");
    });

    $("#design_course_page").on("click", ".modules .lesson[saved='false'] span.delete_lesson_modal", function() {
          var lesson = $(this).closest(".lesson");
          // use the template lesson for getting height
          var lesson_height = $("#design_course_page .lessons .lesson").first().outerHeight();

          var content = $(this).closest(".module").find(".content")[0];

          $(content).data("max", (content.dataset.max - lesson_height));

          content.style.maxHeight = content.dataset.max + "px";
          content.style.height = content.dataset.max + "px";

          lesson.remove();  // no need to update position numbers for lessons. 
    });
    // just after clicking on the save lesson order, fill in the details for the new lesson order
    // and then remote submit the form.

    // enable image upload to new_module_part.
    $("#design_course_page form.card").on("change", "input[type='file'].image-upload", function(event) {
                var file = event.target.files[0];
                var blobURL = URL.createObjectURL(file);
                var form = $(this).closest("form.card");
                var figure = form.find("figure.module-image");

                var upload_module_image_cta = form.find(".file-cta > span");
                upload_module_image_cta.addClass("is-hidden");

                figure.find("img").attr("src", blobURL);
                figure.removeClass("is-hidden");

                form.find(".file .image-delete-bar").removeClass("is-hidden");
                
                var infoElement = form.find(".below-image-info");
                infoElement.removeClass("is-hidden");
                infoElement.find(".image-title").text(file.name);
     });

     // display video player inside the video input field when a video file is uploaded.
    $("#design_course_page .image-delete-bar").on("click", ".delete-image", function() {
                var control_file = $(this).closest(".file");
                var input = control_file.find("input[type='file'].image-upload");
                input[0].value = null;

                control_file.find(".image-delete-bar").addClass("is-hidden");
                var cta = control_file.find(".file-cta");

                cta.find("figure").addClass("is-hidden");
                cta.find("span").removeClass("is-hidden");

                var infoElement = $(this).closest(".control").find(".below-image-info");
                infoElement.addClass("is-hidden");
     });
});