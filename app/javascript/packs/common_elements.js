$(document).on('turbolinks:load', function () {

    $("#comments_list").on("click", ".comment-block .dropdown .dropdown-item.edit", function() {
        var comment_block = $(this).closest(".comment-block");
        console.log(comment_block);
        comment_block.find(".comment-full-text").addClass("is-hidden");
        comment_block.find("form").removeClass("is-hidden");
    });

    $("#comments_list").on("click", ".comment-reply-block .dropdown .dropdown-item.edit", function() {
        var comment_reply_block = $(this).closest(".comment-reply-block");
        comment_reply_block.find(".comment-full-text").addClass("is-hidden");
        comment_reply_block.find("form").removeClass("is-hidden");
    });

    $("#comments_list").on("click", ".comment-body button.cancel-edit", function() {
        var comment_body = $(this).closest(".comment-body");
        comment_body.find(".comment-full-text").removeClass("is-hidden");
        comment_body.find("form").addClass("is-hidden");
    });

    $("#comments_list").on("click", ".comment-block .reply-button[data-logged-in='true']", function() {
        var reply_form = $(this).closest(".comment-block-holder").find("form.new_reply_form")
        reply_form.find("textarea").empty();
        reply_form.removeClass("is-hidden");
    });

    $("#comments_list").on("click", ".comment-block-holder form.new_reply_form button.cancel-edit", function() {
        $(this).closest("form.new_reply_form").addClass("is-hidden");
    });



    
});