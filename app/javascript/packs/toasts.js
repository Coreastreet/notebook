$(document).ready(function () {
    console.log("checking for messages...");

    var message = sessionStorage.getItem("message");
    var message_type = sessionStorage.getItem("message_type");
    var notification_banner;
    if ((message != null) && (message_type != null)) {
        notification_banner = $("#notification_banner");
        $("#notification_banner #banner_text").text(`${message}`)
        notification_banner.addClass(`${message_type}`).removeClass("is-hidden");
        setTimeout(
          function(){
            notification_banner.addClass("is-hidden").removeClass(`${message_type}`);
            sessionStorage.removeItem("message");
            sessionStorage.removeItem("message_type");
          }, 5000
        )
    };

    $("#notification_banner").on("click", "#delete_banner", function() {
        $(this).closest("#notification_banner").addClass("is-hidden");
    });

});
