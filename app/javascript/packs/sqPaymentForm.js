$(document).on('turbolinks:load', function () {

    //TODO: paste code from step 2.1.1
   
     // Create and initialize a payment form object
     const paymentForm = new SqPaymentForm({
       // Initialize the payment form elements
       
       //TODO: Replace with your sandbox application ID
       applicationId: "sandbox-sq0idb-ouC_bLRxDFdhrlTOdznwPg",
       inputClass: 'sq-input',
       autoBuild: false,
       // Customize the CSS for SqPaymentForm iframe elements
       inputStyles: [{
           fontSize: '16px',
           lineHeight: '24px',
           padding: '16px',
           placeholderColor: '#a0a0a0',
           backgroundColor: 'transparent',
       }],
       // Initialize the credit card placeholders
       cardNumber: {
           elementId: 'sq-card-number',
           placeholder: 'Card Number'
       },
       cvv: {
           elementId: 'sq-cvv',
           placeholder: 'CVV'
       },
       expirationDate: {
           elementId: 'sq-expiration-date',
           placeholder: 'MM/YY'
       },
       postalCode: false,
       // SqPaymentForm callback functions
       callbacks: {
           /*
           * callback function: cardNonceResponseReceived
           * Triggered when: SqPaymentForm completes a card nonce request
           */
           cardNonceResponseReceived: function (errors, nonce, cardData) {
                var error_field;

                if (errors) {
                    // Log errors from nonce generation to the browser developer console.
                    for (var i = 0; i < errors.length; i++) {
                        if (errors[i].field == "cardNumber") {
                            error_field = "cardNumber";
                            break;
                        } else if (errors[i].field == "expirationDate") {
                            error_field = "expirationDate";
                            break;
                        } else if (errors[i].field == "cvv") {
                            error_field = "cvv";
                            continue;
                        } else {

                        }
                    } 

                    console.log(errors);

                    $("#form-container").children().removeClass("sq-input--error");
                    
                    if (error_field == "cardNumber") {
                        $("#card-error-notification").text("Card number is not valid.");
                        $("#sq-card-number").addClass("sq-input--error");
                    } else if (error_field == "expirationDate") {
                        $("#card-error-notification").text("Expiry date is not valid.");
                        $("#sq-expiration-date").addClass("sq-input--error");
                    } else if (error_field == "cvv") {
                        $("#card-error-notification").text("CVV must be three or four digits long.");
                        $("#sq-cvv").addClass("sq-input--error");
                    } else {

                    }

                    $("#card-error-notification").removeClass("is-hidden");

                    return;
                } else {  
                    console.log(cardData);
                }
                //TODO: Replace alert with code in step 2.1
                //alert(`The generated nonce is:\n${nonce}`);
                //$("#transaction_card_nonce").val(nonce);

                // hide card form;
                $("#card-error-notification").addClass("is-hidden");
                $("#form-container").children().removeClass("sq-input--error");

                var checkout_container = $("#checkout_container");
                var form = checkout_container.find("form#create-subscription-form");
                var cardholder_name = checkout_container.find("input#cardholder_name_input");
                var first_name = checkout_container.find("#first_name_input");
                var last_name = checkout_container.find("#first_name_input");

                form.find("input#subscription_first_name").val(first_name.val());
                form.find("input#subscription_last_name").val(last_name.val());

                form.find("input#subscription_cardholder_name").val(cardholder_name.val());
                form.find("input#subscription_card_nonce").val(nonce);

                //console.log(cardholder_name.val());
                Rails.fire(form[0], "submit");

                /* $("#form-container").addClass("is-hidden");
                $("#change_card_details").removeClass("is-hidden");
                $("#card_detail_holder img").addClass("is-hidden");
                $(`#card_detail_holder 
                    img[data-card="${cardData.card_brand.toLowerCase()}"`).removeClass("is-hidden");
                $("#card_detail_holder .last_digits").text(`x-${cardData.last_4}`);
                $("#card_detail_holder").removeClass("mb-4").removeClass("is-hidden"); */
        }
      }
    });
    //TODO: paste code from step 1.1.4
    paymentForm.build();
     
    //TODO: paste code from step 2.1.2
    //TODO: paste code from step 1.1.5
    // onGetCardNonce is triggered when the "Pay $1.00" button is clicked
    $("#checkout_container").on("click", "#sq-creditcard", function(event) {

        // Don't submit the form until SqPaymentForm returns with a nonce
        event.preventDefault();
        // Request a nonce from the SqPaymentForm object
        paymentForm.requestCardNonce();
    });

    /*
    $("#checkout_container").on("click", "#change_card_details", function() {
          $("#form-container").removeClass("is-hidden");
          $(this).addClass("is-hidden");
          $("#cancel_card_change").removeClass("is-hidden");
          $("#card_detail_holder").addClass("mb-4");
    });
 
    $("#checkout_container").on("click", "#cancel_card_change", function() {
          $("#form-container").addClass("is-hidden");
          $("#change_card_details").removeClass("is-hidden");
          $("#card_detail_holder").removeClass("mb-4");
    }); */

});