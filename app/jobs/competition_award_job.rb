class CompetitionAwardJob < ApplicationJob
  queue_as :default

  def perform(*competitions)
      competition = competitions[0]
      # all for equal positions e.g. two people share equal first prize.
      winning_submissions = competition.video_submissions.where.not(ranking: nil)

      winning_submissions.each do |winning_submission|
          Notification.create(message: "Congratulations!: Your submission has won #{winning_submission.ranking.ordinalize} place
                                        in the competition - #{competition.title}",
                              notifiable_type: "Competition", 
                              notifiable_id: competition.id,
                              win_alert: true,
                              student_id: winning_submission.student_id)
      end
    # Do something later
  end
end
