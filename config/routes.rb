Rails.application.routes.draw do
    get '/login', to: 'sessions#new'
    post '/login', to: 'sessions#create'
    delete '/logout', to: 'sessions#destroy'

    get '/signup', to: 'students#new'

    resource :faq, only: [] do
        get :winning
        # get :competitions
    end

    resource :my_notebook, only: [] do
        get :courses_in_progress
        get :courses_completed
        get :saved_for_later
        get :payment_details

        get :competitions_current
        get :competitions_past
        get :competitions_saved

        get "courses_completed/:course_id_token/show_results", 
            to: "my_notebooks#show_results", as: "show_course_results"
        # get "courses_in_progress/:course_id_token/show_results", to: "my_notebooks#show_results"

        get :basic_info
        get :password_and_email
        get :bio_and_qualifications
        get :messages
        
        namespace :basic_info do 
          patch "update-display-name"
          patch "update-password"
          patch "update-email"
          patch "disable-account"
          patch "delete-account"
          get "delete-account", to: "my_notebooks#show_delete_account"
          get "show-delete-modal"
        end
    end

    root to: 'competitions#index'

    resources :notifications, only: [] do
        member do 
            patch :read          
        end
    end

    resources :video_submissions, shallow: true, only: [] do
        resources :comments, shallow: true, only: [:create, :destroy, :update] do 
          member do 
              post :create_reply
              post :upvote
              delete :upvote, to: "comments#remove_upvote"
          end
        end
    end

    resources :competitions, only: [:index, :show] do
        collection do
            get :paid
            get :volunteer
        end
        member do 
            post :join
            delete :withdraw
        end
        resource :save_event, only: [:create, :destroy]
        resources :comments, shallow: true, only: [:create] do 
          #member do 
          #    post :create_reply
          #end
        end

        resources :video_submissions, shallow: true, only: [:show, :create, :edit, :update, :destroy] do
          collection do
              post :upload_video
          end

          member do 
              patch :upload_video
              post :upvote
              delete :upvote, to: "video_submissions#remove_upvote"
          end
        end
    end

    resources :courses, param: :course_token, only: [:index, :show] do
       member do

          post "like", to: "api/v1/like_event#create_like"
          post "remove_like", to: "api/v1/like_event#destroy"

          post "dislike", to: "api/v1/like_event#create_dislike"
          post "remove_dislike", to: "api/v1/like_event#destroy"

          post "load_slider_notes", to: "courses#load_slider_notes"
       end
    end

    resources :courses, param: :token, only: [] do
       resources :comments, shallow: true, only: [:create, :destroy]

       resource :save_event, only: [:create, :destroy]
    end

    get "/subjects/:subject/", to: "courses#index_by_subject", as: "index_by_subject"
    get "/courses/:course/", to: "courses#index_by_course", as: "index_by_course"

    get '/account_recovery', to: 'students#account_recovery'

    # get "/premium", to: "subscriptions#offer"
    # get "/payment_success", to: "subscriptions#payment_success"

    resource :subscription, only: [] do
        collection do 
            get :show
            post :create
            delete :destroy, path: "/unsubscribe"
            post :update_card
        end
    end

    get "/checkout", to: "subscriptions#checkout", as: "checkout"
    # post "/checkout_session", to: "shopping_cart#checkout_session", as: "checkout_session"

    # post '/checkout/pay', to: 'transaction#create', as: "transaction_create" 
    # get '/checkout/success', to: 'transaction#success', as: "transaction_success"
    # get '/checkout/show', to: 'transaction#show', as: "transaction_show"

    resources :students, param: :student_username, only: [:new, :create, :index] do
      member do
        get :about
        patch :update_photo
        post :billing_address, to: "billing_addresses#create" 
        patch :billing_address, to: "billing_addresses#update"

        post :bank_account, to: "bank_accounts#create"
        patch :bank_account, to: "bank_accounts#update"
      end
    end

    get "/students/not_me/:email_code", to: "students#not_me", as: "not_me"
    get "/students/new_reset_password/:email_code", to: "students#new_reset_password", as: "new_reset_password"

    namespace :api do
      namespace :v1 do
        get 'accounts/:student_id/verify_email', to: 'accounts#verify_email', as: 'verify_email'
        post 'accounts/:student_id/verify_email', to: 'accounts#verify_email', as: 'pin_verify_email'
        get 'accounts/:student_id/check_username_available', to: 'accounts#check_username_available'
        get 'accounts/:student_id/change_email', to: 'accounts#change_email'

        post 'accounts/recover_username', to: 'accounts#recover_username', as: 'recover_username'
        post 'accounts/reset_password', to: 'accounts#reset_password', as: 'reset_password'
        patch 'accounts/create_reset_password', to: 'accounts#create_reset_password', as: 'create_reset_password' # since this is an update
        post 'accounts/not_me_confirmation', to: 'accounts#not_me_confirmation', as: 'not_me_confirmation'

        get 'accounts/reset_password_confirmation', to: 'accounts#reset_password_confirmation', as: 'reset_password_confirmation'
        get 'accounts/not_me', to: 'accounts#not_me', as: 'not_me'
      end
    end
  # go to session controller to switch to dashboard
  # get '/switch_to_dashboard', to: 'sessions#switch_to_dashboard', as: 'switch_to_dashboard'
  # create a subdomain for a student dashboard
  # replacement for generic show route

  constraints :subdomain => 'dashboard' do

    namespace(:dashboard, :path => '/') do

      delete '/logout', to: 'sessions#destroy', as: 'logout'

      get 'orders/new'
      get 'orders/create'
      get 'orders/index'
      post '/courses', to: "private_courses#create"
      patch "/lessons/intro_summary", to: "lessons#update_intro_summary", as: 'update_intro_summary'

      resources :students, controller: :student_admins, param: :student_username, only: [] do
        member do
          patch :update_email
          get :profile
        end
      end

      resources :courses, controller: :private_courses, param: :course_id_token, only: [] do
          member do
            patch :update_module_order
            patch :update_lesson_order # since many modules are affected
          end
      end

      resources :courses, param: :id_token, only: [] do
          resources :module_parts, param: :module_id, shallow: true, only: [:create, :update, :destroy] do
          end
      end

      resources :modules, controller: :module_parts, only: [] do 
            resources :lessons, param: :lesson_id, shallow: true, only: [:create, :edit, :update, :destroy] do
            end
      end

      resources :lessons, only: [] do
            resources :mc_quiz_questions, shallow: true, only: [:create, :edit, :update, :destroy] do
            end
      end

      resources :video_submissions, only: [], shallow: true do
        member do 
          patch :rate
        end
      end

      resources :students, controller: :student_admins, param: :username, only: [] do

            get :payment_history, to: "student_admins#show_payment_history"

            get :comments

            resources :competitions, controller: :private_competitions, except: [:show], shallow: true do
                collection do
                  post :upload_video
                end

                member do 
                  get :video_submissions
                  patch :announce_winners
                end
            end

            resources :courses, controller: :private_courses, param: :course_id_token, only: [], shallow: true do
              collection do
                get :courses, path: "/", as: "content"

                get "/new", to: "private_courses#courses"
                post :create, path: "/"
              end

              member do
                # all options in the tabs for courses.
                get :edit # first option
                get :analytics
                get :design
                get :intro_summary
                # get :comments second option, view comments.

                patch :update, path: "/"
                delete :destroy, path: "/" # last option
                get :delete
              end
            end

            #resource :my_drive, controller: :private_notes, only: [], shallow: true do
            #  collection do
            #    get "/", to: "private_notes#my_drive"
            #    post :load_slider_notes, as: "slider"
            #  end
            #end
      end

    # get :notes, on: :member
      #resources :student_admins, only: [:show, :edit, :update, :destroy]
    end
  end

  constraints :subdomain => 'learning' do
      namespace(:learning, :path => '/') do
          resources :courses, param: :id_token, only: [] do
              patch :update_completed

              get "/lessons/:learnable_type/:learnable_id", to: "lessons#show",
                  as: "lesson", constraints: { 
                  learnable_type: /(lesson-text|lesson-video|mc-quiz|assignment-video)/ 
              }

              get "/lessons/:learnable_type/:learnable_id/:question_id", to: "lessons#show_quiz_question",
                  as: "quiz_question", constraints: { 
                  learnable_type: /(mc-quiz)/ 
              }
              #get "/lesson-texts/:learnable_id", to: "lessons#show_reading", as: "reading"
              #get "/mc-questions/:learnable_id", to: "lessons#show_mc_question", as: "question"
              #get "/lesson-videos/:learnable_id", to: "lessons#show_video", as: "video"
              #get "/mc-quiz/:learnable_id/start", to: "lessons#start_quiz", as: "mc_quiz_start"

              patch "/mc-questions/:learnable_id", to: "lessons#update_mc_quiz_answer", as: "mc_quiz_answer"
          end
      end
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
