Rails.application.config.session_store :redis_session_store,
  key: "_notebank_session_#{Rails.env}",
  #key_prefix: 'notebank:session:',
  #serializer: :json
  #expires_after: 2.hours,
  httponly: true,
  tld_length: 2, # ngrok
  redis: {
      expire_after: 120.minutes,
      url: 'redis://localhost:6379/0',
}


# ttl: 120.minutes,           Redis expiration, defaults to 'expire_after'
