# initiailize the gateway instance.

ActiveMerchant::Billing::Base.mode = :test # change to :production for live
::GATEWAY = ActiveMerchant::Billing::StripePaymentIntentsGateway.new(
  login: ENV['STRIPE_SECRET_KEY']
)
