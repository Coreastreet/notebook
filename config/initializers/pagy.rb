
# Rails

# Rails: extras assets path required by the helpers that use javascript
# (pagy*_nav_js, pagy*_combo_nav_js, and pagy_items_selector_js)
# See https://ddnexus.github.io/pagy/extras#javascript
Rails.application.config.assets.paths << Pagy.root.join('javascripts')

require 'pagy/extras/bulma'
