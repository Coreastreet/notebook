ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __dir__)

require "bundler/setup" # Set up gems listed in the Gemfile.
require "bootsnap/setup" # Speed up boot time by caching expensive operations.

=begin
at_exit do
  if Rails.const_defined? "Server"
          insert_array = []

          t = Time.current.to_i
          past_24hr_viewEvents = Redis.current.zrange("past_24hr_viewEvents", 0, -1)
          past_24hr_viewEvents.each do |student_string|
              student_id = /student:(\d+)/.match(note_string)[1]

              Redis.current.zrange("student:#{student_id}", 0, -1, with_scores: true).each do |note_id_string, time_of_view|
                    view_event_record = Hash.new 

                    note_id = /note:(\d+)/.match(note_id_string)[1]

                    view_event_record["time_of_view"] = Time.at(time_of_view.to_i).utc.to_datetime
                    view_event_record["student_id"] = student_id
                    view_event_record["note_id"] = note_id
                    view_event_record["created_at"] = Time.now.utc.iso8601

                    insert_array.push(view_event_record)
              end
              Redis.current.del("student:#{student_id}")
          end

          past24hr_viewEvents = Redis.current.zrangebyscore("past_24hr_viewEvents", 0, t)
          if (!past24hr_viewEvents.empty?)
              Redis.current.zrem("past_24hr_viewEvents", past24hr_viewEvents)
          end

          if (!insert_array.empty?) 
              ViewEvent.insert_all(insert_array)
          end
  end
end

=end
