# require 'active_storage/engine'

require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Notebank

  USERNAME_REGEX = /^[a-zA-Z0-9_]+$/
  DOMAIN = "localtest.me" # "5f2dcfcdf94e.ngrok.io"  ngrok

  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    config.autoload_paths << Rails.root.join('lib')
    #config.middleware.insert_before 0, Rack::Cors do
    #  allow do
    #    origins '*'
    #    resource '/*',
    #      :headers => :any,
    #      :methods => [:get, :post, :options]
    #  end
    #end
    config.active_storage.content_types_allowed_inline += ["video/mp4"]

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
