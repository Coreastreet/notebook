# Preview all emails at http://localhost:3000/rails/mailers/student_mailer
class StudentMailerPreview < ActionMailer::Preview
      def welcome_email
          StudentMailer.with(student: Student.find_by(username: "JusticeYe")).welcome_email
      end

      def change_email
          StudentMailer.with(student: Student.find_by(username: "JusticeYe")).change_email
      end

      def recover_username
          StudentMailer.with(username: Student.last.username, email: Student.last.email).recover_username
      end

      def reset_password
          StudentMailer.with(username: Student.last.username, email: Student.last.email).reset_password
      end
end
