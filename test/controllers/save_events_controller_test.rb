require "test_helper"

class SaveEventsControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get save_events_create_url
    assert_response :success
  end

  test "should get destroy" do
    get save_events_destroy_url
    assert_response :success
  end
end
