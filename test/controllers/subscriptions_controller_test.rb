require "test_helper"

class SubscriptionsControllerTest < ActionDispatch::IntegrationTest
  test "should get offer" do
    get subscriptions_offer_url
    assert_response :success
  end

  test "should get show" do
    get subscriptions_show_url
    assert_response :success
  end

  test "should get create" do
    get subscriptions_create_url
    assert_response :success
  end

  test "should get destroy" do
    get subscriptions_destroy_url
    assert_response :success
  end

  test "should get update_card" do
    get subscriptions_update_card_url
    assert_response :success
  end
end
