require "test_helper"

class EnrolmentEventsControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get enrolment_events_create_url
    assert_response :success
  end

  test "should get destroy" do
    get enrolment_events_destroy_url
    assert_response :success
  end
end
