require "test_helper"

class Dashboard::PrivateCompetitionsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get dashboard_private_competitions_new_url
    assert_response :success
  end

  test "should get create" do
    get dashboard_private_competitions_create_url
    assert_response :success
  end

  test "should get edit" do
    get dashboard_private_competitions_edit_url
    assert_response :success
  end

  test "should get update" do
    get dashboard_private_competitions_update_url
    assert_response :success
  end

  test "should get destroy" do
    get dashboard_private_competitions_destroy_url
    assert_response :success
  end

  test "should get index" do
    get dashboard_private_competitions_index_url
    assert_response :success
  end
end
