require "test_helper"

class Dashboard::StudentAdminsControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get dashboard_student_admins_show_url
    assert_response :success
  end

  test "should get edit" do
    get dashboard_student_admins_edit_url
    assert_response :success
  end

  test "should get update" do
    get dashboard_student_admins_update_url
    assert_response :success
  end

  test "should get destroy" do
    get dashboard_student_admins_destroy_url
    assert_response :success
  end
end
