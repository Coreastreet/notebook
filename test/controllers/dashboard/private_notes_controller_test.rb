require "test_helper"

class Dashboard::PrivateNotesControllerTest < ActionDispatch::IntegrationTest
  test "should get index_purchases" do
    get dashboard_private_notes_index_purchases_url
    assert_response :success
  end

  test "should get index_uploads" do
    get dashboard_private_notes_index_uploads_url
    assert_response :success
  end

  test "should get show" do
    get dashboard_private_notes_show_url
    assert_response :success
  end
end
