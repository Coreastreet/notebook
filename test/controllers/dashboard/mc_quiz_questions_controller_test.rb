require "test_helper"

class Dashboard::McQuizQuestionsControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get dashboard_mc_quiz_questions_create_url
    assert_response :success
  end

  test "should get edit" do
    get dashboard_mc_quiz_questions_edit_url
    assert_response :success
  end

  test "should get update" do
    get dashboard_mc_quiz_questions_update_url
    assert_response :success
  end

  test "should get destroy" do
    get dashboard_mc_quiz_questions_destroy_url
    assert_response :success
  end
end
