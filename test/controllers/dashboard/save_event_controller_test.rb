require "test_helper"

class Dashboard::SaveEventControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get dashboard_save_event_create_url
    assert_response :success
  end

  test "should get destroy" do
    get dashboard_save_event_destroy_url
    assert_response :success
  end
end
