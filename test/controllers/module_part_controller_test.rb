require "test_helper"

class ModulePartControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get module_part_create_url
    assert_response :success
  end

  test "should get show" do
    get module_part_show_url
    assert_response :success
  end

  test "should get update" do
    get module_part_update_url
    assert_response :success
  end

  test "should get destroy" do
    get module_part_destroy_url
    assert_response :success
  end
end
