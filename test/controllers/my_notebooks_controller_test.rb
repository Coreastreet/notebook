require "test_helper"

class MyNotebooksControllerTest < ActionDispatch::IntegrationTest
  test "should get courses_in_progress" do
    get my_notebooks_courses_in_progress_url
    assert_response :success
  end

  test "should get courses_completed" do
    get my_notebooks_courses_completed_url
    assert_response :success
  end

  test "should get purchases" do
    get my_notebooks_purchases_url
    assert_response :success
  end

  test "should get account" do
    get my_notebooks_account_url
    assert_response :success
  end

  test "should get messages" do
    get my_notebooks_messages_url
    assert_response :success
  end
end
