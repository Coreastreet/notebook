require "test_helper"

class Api::V1::StudentsControllerTest < ActionDispatch::IntegrationTest
  test "should get verify_email" do
    get api_v1_students_verify_email_url
    assert_response :success
  end
end
