require "test_helper"

class Learning::LessonsControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get learning_lessons_show_url
    assert_response :success
  end
end
