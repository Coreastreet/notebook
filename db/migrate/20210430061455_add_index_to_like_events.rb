class AddIndexToLikeEvents < ActiveRecord::Migration[6.1]
  def change
      add_index :like_events, [:student_id, :note_id], unique: true
  end
end
