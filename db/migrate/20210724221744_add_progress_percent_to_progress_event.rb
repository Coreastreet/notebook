class AddProgressPercentToProgressEvent < ActiveRecord::Migration[6.1]
  def change
    add_column :progress_events, :progress_percent, :decimal, precision: 5, scale: 2, default: 0
  end
end
