class RemoveReferenceCoursesFromSaveEvent < ActiveRecord::Migration[6.1]
  def change
      remove_reference :save_events, :course, index: true, foriegn_key: true 
  end
end
