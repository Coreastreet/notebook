class RenameTypeToTestType < ActiveRecord::Migration[6.1]
  def change
    rename_column :results, :type, :test_type
  end
end
