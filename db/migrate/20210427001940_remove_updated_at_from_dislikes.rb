class RemoveUpdatedAtFromDislikes < ActiveRecord::Migration[6.1]
  def change
    remove_column :dislikes, :updated_at, :datetime
  end
end
