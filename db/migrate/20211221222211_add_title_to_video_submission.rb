class AddTitleToVideoSubmission < ActiveRecord::Migration[6.1]
  def change
    add_column :video_submissions, :title, :string, null: false, default: "Untitled"
  end
end
