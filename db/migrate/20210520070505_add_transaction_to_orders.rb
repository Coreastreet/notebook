class AddTransactionToOrders < ActiveRecord::Migration[6.1]
  def change
    add_reference :orders, :transaction, null: false, foreign_key: true
  end
end
