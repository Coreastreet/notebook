class AddCourseEnumToNotes < ActiveRecord::Migration[6.1]
  def change
    add_column :notes, :course, :integer
  end
end
