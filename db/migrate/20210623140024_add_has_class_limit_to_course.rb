class AddHasClassLimitToCourse < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :has_class_limit?, :boolean, default: false
  end
end
