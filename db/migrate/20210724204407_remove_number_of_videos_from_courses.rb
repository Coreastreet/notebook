class RemoveNumberOfVideosFromCourses < ActiveRecord::Migration[6.1]
  def change
    remove_column :courses, :number_of_videos, :integer
  end
end
