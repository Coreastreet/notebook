class DropAvatars < ActiveRecord::Migration[6.1]
  def change
    drop_table :avatars
  end
end
