class AddPaperSizeRatioToNote < ActiveRecord::Migration[6.1]
  def change
    add_column :notes, :paper_size_ratio, :decimal, precision: 3, scale: 2, default: 1.3
  end
end
