class AddHasContentToLesson < ActiveRecord::Migration[6.1]
  def change
    add_column :lessons, :has_content, :boolean, default: false
  end
end
