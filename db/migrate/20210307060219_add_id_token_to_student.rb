class AddIdTokenToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :id_token, :string
    add_index :students, :id_token, unique: true
  end
end
