class AddPolymorphicIndexToNotifications < ActiveRecord::Migration[6.1]
  def change
    add_reference :notifications, :notifiable, polymorphic: true, null: true
  end
end
