class AddReasonForDeletionToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :reason_for_deletion, :text
  end
end
