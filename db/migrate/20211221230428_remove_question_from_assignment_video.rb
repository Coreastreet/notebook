class RemoveQuestionFromAssignmentVideo < ActiveRecord::Migration[6.1]
  def change
    remove_column :assignment_videos, :question, :text
  end
end
