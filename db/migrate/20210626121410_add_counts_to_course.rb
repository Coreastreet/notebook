class AddCountsToCourse < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :assignment_count, :integer
    add_column :courses, :quiz_count, :integer
    add_column :courses, :lesson_count, :integer
    add_column :courses, :practical_count, :integer
  end
end
