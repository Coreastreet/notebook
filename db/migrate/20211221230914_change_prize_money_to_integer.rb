class ChangePrizeMoneyToInteger < ActiveRecord::Migration[6.1]
  def change
    change_column :assignment_videos, :prize_money, :integer, default: 0
  end
end
