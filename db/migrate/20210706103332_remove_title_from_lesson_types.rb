class RemoveTitleFromLessonTypes < ActiveRecord::Migration[6.1]
  def change
      remove_column :lesson_videos, :title, :string
      remove_column :practicals, :title, :string
      remove_column :mc_quizzes, :title, :string
      remove_column :sa_quizzes, :title, :string
      remove_column :ma_quizzes, :title, :string
      remove_column :fa_quizzes, :title, :string
  end
end
