class AddActivatedToReportAccountDigest < ActiveRecord::Migration[6.1]
  def change
    add_column :report_account_digests, :activated, :boolean, default: false
  end
end
