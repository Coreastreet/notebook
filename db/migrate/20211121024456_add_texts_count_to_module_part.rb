class AddTextsCountToModulePart < ActiveRecord::Migration[6.1]
  def change
    add_column :module_parts, :texts_count, :integer, default: 0
  end
end
