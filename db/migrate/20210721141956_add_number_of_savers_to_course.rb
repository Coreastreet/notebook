class AddNumberOfSaversToCourse < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :number_of_savers, :integer, default: 0
  end
end
