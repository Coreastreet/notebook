class AddSubmissionRankingsToCompetition < ActiveRecord::Migration[6.1]
  def change
    add_column :competitions, :submission_rankings, :integer, array: true, default: []
  end
end
