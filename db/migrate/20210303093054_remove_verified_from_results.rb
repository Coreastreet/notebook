class RemoveVerifiedFromResults < ActiveRecord::Migration[6.1]
  def change
    remove_column :results, :verified, :boolean
  end
end
