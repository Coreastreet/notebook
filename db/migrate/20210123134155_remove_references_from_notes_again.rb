class RemoveReferencesFromNotesAgain < ActiveRecord::Migration[6.1]
  def change
    remove_reference :notes, :students, null: false, foreign_key: true
    add_reference :notes, :student, null: false, foreign_key: true
  end
end
