class ChangeIsCompleteToFalseProgress < ActiveRecord::Migration[6.1]
  def change
    change_column :progress_events, :is_complete, :boolean, :default => false
  end
end
