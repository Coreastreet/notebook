class AddRatingToVideoSubmission < ActiveRecord::Migration[6.1]
  def change
    add_column :video_submissions, :rating, :decimal, precision: 3, scale: 1
  end
end
