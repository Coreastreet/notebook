class RemoveTotalPriceFromShoppingCart < ActiveRecord::Migration[6.1]
  def change
    remove_column :shopping_carts, :total_price, :decimal
  end
end
