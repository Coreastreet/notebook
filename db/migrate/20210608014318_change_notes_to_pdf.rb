class ChangeNotesToPdf < ActiveRecord::Migration[6.1]
  def change
    rename_table :notes, :pdfs
  end
end
