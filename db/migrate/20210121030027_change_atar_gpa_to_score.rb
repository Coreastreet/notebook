class ChangeAtarGpaToScore < ActiveRecord::Migration[6.1]
  def change
    rename_table :atars, :scores
  end
end
