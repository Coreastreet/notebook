class SetDefaultDeadlineTimeToMidnight < ActiveRecord::Migration[6.1]
  def change
    change_column_default :competitions, :deadline_time, from: nil, to: "11:59"
  end
end
