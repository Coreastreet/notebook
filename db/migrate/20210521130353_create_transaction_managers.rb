class CreateTransactionManagers < ActiveRecord::Migration[6.1]
  def change
    create_table :transaction_managers do |t|
      t.integer :guest_session_count, null: false, default: 0
      t.integer :student_session_count, null: false, default: 0

      t.timestamps
    end
  end
end
