class AddMostRecentModuleToProgressEvent < ActiveRecord::Migration[6.1]
  def change
    add_column :progress_events, :most_recent_module_id, :integer
  end
end
