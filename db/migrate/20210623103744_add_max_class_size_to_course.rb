class AddMaxClassSizeToCourse < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :max_class_size, :integer, limit: 4, default: 0
  end
end
