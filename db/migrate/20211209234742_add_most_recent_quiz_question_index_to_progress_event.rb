class AddMostRecentQuizQuestionIndexToProgressEvent < ActiveRecord::Migration[6.1]
  def change
    add_column :progress_events, :most_recent_quiz_question_index, 
               :jsonb, default: {}
  end
end
