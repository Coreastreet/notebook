class CreateCompetitions < ActiveRecord::Migration[6.1]
  def change
    create_table :competitions do |t|
      t.integer :prize_money, default: 0
      t.date :deadline_date
      t.time :deadline_time
      t.integer :max_video_length, default: 600
      t.integer :videos_submissions_count, default: 0
      t.string :title
      t.references :student, null: false, foreign_key: true

      t.timestamps
    end
  end
end
