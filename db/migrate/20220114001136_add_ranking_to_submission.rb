class AddRankingToSubmission < ActiveRecord::Migration[6.1]
  def change
    add_column :video_submissions, :ranking, :integer, limit: 1
  end
end
