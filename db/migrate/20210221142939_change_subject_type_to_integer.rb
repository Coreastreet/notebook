class ChangeSubjectTypeToInteger < ActiveRecord::Migration[6.1]
  def change
    remove_column :notes, :subject, :string
    add_column :notes, :subject, :integer
  end
end
