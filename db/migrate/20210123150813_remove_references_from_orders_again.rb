class RemoveReferencesFromOrdersAgain < ActiveRecord::Migration[6.1]
  def change
    remove_reference :orders, :notes, null: false, foreign_key: true
    add_reference :orders, :note, null: false, foreign_key: true
  end
end
