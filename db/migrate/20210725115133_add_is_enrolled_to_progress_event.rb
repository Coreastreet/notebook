class AddIsEnrolledToProgressEvent < ActiveRecord::Migration[6.1]
  def change
    add_column :progress_events, :is_enrolled, :boolean, default: :false
  end
end
