class RenameNumberOfQuizzesInModulePart < ActiveRecord::Migration[6.1]
  def change
    rename_column :module_parts, :number_of_quizzes, :quizzes_count 
  end
end
