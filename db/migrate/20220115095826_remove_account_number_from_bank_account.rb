class RemoveAccountNumberFromBankAccount < ActiveRecord::Migration[6.1]
  def change
    remove_column :bank_accounts, :account_number, :integer
  end
end
