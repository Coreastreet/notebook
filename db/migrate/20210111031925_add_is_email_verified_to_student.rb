class AddIsEmailVerifiedToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :is_email_verified, :boolean, default: false
  end
end
