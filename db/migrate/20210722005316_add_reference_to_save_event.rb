class AddReferenceToSaveEvent < ActiveRecord::Migration[6.1]
  def change
    add_reference :save_events, :course, null: false, foreign_key: true
  end
end
