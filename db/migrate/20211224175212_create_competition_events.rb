class CreateCompetitionEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :competition_events do |t|
      t.references :student, null: false, foreign_key: true
      t.references :competition, null: false, foreign_key: true

      t.timestamps
    end
  end
end
