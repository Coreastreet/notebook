class RemoveStudentIdFromNotes < ActiveRecord::Migration[6.1]
  def change
     remove_reference :notes, :student, index: true
  end
end
