class AddStartDateToCompetition < ActiveRecord::Migration[6.1]
  def change
    add_column :competitions, :start_date, :date
  end
end
