class AddUniqueChildToCompetitionEvent < ActiveRecord::Migration[6.1]
  def change
    add_index :competition_events, [:student_id, :competition_id], unique: true
  end
end
