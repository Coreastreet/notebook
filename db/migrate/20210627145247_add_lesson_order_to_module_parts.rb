class AddLessonOrderToModuleParts < ActiveRecord::Migration[6.1]
  def change
    add_column :module_parts, :lesson_order, :integer, array: true, default: []
  end
end
