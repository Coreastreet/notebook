class RemoveTitleFromCompetition < ActiveRecord::Migration[6.1]
  def change
    remove_column :competitions, :title, :string
  end
end
