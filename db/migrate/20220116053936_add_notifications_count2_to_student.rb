class AddNotificationsCount2ToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :notifications_count, :integer, default: 0
  end
end
