class AddMessageToVideoSubmission < ActiveRecord::Migration[6.1]
  def change
    add_column :video_submissions, :message, :text
  end
end
