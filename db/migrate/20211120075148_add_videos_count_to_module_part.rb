class AddVideosCountToModulePart < ActiveRecord::Migration[6.1]
  def change
    add_column :module_parts, :videos_count, :integer, default: 0
  end
end
