class RemoveReferencesFromNotes < ActiveRecord::Migration[6.1]
  def change
    remove_reference :notes, :high_school_subject, null: false
    remove_reference :notes, :uni_subject, null: false
  end
end
