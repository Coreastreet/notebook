class AddHasPrizeToCompetition < ActiveRecord::Migration[6.1]
  def change
    add_column :competitions, :has_prize, :boolean, default: false
  end
end
