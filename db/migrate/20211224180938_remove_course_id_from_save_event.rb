class RemoveCourseIdFromSaveEvent < ActiveRecord::Migration[6.1]
  def change
    remove_column :save_events, :course_id, :reference
  end
end
