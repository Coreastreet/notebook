class ChangeColumnNullFromVideoSubmissions < ActiveRecord::Migration[6.1]
  def change
    change_column_null :video_submissions, :assignment_video_id, true
  end
end
