class AddTotalPriceToShoppingCart < ActiveRecord::Migration[6.1]
  def change
      add_column :shopping_carts, :total_price, :decimal, precision: 6, scale: 2, default: 0.00
  end
end
