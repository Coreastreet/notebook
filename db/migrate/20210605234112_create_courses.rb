class CreateCourses < ActiveRecord::Migration[6.1]
  def change
    create_table :courses do |t|
      t.string :title
      t.decimal :price
      t.integer :subject
      t.integer :number_of_videos
      t.integer :number_of_exercises
      t.text :description
      t.references :student, null: false, foreign_key: true
      t.integer :difficulty_level
      t.string :id_token

      t.timestamps
    end
    add_index :courses, :id_token, unique: true
  end
end
