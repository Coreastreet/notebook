class ChangeColumnDefaultForDeadline < ActiveRecord::Migration[6.1]
  def change
     change_column_default :assignment_videos, :deadline, nil
  end
end
