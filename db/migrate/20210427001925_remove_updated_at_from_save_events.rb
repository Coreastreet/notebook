class RemoveUpdatedAtFromSaveEvents < ActiveRecord::Migration[6.1]
  def change
    remove_column :save_events, :updated_at, :datetime
  end
end
