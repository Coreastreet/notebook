class AddSaveableToSaveEvent < ActiveRecord::Migration[6.1]
  def change
    add_reference :save_events, :saveable, polymorphic: true, null: false
  end
end
