class DropCourseTable < ActiveRecord::Migration[6.1]
  def change
    remove_column :courses, :title, :string
  end
end
