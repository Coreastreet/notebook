class DropSubResultsTable < ActiveRecord::Migration[6.1]
  def change
    drop_table :sub_results
  end
end
