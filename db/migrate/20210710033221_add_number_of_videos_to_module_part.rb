class AddNumberOfVideosToModulePart < ActiveRecord::Migration[6.1]
  def change
    add_column :module_parts, :number_of_videos, :integer, default: 0
  end
end
