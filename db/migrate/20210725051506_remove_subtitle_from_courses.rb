class RemoveSubtitleFromCourses < ActiveRecord::Migration[6.1]
  def change
    remove_column :courses, :subtitle, :string
  end
end
