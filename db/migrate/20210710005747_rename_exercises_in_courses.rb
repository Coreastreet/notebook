class RenameExercisesInCourses < ActiveRecord::Migration[6.1]
  def change
      rename_column :courses, :number_of_exercises, :number_of_quizzes 
  end
end
