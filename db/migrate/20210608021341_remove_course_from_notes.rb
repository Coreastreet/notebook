class RemoveCourseFromNotes < ActiveRecord::Migration[6.1]
  def change
    remove_column :pdfs, :course, :integer
  end
end
