class AddInclusionToMarkSubResult < ActiveRecord::Migration[6.1]
  def change
    change_column :sub_results, :mark, :integer, inclusion: 0..100
  end
end
