class RemoveShoppingCartFromNotes < ActiveRecord::Migration[6.1]
  def change
    remove_reference :notes, :shopping_cart, null: false, foreign_key: true
  end
end
