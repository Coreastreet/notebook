class RemoveColorFromAvatar < ActiveRecord::Migration[6.1]
  def change
    remove_column :avatars, :color, :string
  end
end
