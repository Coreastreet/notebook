class CreateModuleParts < ActiveRecord::Migration[6.1]
  def change
    create_table :module_parts do |t|
      t.string :name
      t.references :course, null: false, foreign_key: true

      t.timestamps
    end
  end
end
