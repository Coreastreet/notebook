class AddIndexToSubmissionRanking < ActiveRecord::Migration[6.1]
  def change
    add_index :video_submissions, :ranking, unique: true
  end
end
