class AddOccupationToStudents < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :occupation, :string
  end
end
