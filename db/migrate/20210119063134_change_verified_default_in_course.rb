class ChangeVerifiedDefaultInCourse < ActiveRecord::Migration[6.1]
  def change
    change_column :courses, :verified, :boolean, default: false
  end
end
