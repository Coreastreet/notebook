class ChangeMessageToDescription < ActiveRecord::Migration[6.1]
  def change
    rename_column :video_submissions, :message, :description
  end
end
