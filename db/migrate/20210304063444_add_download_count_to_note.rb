class AddDownloadCountToNote < ActiveRecord::Migration[6.1]
  def change
    add_column :notes, :download_count, :integer, default: 0
  end
end
