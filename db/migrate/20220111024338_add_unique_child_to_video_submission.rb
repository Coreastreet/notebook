class AddUniqueChildToVideoSubmission < ActiveRecord::Migration[6.1]
  def change
    add_index :video_submissions, [:student_id, :competition_id], unique: true
  end
end
