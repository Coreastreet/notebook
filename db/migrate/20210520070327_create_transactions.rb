class CreateTransactions < ActiveRecord::Migration[6.1]
  def change
    create_table :transactions do |t|
      t.integer :number_of_orders
      t.references :student, null: false, foreign_key: true
      t.decimal :total_price, precision: 7, scale: 2

      t.timestamps
    end
  end
end
