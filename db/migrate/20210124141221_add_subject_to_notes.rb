class AddSubjectToNotes < ActiveRecord::Migration[6.1]
  def change
    add_column :notes, :subject, :string
    add_column :notes, :note_type, :integer
  end
end
