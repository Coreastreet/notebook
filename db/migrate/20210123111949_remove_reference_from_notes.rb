class RemoveReferenceFromNotes < ActiveRecord::Migration[6.1]
  def change
    remove_reference(:notes, :notable, polymorphic: true)
  end
end
