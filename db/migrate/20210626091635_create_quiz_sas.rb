class CreateQuizSas < ActiveRecord::Migration[6.1]
  def change
    create_table :quiz_sas do |t|
      t.string :title
      t.string :question
      t.text :correct_answer

      t.timestamps
    end
  end
end
