class AddFieldsToAssignmentVideo < ActiveRecord::Migration[6.1]
  def change
    add_column :assignment_videos, :video_submissions_count, :integer, default: 0
    add_column :assignment_videos, :min_students, :integer, default: 2
    add_column :assignment_videos, :max_video_length, :integer, default: 600
    add_column :assignment_videos, :deadline, :time, default: Time.now + 1.day
    add_column :assignment_videos, :prize_money, :decimal, precision: 3, scale: 2, default: 0.00
  end
end
