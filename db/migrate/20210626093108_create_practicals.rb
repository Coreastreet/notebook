class CreatePracticals < ActiveRecord::Migration[6.1]
  def change
    create_table :practicals do |t|
      t.string :title
      t.text :description
      t.date :dateOfMeetup
      t.string :location
      t.time :startTime
      t.time :endTime
      t.decimal :price
      t.integer :min_students
      t.integer :max_students
      t.integer :students_booked

      t.timestamps
    end
  end
end
