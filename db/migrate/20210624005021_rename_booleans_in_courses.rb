class RenameBooleansInCourses < ActiveRecord::Migration[6.1]
  def change
    rename_column :courses, :has_assignment_marking?, :has_assignment_marking
    rename_column :courses, :has_class_limit?, :has_class_limit
  end
end
