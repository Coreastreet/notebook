class AddBackgroundColorToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :background_color, :string
  end
end
