class AddQuestionOrderToQuiz < ActiveRecord::Migration[6.1]
  def change
    add_column :mc_quizzes, :question_order, :integer, array: true, default: []
  end
end
