class CreateQuizMas < ActiveRecord::Migration[6.1]
  def change
    create_table :quiz_mas do |t|
      t.string :title
      t.text :hash_array_answers

      t.timestamps
    end
  end
end
