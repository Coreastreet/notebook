class RenameCoursesToResults < ActiveRecord::Migration[6.1]
  def change
    rename_table :courses, :results
  end
end
