class ChangeTitleInLessons < ActiveRecord::Migration[6.1]
  def change
    change_column :lessons, :title, :string, default: 'Untitled'
  end
end
