class AddIsPremiumToCourses < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :is_premium, :boolean, default: :false
  end
end
