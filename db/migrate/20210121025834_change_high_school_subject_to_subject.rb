class ChangeHighSchoolSubjectToSubject < ActiveRecord::Migration[6.1]
  def change
    rename_table :high_school_subjects, :subjects
  end
end
