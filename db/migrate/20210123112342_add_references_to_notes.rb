class AddReferencesToNotes < ActiveRecord::Migration[6.1]
  def change
    add_column :notes, :description, :text
    add_reference :notes, :students, null: false, foreign_key: true
  end
end
