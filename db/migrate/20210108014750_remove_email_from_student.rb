class RemoveEmailFromStudent < ActiveRecord::Migration[6.1]
  def change
    remove_column :students, :email, :string
  end
end
