class AddAssignmentMarkingChargeToCourse < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :assignment_marking_charge, :integer, limit: 4, default: 0
  end
end
