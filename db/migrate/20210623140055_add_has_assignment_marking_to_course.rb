class AddHasAssignmentMarkingToCourse < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :has_assignment_marking?, :boolean, default: false
  end
end
