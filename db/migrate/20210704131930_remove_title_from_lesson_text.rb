class RemoveTitleFromLessonText < ActiveRecord::Migration[6.1]
  def change
    remove_column :lesson_texts, :title, :string
  end
end
