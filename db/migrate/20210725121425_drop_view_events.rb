class DropViewEvents < ActiveRecord::Migration[6.1]
  def change
    drop_table :view_events
  end
end
