class CreateQuizMcs < ActiveRecord::Migration[6.1]
  def change
    create_table :quiz_mcs do |t|
      t.string :title
      t.boolean :timeLimit
      t.integer :time_allowed
      t.integer :mc_questions_count

      t.timestamps
    end
  end
end
