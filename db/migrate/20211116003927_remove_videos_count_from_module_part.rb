class RemoveVideosCountFromModulePart < ActiveRecord::Migration[6.1]
  def change
    remove_column :module_parts, :videos_count, :integer
  end
end
