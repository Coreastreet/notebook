class CreateAssignmentTexts < ActiveRecord::Migration[6.1]
  def change
    create_table :assignment_texts do |t|
      t.text :question

      t.timestamps
    end
  end
end
