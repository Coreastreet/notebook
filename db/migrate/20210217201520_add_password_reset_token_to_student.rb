class AddPasswordResetTokenToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :password_reset_digest, :string
  end
end
