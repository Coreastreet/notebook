class RemoveExplanationFromLessonText < ActiveRecord::Migration[6.1]
  def change
    remove_column :lesson_texts, :explanation, :text
  end
end
