class CreateBillingAddresses < ActiveRecord::Migration[6.1]
  def change
    create_table :billing_addresses do |t|
      t.string :address_line_one
      t.string :address_line_two
      t.string :suburb
      t.string :state
      t.integer :postcode, limit: 3
      t.string :country
      t.references :student, null: false, foreign_key: true

      t.timestamps
    end
  end
end
