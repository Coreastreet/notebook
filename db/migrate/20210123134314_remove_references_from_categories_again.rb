class RemoveReferencesFromCategoriesAgain < ActiveRecord::Migration[6.1]
  def change
    remove_reference :categories, :notes, null: false, foreign_key: true
    add_reference :categories, :note, null: false, foreign_key: true
  end
end
