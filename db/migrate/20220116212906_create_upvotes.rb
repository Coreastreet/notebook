class CreateUpvotes < ActiveRecord::Migration[6.1]
  def change
    create_table :upvotes do |t|
      t.references :student, null: false, foreign_key: true
      t.references :upvoteable, polymorphic: true, null: false

      t.timestamps
    end
  end
end
