class AddQuizAnswersToProgressEvent < ActiveRecord::Migration[6.1]
  def change
    add_column :progress_events, :hash_of_quiz_answers, :jsonb, default: {}
  end
end
