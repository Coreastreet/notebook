class RemoveRatingFromCompetitionEvent < ActiveRecord::Migration[6.1]
  def change
    remove_column :competition_events, :rating, :decimal
  end
end
