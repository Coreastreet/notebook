class AddTitleToCompetition < ActiveRecord::Migration[6.1]
  def change
    add_column :competitions, :title, :string, default: "Untitled"
  end
end
