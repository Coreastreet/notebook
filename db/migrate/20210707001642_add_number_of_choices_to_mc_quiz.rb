class AddNumberOfChoicesToMcQuiz < ActiveRecord::Migration[6.1]
  def change
    add_column :mc_quizzes, :number_of_choices, :integer, default: 3
  end
end
