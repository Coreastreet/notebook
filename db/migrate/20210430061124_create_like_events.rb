class CreateLikeEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :like_events do |t|
      t.references :student, null: false, foreign_key: true
      t.references :note, null: false, foreign_key: true
      t.boolean :like_status, null: false, default: false
      t.boolean :dislike_status, null: false, default: false

      t.timestamps
    end
  end
end
