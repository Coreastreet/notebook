class CreateSaveEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :save_events do |t|
      t.references :student, null: false, foreign_key: true
      t.references :note, null: false, foreign_key: true

      t.timestamps
    end
  end
end
