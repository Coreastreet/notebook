class AddDeadlineDateToAssignmentVideos < ActiveRecord::Migration[6.1]
  def change
    add_column :assignment_videos, :deadline_date, :date
  end
end
