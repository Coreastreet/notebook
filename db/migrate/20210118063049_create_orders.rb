class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.decimal :total
      t.references :notes, null: false, foreign_key: true
      t.references :buyer
      t.references :seller

      t.timestamps
    end

    add_foreign_key :orders, :students, column: :buyer_id, primary_key: :id
    add_foreign_key :orders, :students, column: :seller_id, primary_key: :id
  end
end
