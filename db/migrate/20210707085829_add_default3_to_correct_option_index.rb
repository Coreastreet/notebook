class AddDefault3ToCorrectOptionIndex < ActiveRecord::Migration[6.1]
  def change
    change_column :mc_quizzes, :correct_answer_index, :integer, default: 0
  end
end
