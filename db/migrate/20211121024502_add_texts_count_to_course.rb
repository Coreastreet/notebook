class AddTextsCountToCourse < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :texts_count, :integer, default: 0
  end
end
