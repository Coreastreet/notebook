class AddMostRecentLessonToProgressEvent < ActiveRecord::Migration[6.1]
  def change
    add_column :progress_events, :most_recent_lesson_id, :integer
  end
end
