class AddLessonTypeToModuleParts < ActiveRecord::Migration[6.1]
  def change
    add_column :module_parts, :lesson_type, :integer, default: [], array: true
  end
end
