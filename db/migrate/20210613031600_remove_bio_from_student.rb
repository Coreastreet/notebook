class RemoveBioFromStudent < ActiveRecord::Migration[6.1]
  def change
    remove_column :students, :bio, :text
  end
end
