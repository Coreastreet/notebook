class MakeZeroDefaultAgainInCourses < ActiveRecord::Migration[6.1]
  def change
      change_column :courses, :number_of_quizzes, :integer, default: 0
      change_column :courses, :number_of_videos, :integer, default: 0
  end
end
