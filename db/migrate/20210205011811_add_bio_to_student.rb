class AddBioToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :bio, :text
  end
end
