class MakeZeroDefaultInCourses < ActiveRecord::Migration[6.1]
  def change
      change_column :courses, :number_of_quizzes, :integer, null: true
      change_column :courses, :number_of_videos, :integer, null: true
  end
end
