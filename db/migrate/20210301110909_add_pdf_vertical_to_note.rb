class AddPdfVerticalToNote < ActiveRecord::Migration[6.1]
  def change
    add_column :notes, :is_horizontal?, :boolean, default: false
  end
end
