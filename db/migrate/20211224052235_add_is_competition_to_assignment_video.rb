class AddIsCompetitionToAssignmentVideo < ActiveRecord::Migration[6.1]
  def change
    add_column :assignment_videos, :is_competition, :boolean, default: false, null: false
  end
end
