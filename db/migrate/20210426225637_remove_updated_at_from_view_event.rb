class RemoveUpdatedAtFromViewEvent < ActiveRecord::Migration[6.1]
  def change
    remove_column :view_events, :updated_at, :datetime
  end
end
