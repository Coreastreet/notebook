class AddSubjectTypeToSubject < ActiveRecord::Migration[6.1]
  def change
    add_column :subjects, :subject_type, :integer
  end
end
