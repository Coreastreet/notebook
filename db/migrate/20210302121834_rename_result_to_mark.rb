class RenameResultToMark < ActiveRecord::Migration[6.1]
  def change
    rename_column :results, :result, :mark
  end
end
