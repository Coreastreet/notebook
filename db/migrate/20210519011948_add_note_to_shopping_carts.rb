class AddNoteToShoppingCarts < ActiveRecord::Migration[6.1]
  def change
    add_reference :shopping_carts, :note, null: false, foreign_key: true
  end
end
