class RenameNameInLessons < ActiveRecord::Migration[6.1]
  def change
      rename_column :lessons, :name, :title
  end
end
