class RemoveEmailVerifiedFromStudent < ActiveRecord::Migration[6.1]
  def change
    remove_column :students, :email_verified, :boolean
  end
end
