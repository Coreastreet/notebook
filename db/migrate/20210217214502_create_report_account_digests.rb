class CreateReportAccountDigests < ActiveRecord::Migration[6.1]
  def change
    create_table :report_account_digests do |t|
      t.string :digest
      t.references :student, null: false, foreign_key: true

      t.timestamps
    end
  end
end
