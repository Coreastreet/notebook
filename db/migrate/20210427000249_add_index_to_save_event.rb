class AddIndexToSaveEvent < ActiveRecord::Migration[6.1]
  def change
    add_index :save_events, [:student_id, :note_id], unique: true
  end
end
