class AddActivatedToPasswordResetDigest < ActiveRecord::Migration[6.1]
  def change
    add_column :password_reset_digests, :activated, :boolean, default: false
  end
end
