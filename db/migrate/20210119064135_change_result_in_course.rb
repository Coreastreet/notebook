class ChangeResultInCourse < ActiveRecord::Migration[6.1]
  def change
    change_column :courses, :result, :decimal, precision: 5, scale: 2
  end
end
