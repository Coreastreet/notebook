class ChangeDeadlineInAssignmentVideos < ActiveRecord::Migration[6.1]
  def change
    rename_column :assignment_videos, :deadline, :deadline_time
  end
end
