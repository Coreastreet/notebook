class AddResultsDateToCompetition < ActiveRecord::Migration[6.1]
  def change
    add_column :competitions, :results_date, :date
  end
end
