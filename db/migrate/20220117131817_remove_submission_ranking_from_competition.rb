class RemoveSubmissionRankingFromCompetition < ActiveRecord::Migration[6.1]
  def change
    remove_column :competitions, :submission_rankings, :integer
  end
end
