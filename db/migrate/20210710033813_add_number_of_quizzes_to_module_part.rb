class AddNumberOfQuizzesToModulePart < ActiveRecord::Migration[6.1]
  def change
    add_column :module_parts, :number_of_quizzes, :integer, default: 0
  end
end
