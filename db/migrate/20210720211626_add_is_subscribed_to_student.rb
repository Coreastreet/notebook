class AddIsSubscribedToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :is_subscribed, :boolean, default: false
  end
end
