class CreateQuizFas < ActiveRecord::Migration[6.1]
  def change
    create_table :quiz_fas do |t|
      t.string :title
      t.text :answer_text
      t.integer :hidden_word_indexes, array: true, default: []

      t.timestamps
    end
  end
end
