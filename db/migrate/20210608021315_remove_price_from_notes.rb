class RemovePriceFromNotes < ActiveRecord::Migration[6.1]
  def change
    remove_column :pdfs, :price, :decimal
  end
end
