class AddTimeZoneToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :time_zone, :string, null: false, default: "+00:00"
  end
end
