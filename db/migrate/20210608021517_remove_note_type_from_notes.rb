class RemoveNoteTypeFromNotes < ActiveRecord::Migration[6.1]
  def change
    remove_column :pdfs, :note_type, :integer
  end
end
