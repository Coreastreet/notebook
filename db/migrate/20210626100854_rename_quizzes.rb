class RenameQuizzes < ActiveRecord::Migration[6.1]
  def change
    rename_table :quiz_fas, :fa_quizzes
    rename_table :quiz_mas, :ma_quizzes
    rename_table :quiz_mcs, :mc_quizzes
    rename_table :quiz_sas, :sa_quizzes
  end
end
