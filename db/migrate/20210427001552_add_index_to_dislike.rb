class AddIndexToDislike < ActiveRecord::Migration[6.1]
  def change
     add_index :dislikes, [:student_id, :note_id], unique: true
  end
end
