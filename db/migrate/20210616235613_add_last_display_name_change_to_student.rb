class AddLastDisplayNameChangeToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :last_display_name_change, :datetime
  end
end
