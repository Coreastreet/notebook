class CreateProgressEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :progress_events do |t|
      t.references :student, null: false, foreign_key: true
      t.references :course, null: false, foreign_key: true
      t.integer :lessons_completed, array: true, default: []
      t.boolean :is_complete

      t.timestamps
    end
  end
end
