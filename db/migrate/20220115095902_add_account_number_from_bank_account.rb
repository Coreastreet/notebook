class AddAccountNumberFromBankAccount < ActiveRecord::Migration[6.1]
  def change
    add_column :bank_accounts, :account_number, :bigint
  end
end
