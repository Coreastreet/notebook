class RemoveAttrFromMcQuiz < ActiveRecord::Migration[6.1]
  def change
    remove_column :mc_quizzes, :mc_questions_count, :integer
    remove_column :mc_quizzes, :timeLimit, :boolean
    remove_column :mc_quizzes, :time_allowed, :integer
  end
end
