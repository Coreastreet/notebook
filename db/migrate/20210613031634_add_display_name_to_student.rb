class AddDisplayNameToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :display_name, :string
  end
end
