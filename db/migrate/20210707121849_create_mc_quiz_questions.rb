class CreateMcQuizQuestions < ActiveRecord::Migration[6.1]
  def change
    create_table :mc_quiz_questions do |t|
      t.string :options, default: [], array: true
      t.integer :correct_answer_index, default: 0
      t.integer :number_of_choices, default: 4
      t.references :mc_quiz, null: false, foreign_key: true

      t.timestamps
    end
  end
end
