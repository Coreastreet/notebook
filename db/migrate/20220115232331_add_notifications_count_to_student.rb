class AddNotificationsCountToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :unread_notifications_count, :integer, default: 0
  end
end
