class ChangeDefaultRatingInSubmission < ActiveRecord::Migration[6.1]
  def change
    change_column_default :video_submissions, :rating, from: nil, to: 0
  end
end
