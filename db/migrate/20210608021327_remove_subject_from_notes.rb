class RemoveSubjectFromNotes < ActiveRecord::Migration[6.1]
  def change
    remove_column :pdfs, :subject, :integer
  end
end
