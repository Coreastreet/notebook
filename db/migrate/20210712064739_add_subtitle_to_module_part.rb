class AddSubtitleToModulePart < ActiveRecord::Migration[6.1]
  def change
    add_column :module_parts, :subtitle, :string
  end
end
