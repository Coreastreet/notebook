class RenameIsCompleteInProgressEvent < ActiveRecord::Migration[6.1]
  def change
    rename_column :progress_events, :is_complete, :is_completed
  end
end
