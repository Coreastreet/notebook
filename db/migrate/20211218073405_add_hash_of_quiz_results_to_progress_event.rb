class AddHashOfQuizResultsToProgressEvent < ActiveRecord::Migration[6.1]
  def change
    add_column :progress_events, :hash_of_quiz_results, :jsonb, default: {}
  end
end
