class AddEmailVerifiedToStudents < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :email_verified, :boolean
  end
end
