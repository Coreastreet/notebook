class CreateNotes < ActiveRecord::Migration[6.1]
  def change
    create_table :notes do |t|
      t.string :title
      t.decimal :price
      t.integer :number_of_pages
      t.references :high_school_subject, null: false, foreign_key: true
      t.references :uni_subject, null: false, foreign_key: true

      t.timestamps
    end
  end
end
