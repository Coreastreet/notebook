class RenameNameInModuleParts < ActiveRecord::Migration[6.1]
  def change
      rename_column :module_parts, :name, :title
  end
end
