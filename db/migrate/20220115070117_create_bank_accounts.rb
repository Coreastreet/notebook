class CreateBankAccounts < ActiveRecord::Migration[6.1]
  def change
    create_table :bank_accounts do |t|
      t.string :account_holder_name
      t.integer :account_number
      t.integer :bsb
      t.references :student, null: false, foreign_key: true

      t.timestamps
    end
  end
end
