class AddWinAlertToNotification < ActiveRecord::Migration[6.1]
  def change
    add_column :notifications, :win_alert, :boolean, default: false
  end
end
