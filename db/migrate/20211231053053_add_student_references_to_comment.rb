class AddStudentReferencesToComment < ActiveRecord::Migration[6.1]
  def change
    add_reference :comments, :student, null: false, foreign_key: true
  end
end
