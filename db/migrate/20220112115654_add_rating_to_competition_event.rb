class AddRatingToCompetitionEvent < ActiveRecord::Migration[6.1]
  def change
    add_column :competition_events, :rating, :decimal, precision: 3, scale: 1
  end
end
