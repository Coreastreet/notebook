class CreateAssignmentVideos < ActiveRecord::Migration[6.1]
  def change
    create_table :assignment_videos do |t|
      t.text :question

      t.timestamps
    end
  end
end
