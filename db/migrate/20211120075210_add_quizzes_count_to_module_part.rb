class AddQuizzesCountToModulePart < ActiveRecord::Migration[6.1]
  def change
    add_column :module_parts, :quizzes_count, :integer, default: 0
  end
end
