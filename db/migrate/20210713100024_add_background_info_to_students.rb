class AddBackgroundInfoToStudents < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :background_info, :text
  end
end
