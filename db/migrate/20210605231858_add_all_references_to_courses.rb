class AddAllReferencesToCourses < ActiveRecord::Migration[6.1]
  def change
      add_reference :categories, :course, index: true
      add_reference :comments, :course, index: true
      add_reference :like_events, :course, index: true
      add_reference :orders, :course, index: true
      add_reference :save_events, :course, index: true
      add_reference :shopping_carts, :course, index: true
      add_reference :view_events, :course, index: true
  end
end
