class RemoveReferencesFromVideoSubmissions < ActiveRecord::Migration[6.1]
  def change
    remove_reference :video_submissions, :competition_event, index: true, foreign_key: true
  end
end
