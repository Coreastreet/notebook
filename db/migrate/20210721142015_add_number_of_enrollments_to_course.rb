class AddNumberOfEnrollmentsToCourse < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :number_of_enrolments, :integer, default: 0
  end
end
