class AddLearnableToLesson < ActiveRecord::Migration[6.1]
  def change
    add_reference :lessons, :learnable, polymorphic: true, index: true, null: false
  end
end
