class RemoveInclusionToMarkSubResult < ActiveRecord::Migration[6.1]
  def change
    change_column :sub_results, :mark, :integer, limit: 1
  end
end
