class AddTitleToMcQuizQuestion < ActiveRecord::Migration[6.1]
  def change
    add_column :mc_quiz_questions, :title, :string
  end
end
