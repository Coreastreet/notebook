class ConvertCourseToEnum < ActiveRecord::Migration[6.1]
  def change
    remove_column :notes, :course, :string
  end
end
