class AddReasonForDeactivationToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :reason_for_deactivation, :text
  end
end
