class AddCompetitionToVideoSubmission < ActiveRecord::Migration[6.1]
  def change
    add_reference :video_submissions, :competition, null: false, foreign_key: true
  end
end
