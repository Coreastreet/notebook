class AddModuleOrderToCourses < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :module_order, :integer, array: true, default: []
  end
end
