class RemoveNumberOfQuizzesFromCourses < ActiveRecord::Migration[6.1]
  def change
    remove_column :courses, :number_of_quizzes, :integer
  end
end
