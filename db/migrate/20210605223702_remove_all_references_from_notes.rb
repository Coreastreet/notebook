class RemoveAllReferencesFromNotes < ActiveRecord::Migration[6.1]
  def change
      remove_reference :categories, :note, index: true
      remove_reference :comments, :note, index: true
      remove_reference :like_events, :note, index: true
      remove_reference :orders, :note, index: true
      remove_reference :save_events, :note, index: true
      remove_reference :shopping_carts, :note, index: true
      remove_reference :view_events, :note, index: true
  end
end
