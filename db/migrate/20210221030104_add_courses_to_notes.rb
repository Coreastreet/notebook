class AddCoursesToNotes < ActiveRecord::Migration[6.1]
  def change
    add_column :notes, :course, :string
  end
end
