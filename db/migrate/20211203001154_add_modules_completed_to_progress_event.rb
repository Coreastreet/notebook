class AddModulesCompletedToProgressEvent < ActiveRecord::Migration[6.1]
  def change
    add_column :progress_events, :modules_completed, :integer, default: [], array: true
  end
end
