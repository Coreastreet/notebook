class RemoveSamplePageFromNotes < ActiveRecord::Migration[6.1]
  def change
    remove_column :pdfs, :number_of_sample_pages, :integer
  end
end
