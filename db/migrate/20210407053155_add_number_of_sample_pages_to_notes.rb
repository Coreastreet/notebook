class AddNumberOfSamplePagesToNotes < ActiveRecord::Migration[6.1]
  def change
    add_column :notes, :number_of_sample_pages, :integer
  end
end
