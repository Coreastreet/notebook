class CreatePasswordResetDigests < ActiveRecord::Migration[6.1]
  def change
    create_table :password_reset_digests do |t|
      t.string :digest
      t.references :student, null: false, foreign_key: true

      t.timestamps
    end
  end
end
