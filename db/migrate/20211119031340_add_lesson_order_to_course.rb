class AddLessonOrderToCourse < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :lesson_order, :integer, default: [], array: true
  end
end
