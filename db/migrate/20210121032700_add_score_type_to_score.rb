class AddScoreTypeToScore < ActiveRecord::Migration[6.1]
  def change
    add_column :scores, :score_type, :integer
  end
end
