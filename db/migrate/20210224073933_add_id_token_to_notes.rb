class AddIdTokenToNotes < ActiveRecord::Migration[6.1]
  def change
    add_column :notes, :id_token, :string
    add_index :notes, :id_token, unique: true
  end
end
