class CreateVideoSubmissions < ActiveRecord::Migration[6.1]
  def change
    create_table :video_submissions do |t|
      t.references :assignment_video, null: false, foreign_key: true
      t.references :student, null: false, foreign_key: true
      t.integer :upvotes,  default: 0
      t.integer :downvotes,  default: 0

      t.timestamps
    end
  end
end
