class CreateAvatars < ActiveRecord::Migration[6.1]
  def change
    create_table :avatars do |t|
      t.string :background_color
      t.string :color
      t.references :student, null: false, foreign_key: true

      t.timestamps
    end
  end
end
