class AddPinToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :one_time_pin, :integer
  end
end
