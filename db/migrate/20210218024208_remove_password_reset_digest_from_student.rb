class RemovePasswordResetDigestFromStudent < ActiveRecord::Migration[6.1]
  def change
    remove_column :students, :password_reset_digest, :string
  end
end
