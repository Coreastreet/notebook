class AddJsonbToCourse < ActiveRecord::Migration[6.1]
  def change
    add_column :courses, :hash_of_lessons, :jsonb, default: {}
  end
end
