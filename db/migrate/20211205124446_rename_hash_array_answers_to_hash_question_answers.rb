class RenameHashArrayAnswersToHashQuestionAnswers < ActiveRecord::Migration[6.1]
  def change
    rename_column :mc_quiz_answers, :hash_array_answers, :hash_question_answers
  end
end
