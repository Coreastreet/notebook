class AddCommentsCountToVideoSubmission < ActiveRecord::Migration[6.1]
  def change
    add_column :video_submissions, :comments_count, :integer, default: 0
  end
end
