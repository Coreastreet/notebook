class AddLessonCountToModulePart < ActiveRecord::Migration[6.1]
  def change
    add_column :module_parts, :lesson_count, :integer
  end
end
