class AddOptionsToMcQuiz < ActiveRecord::Migration[6.1]
  def change
      add_column :mc_quizzes, :options, :string, array: true, default: []
      add_column :mc_quizzes, :correct_answer_index, :integer 
  end
end
