class CreateHighSchoolSubjects < ActiveRecord::Migration[6.1]
  def change
    create_table :high_school_subjects do |t|

      t.timestamps
    end
  end
end
