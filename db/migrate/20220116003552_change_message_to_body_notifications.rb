class ChangeMessageToBodyNotifications < ActiveRecord::Migration[6.1]
  def change
        change_column :notifications, :message, :text
  end
end
