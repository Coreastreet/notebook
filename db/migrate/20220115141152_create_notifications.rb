class CreateNotifications < ActiveRecord::Migration[6.1]
  def change
    create_table :notifications do |t|
      t.string :message
      t.boolean :read, default: false
      t.references :student, null: false, foreign_key: true

      t.timestamps
    end
  end
end
