class AddLikeStatusToLikeEvent < ActiveRecord::Migration[6.1]
  def change
    add_column :like_events, :like_status, :boolean, null: false, default: false
    add_column :like_events, :dislike_status, :boolean, null: false, default: false
  end
end
