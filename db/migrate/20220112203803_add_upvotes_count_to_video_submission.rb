class AddUpvotesCountToVideoSubmission < ActiveRecord::Migration[6.1]
  def change
    add_column :video_submissions, :upvotes_count, :integer, default: 0
  end
end
