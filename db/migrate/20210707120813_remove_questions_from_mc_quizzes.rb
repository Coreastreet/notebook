class RemoveQuestionsFromMcQuizzes < ActiveRecord::Migration[6.1]
  def change
      remove_column :mc_quizzes, :options, :string
      remove_column :mc_quizzes, :correct_answer_index, :string
      remove_column :mc_quizzes, :number_of_choices, :string
  end
end
