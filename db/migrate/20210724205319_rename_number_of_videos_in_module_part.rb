class RenameNumberOfVideosInModulePart < ActiveRecord::Migration[6.1]
  def change
    rename_column :module_parts, :number_of_videos, :videos_count 
  end
end
