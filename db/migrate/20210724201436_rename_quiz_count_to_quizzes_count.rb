class RenameQuizCountToQuizzesCount < ActiveRecord::Migration[6.1]
  def change
      rename_column :courses, :quiz_count, :quizzes_count
      change_column :courses, :quizzes_count, :integer, default: 0
  end
end
