class DropEnrollEvents < ActiveRecord::Migration[6.1]
  def change
    drop_table :enroll_events
  end
end
