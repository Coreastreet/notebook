class AddUserIndexToStudent < ActiveRecord::Migration[6.1]
  def change
    add_index :students, :username, unique: true
  end
end
