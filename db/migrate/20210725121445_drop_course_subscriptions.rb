class DropCourseSubscriptions < ActiveRecord::Migration[6.1]
  def change
    drop_table :course_subscriptions
  end
end
