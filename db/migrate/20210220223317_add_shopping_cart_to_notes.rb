class AddShoppingCartToNotes < ActiveRecord::Migration[6.1]
  def change
    add_reference :notes, :shopping_cart, null: true, foreign_key: true
  end
end
