class RemoveLessonCountFromModulePart < ActiveRecord::Migration[6.1]
  def change
    remove_column :module_parts, :lesson_count, :integer  
  end
end
