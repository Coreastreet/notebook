class AddPolymorphicReferenceToNotes < ActiveRecord::Migration[6.1]
  def change
    add_reference :notes, :notable, polymorphic: true, null: false
  end
end
