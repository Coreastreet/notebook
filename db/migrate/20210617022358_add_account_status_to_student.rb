class AddAccountStatusToStudent < ActiveRecord::Migration[6.1]
  def change
    add_column :students, :account_status, :integer, default: 0
  end
end
