class RenameLessonCountToLessonsCount < ActiveRecord::Migration[6.1]
  def change
      rename_column :courses, :lesson_count, :lessons_count
      change_column :courses, :lessons_count, :integer, default: 0
  end
end
