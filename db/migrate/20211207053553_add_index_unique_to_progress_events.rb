class AddIndexUniqueToProgressEvents < ActiveRecord::Migration[6.1]
  def change
    add_index :progress_events, [:student_id, :course_id], unique: true
  end
end
