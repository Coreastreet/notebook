# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_17_232839) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "assignment_texts", force: :cascade do |t|
    t.text "question"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "assignment_videos", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "video_submissions_count", default: 0
    t.integer "min_students", default: 2
    t.integer "max_video_length", default: 600
    t.time "deadline_time"
    t.integer "prize_money", default: 0
    t.date "deadline_date"
    t.boolean "is_competition", default: false, null: false
  end

  create_table "bank_accounts", force: :cascade do |t|
    t.string "account_holder_name"
    t.integer "bsb"
    t.bigint "student_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "account_number"
    t.index ["student_id"], name: "index_bank_accounts_on_student_id"
  end

  create_table "billing_addresses", force: :cascade do |t|
    t.string "address_line_one"
    t.string "address_line_two"
    t.string "suburb"
    t.string "state"
    t.integer "postcode"
    t.string "country"
    t.bigint "student_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["student_id"], name: "index_billing_addresses_on_student_id"
  end

  create_table "card_holders", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "course_id"
    t.index ["course_id"], name: "index_categories_on_course_id"
  end

  create_table "comments", force: :cascade do |t|
    t.text "body"
    t.string "commentable_type", null: false
    t.bigint "commentable_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "student_id", null: false
    t.integer "upvotes_count", default: 0
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable"
    t.index ["student_id"], name: "index_comments_on_student_id"
  end

  create_table "competition_events", force: :cascade do |t|
    t.bigint "student_id", null: false
    t.bigint "competition_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["competition_id"], name: "index_competition_events_on_competition_id"
    t.index ["student_id", "competition_id"], name: "index_competition_events_on_student_id_and_competition_id", unique: true
    t.index ["student_id"], name: "index_competition_events_on_student_id"
  end

  create_table "competitions", force: :cascade do |t|
    t.integer "prize_money", default: 0
    t.date "deadline_date"
    t.time "deadline_time", default: "2000-01-01 11:59:00"
    t.integer "max_video_length", default: 600
    t.integer "videos_submissions_count", default: 0
    t.bigint "student_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "subtitle"
    t.string "title", default: "Untitled"
    t.boolean "has_prize", default: false
    t.date "start_date"
    t.date "results_date"
    t.index ["student_id"], name: "index_competitions_on_student_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "title"
    t.decimal "price"
    t.integer "subject"
    t.text "description"
    t.integer "difficulty_level"
    t.string "id_token"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "student_id"
    t.text "objectives"
    t.integer "max_class_size", default: 0
    t.integer "assignment_marking_charge", default: 0
    t.boolean "has_class_limit", default: false
    t.boolean "has_assignment_marking", default: false
    t.integer "assignment_count"
    t.integer "quizzes_count", default: 0
    t.integer "lessons_count", default: 0
    t.integer "practical_count"
    t.integer "module_order", default: [], array: true
    t.integer "number_of_savers", default: 0
    t.integer "number_of_enrolments", default: 0
    t.integer "videos_count", default: 0
    t.boolean "is_premium", default: false
    t.jsonb "hash_of_lessons", default: {}
    t.integer "lesson_order", default: [], array: true
    t.integer "texts_count", default: 0
    t.index ["id_token"], name: "index_courses_on_id_token", unique: true
    t.index ["student_id"], name: "index_courses_on_student_id"
  end

  create_table "fa_quizzes", force: :cascade do |t|
    t.text "answer_text"
    t.integer "hidden_word_indexes", default: [], array: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "lesson_texts", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "lesson_videos", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "lessons", force: :cascade do |t|
    t.string "title", default: "Untitled"
    t.bigint "module_part_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "learnable_type", null: false
    t.bigint "learnable_id", null: false
    t.boolean "has_content", default: false
    t.index ["learnable_type", "learnable_id"], name: "index_lessons_on_learnable"
    t.index ["module_part_id"], name: "index_lessons_on_module_part_id"
  end

  create_table "ma_quizzes", force: :cascade do |t|
    t.text "hash_array_answers"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "mc_quiz_answers", force: :cascade do |t|
    t.bigint "student_id", null: false
    t.bigint "mc_quiz_id", null: false
    t.jsonb "hash_question_answers", default: {}
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["mc_quiz_id"], name: "index_mc_quiz_answers_on_mc_quiz_id"
    t.index ["student_id"], name: "index_mc_quiz_answers_on_student_id"
  end

  create_table "mc_quiz_questions", force: :cascade do |t|
    t.string "options", default: [], array: true
    t.integer "correct_answer_index", default: 0
    t.integer "number_of_choices", default: 4
    t.bigint "mc_quiz_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "title"
    t.index ["mc_quiz_id"], name: "index_mc_quiz_questions_on_mc_quiz_id"
  end

  create_table "mc_quizzes", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "question_order", default: [], array: true
    t.integer "mc_quiz_questions_count", default: 0
  end

  create_table "module_parts", force: :cascade do |t|
    t.string "title"
    t.bigint "course_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "lesson_order", default: [], array: true
    t.integer "lessons_count", default: 0
    t.string "subtitle"
    t.integer "lesson_type", default: [], array: true
    t.integer "videos_count", default: 0
    t.integer "quizzes_count", default: 0
    t.integer "texts_count", default: 0
    t.index ["course_id"], name: "index_module_parts_on_course_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.text "message"
    t.boolean "read", default: false
    t.bigint "student_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "notifiable_type"
    t.bigint "notifiable_id"
    t.boolean "win_alert", default: false
    t.index ["notifiable_type", "notifiable_id"], name: "index_notifications_on_notifiable"
    t.index ["student_id"], name: "index_notifications_on_student_id"
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "buyer_id"
    t.bigint "seller_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "transaction_id", null: false
    t.bigint "course_id"
    t.index ["buyer_id"], name: "index_orders_on_buyer_id"
    t.index ["course_id"], name: "index_orders_on_course_id"
    t.index ["seller_id"], name: "index_orders_on_seller_id"
    t.index ["transaction_id"], name: "index_orders_on_transaction_id"
  end

  create_table "password_reset_digests", force: :cascade do |t|
    t.string "digest"
    t.bigint "student_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "activated", default: false
    t.index ["student_id"], name: "index_password_reset_digests_on_student_id"
  end

  create_table "pdfs", force: :cascade do |t|
    t.string "title"
    t.integer "number_of_pages"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "description"
    t.string "id_token"
    t.boolean "is_horizontal?", default: false
    t.integer "download_count", default: 0
    t.decimal "paper_size_ratio", precision: 3, scale: 2, default: "1.3"
    t.index ["id_token"], name: "index_pdfs_on_id_token", unique: true
  end

  create_table "practicals", force: :cascade do |t|
    t.text "description"
    t.date "dateOfMeetup"
    t.string "location"
    t.time "startTime"
    t.time "endTime"
    t.decimal "price"
    t.integer "min_students"
    t.integer "max_students"
    t.integer "students_booked"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "progress_events", force: :cascade do |t|
    t.bigint "student_id", null: false
    t.bigint "course_id", null: false
    t.integer "lessons_completed", default: [], array: true
    t.boolean "is_completed", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.decimal "progress_percent", precision: 5, scale: 2, default: "0.0"
    t.boolean "is_enrolled", default: false
    t.integer "modules_completed", default: [], array: true
    t.integer "most_recent_lesson_id"
    t.integer "most_recent_module_id"
    t.jsonb "hash_of_quiz_answers", default: {}
    t.jsonb "most_recent_quiz_question_index", default: {}
    t.jsonb "hash_of_quiz_results", default: {}
    t.index ["course_id"], name: "index_progress_events_on_course_id"
    t.index ["student_id", "course_id"], name: "index_progress_events_on_student_id_and_course_id", unique: true
    t.index ["student_id"], name: "index_progress_events_on_student_id"
  end

  create_table "report_account_digests", force: :cascade do |t|
    t.string "digest"
    t.bigint "student_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "activated", default: false
    t.index ["student_id"], name: "index_report_account_digests_on_student_id"
  end

  create_table "sa_quizzes", force: :cascade do |t|
    t.string "question"
    t.text "correct_answer"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "save_events", force: :cascade do |t|
    t.bigint "student_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.string "saveable_type", null: false
    t.bigint "saveable_id", null: false
    t.index ["saveable_type", "saveable_id"], name: "index_save_events_on_saveable"
    t.index ["student_id"], name: "index_save_events_on_student_id"
  end

  create_table "shopping_carts", force: :cascade do |t|
    t.bigint "student_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "course_id"
    t.index ["course_id"], name: "index_shopping_carts_on_course_id"
    t.index ["student_id"], name: "index_shopping_carts_on_student_id"
  end

  create_table "students", force: :cascade do |t|
    t.string "username"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email"
    t.integer "one_time_pin"
    t.boolean "is_email_verified", default: false
    t.string "id_token"
    t.string "background_color"
    t.string "display_name"
    t.datetime "last_display_name_change"
    t.text "reason_for_deactivation"
    t.text "reason_for_deletion"
    t.integer "account_status", default: 0
    t.text "background_info"
    t.string "occupation"
    t.boolean "is_subscribed", default: false
    t.string "time_zone", default: "+00:00", null: false
    t.integer "unread_notifications_count", default: 0
    t.integer "notifications_count", default: 0
    t.index ["email"], name: "index_students_on_email", unique: true
    t.index ["id_token"], name: "index_students_on_id_token", unique: true
    t.index ["username"], name: "index_students_on_username", unique: true
  end

  create_table "subscriptions", force: :cascade do |t|
    t.bigint "student_id", null: false
    t.decimal "monthly_price", precision: 7, scale: 2
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "country_origin", null: false
    t.datetime "finished_at", precision: 6
    t.index ["student_id"], name: "index_subscriptions_on_student_id"
  end

  create_table "transaction_managers", force: :cascade do |t|
    t.integer "guest_session_count", default: 0, null: false
    t.integer "student_session_count", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "upvotes", force: :cascade do |t|
    t.bigint "student_id", null: false
    t.string "upvoteable_type", null: false
    t.bigint "upvoteable_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["student_id"], name: "index_upvotes_on_student_id"
    t.index ["upvoteable_type", "upvoteable_id"], name: "index_upvotes_on_upvoteable"
  end

  create_table "video_submissions", force: :cascade do |t|
    t.bigint "assignment_video_id"
    t.bigint "student_id", null: false
    t.integer "upvotes", default: 0
    t.integer "downvotes", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "title", default: "Untitled", null: false
    t.bigint "competition_id", null: false
    t.text "description"
    t.integer "comments_count", default: 0
    t.integer "upvotes_count", default: 0
    t.decimal "rating", precision: 3, scale: 1, default: "0.0"
    t.integer "ranking", limit: 2
    t.index ["assignment_video_id"], name: "index_video_submissions_on_assignment_video_id"
    t.index ["competition_id"], name: "index_video_submissions_on_competition_id"
    t.index ["ranking"], name: "index_video_submissions_on_ranking", unique: true
    t.index ["student_id", "competition_id"], name: "index_video_submissions_on_student_id_and_competition_id", unique: true
    t.index ["student_id"], name: "index_video_submissions_on_student_id"
  end

  create_table "view_events", force: :cascade do |t|
    t.datetime "time_of_view"
    t.bigint "student_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.bigint "course_id"
    t.index ["course_id"], name: "index_view_events_on_course_id"
    t.index ["student_id"], name: "index_view_events_on_student_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "bank_accounts", "students"
  add_foreign_key "billing_addresses", "students"
  add_foreign_key "comments", "students"
  add_foreign_key "competition_events", "competitions"
  add_foreign_key "competition_events", "students"
  add_foreign_key "competitions", "students"
  add_foreign_key "lessons", "module_parts"
  add_foreign_key "mc_quiz_answers", "mc_quizzes"
  add_foreign_key "mc_quiz_answers", "students"
  add_foreign_key "mc_quiz_questions", "mc_quizzes"
  add_foreign_key "module_parts", "courses"
  add_foreign_key "notifications", "students"
  add_foreign_key "orders", "students", column: "buyer_id"
  add_foreign_key "orders", "students", column: "seller_id"
  add_foreign_key "orders", "subscriptions", column: "transaction_id"
  add_foreign_key "password_reset_digests", "students"
  add_foreign_key "progress_events", "courses"
  add_foreign_key "progress_events", "students"
  add_foreign_key "report_account_digests", "students"
  add_foreign_key "save_events", "students"
  add_foreign_key "shopping_carts", "students"
  add_foreign_key "subscriptions", "students"
  add_foreign_key "upvotes", "students"
  add_foreign_key "video_submissions", "assignment_videos"
  add_foreign_key "video_submissions", "competitions"
  add_foreign_key "video_submissions", "students"
  add_foreign_key "view_events", "students"
end
