# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# create 5 students; aka, 4 more students

#usernames = ["coreastreet50", "Stephen", "Gabriel", "Julian"]
#emails = ["coreastreet50@gmail.com", "Stephen@gmail.com", "Gabriel@gmail.com", "Julian@gmail.com" ]
#pins = [345623, 185234, 340993, 293090]

#[*0..3].each do |index|
#  Student.create!(
#          username: usernames[index],
#          password: "Soba3724",
#          email: emails[index],
#          one_time_pin: pins[index],
#          is_email_verified: true
#    )
#end
=begin
notes = Student.first.notes

notes.each do |note|
  path = Rails.root.join("app/assets/notes_collection/#{note.title}.pdf")
  pdf = Magick::ImageList.new(path)
  first_page = pdf.first
  note.attach.first_page
end
=end

array = Student.all
categories = ["History", "Biology", "Business", "English", "Finance",
          "Maths", "English", "Science", "Modern History", "Ancient History"]
titles = ["agrippa", "maintaining_a_balance", "nature_of_business", "dubliners", "finance_strategies",
          "function_cheatsheet", "owen", "scientist", "ww2", "xerxes"]

schools = ["NSBHS", "James Ruse", "Epping High", "St George Girls", "Hurstville Technical"]
#array.each_with_index do |student, index|
#result: 99 + rand().round(2),
#educational_institute: schools.sample,
#year_of_completion: rand(2012..2020),
#verified: true,

[*0..9].each do |index|
    note = Note.create!(
      title: titles[index],
      price: rand(1..100),
      number_of_pages: rand(1..100),
      description: "Here goes the text",
      student_id: array.sample.id
  )
  path = Rails.root.join("app/assets/notes_collection/#{titles[index]}.pdf")
  reader = PDF::Reader.new(path)

  category = Category.create!(
      name: categories[index],
      note_id: subject.id
  )

  note.pdf.attach(
                        io: File.open(path),
                  filename: "#{titles[index]}3.pdf",
              content_type: 'application/pdf'
  )

end
