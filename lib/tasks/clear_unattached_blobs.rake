

desc 'Remove all files that are no longer attached to notes.'
task whenever_call: :environment do
  ActiveStorage::Blob.unattached.each(&:purge)
end
